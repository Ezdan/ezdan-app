app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
            .state('land', {
                url: '/land',
                templateUrl: 'components/land/land.html',
                controller: 'landController',
                resolve: {
                    isLoggedIn: function ($rootScope, $location,$ionicCloudProvider) {
                      $ionicCloudProvider.init({
                        "core": {
                          "app_id": "61771bd4"
                        }
                      });
                        if ($rootScope.user.loggedIn) {
//                            $location.path("/home");
                        }
                    }
                }
            })
            .state('register', {
                url: '/register',
                templateUrl: 'components/user/register/register.html',
                controller: 'registerController'
            })
            .state('home', {
                url: '/home',
                templateUrl: 'components/home/home.html',
                controller: 'homeController'
            })
            .state('sakin', {
                url: '/sakin',
                //controller: '',
                templateUrl: 'components/sakin/sakin.html'
            })
            .state('request', {
                url: '/request',
                //controller: '',
                templateUrl: 'components/request/request.html'
            })
            .state('surveys', {
                url: '/surveys',
                controller: 'surveysController',
                templateUrl: 'components/surveys/surveys.html'
            })
            .state('surveys/', {
                url: '/surveys/:surveyId',
                controller: 'surveyPerLocationController',
                templateUrl: 'components/surveys/surveyPerLocation.html'
            })
            .state('surveys//', {
                url: '/surveys/:surveyId/:questions',
                controller: 'surveyQuestionsController',
                templateUrl: 'components/surveys/surveyQuestions.html'
            })
            .state('settings', {
                url: '/settings',
                controller: 'settingsController',
                templateUrl: 'components/settings/settings.html'
            })
            .state('about', {
                url: '/about',
                templateUrl: 'components/plus/plus.html'
            })
            .state('plus', {
                url: '/plus',
                controller: 'plusController',
                templateUrl: 'components/plus/plus.html'
            });
    $urlRouterProvider.otherwise('/land');
});
