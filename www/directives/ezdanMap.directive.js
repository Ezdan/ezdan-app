'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('ezdanMap', function() {
        return{
            templateUrl: 'components/cms/templates/ezdanMap.html'
        };

    });
})();
