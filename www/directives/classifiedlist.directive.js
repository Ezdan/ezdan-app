'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('classifiedList', function() {

        return{
            templateUrl: 'components/cms/templates/classifiedlist.html'
        };

    });
})();
