'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('futureProjectsList', function() {

        return{
            templateUrl: 'components/cms/templates/futureProjectsList.html'
        };

    });
})();
