'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('sniffNetwork', ['$rootScope', '$cordovaNetwork', function($rootScope, $cordovaNetwork) {
        return {
            restrict: 'A',
            link: function(scope, elem, attrs) {
                $rootScope.isOnline = true;
                // document.addEventListener("deviceready", function () {
                //     $rootScope.network = $cordovaNetwork.getNetwork();
                //     $rootScope.isOnline = $cordovaNetwork.isOnline();
                //     // $scope.$apply();
                //     console.log($rootScope.network);
                //     console.log($rootScope.isOnline);
                //
                //     // listen for Online event
                //     $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                //         $rootScope.isOnline = true;
                //         $rootScope.network = $cordovaNetwork.getNetwork();
                //
                //         console.log("online");
                //         console.log($rootScope.isOnline);
                //         console.log($rootScope.network);
                //         // $scope.$apply();
                //     })
                //     // listen for Offline event
                //     $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                //         $rootScope.isOnline = false;
                //         $rootScope.network = $cordovaNetwork.getNetwork();
                //         $rootScope.popup(
                //             'warning',
                //             $rootScope.langCounterpart('warning') || 'Warning!',
                //             $rootScope.langCounterpart('warning_message') || 'You are using the app in offline mode, some features might not be supported.'
                //         );
                //
                //         console.log("got offline");
                //         console.log($rootScope.isOnline);
                //         console.log($rootScope.network);
                //         console.log(networkState);
                //         console.log(event);
                //         // $scope.$apply();
                //     })
                // }, false);
            }
        }
    }])
    .directive('focusOn', ['$timeout', function($timeout) {
        return {
            restrict: 'A',
            link: function($scope, $element, $attr) {
                $scope.$watch($attr.focusOn, function(focusVal) {
                    $timeout(function() {
                        focusVal ? $element[0].focus() : $element[0].blur();
                    });
                });
            }
        }
    }])
    .directive('toggleText', function(){
        return {
            scope: '=',
            link: function(scope, element, attrs) {
                scope.$watch(attrs.toggleText, function(val) {
                    console.log(val)
                })
                console.log(attrs.toggleText);
                console.log($(element).find('.back-text').text());
            }
        }
    })
    .directive('ezLeftMenu', function () {
        return {
            templateUrl: "./templates/menu.html"
        }
    })
    .directive('pwCheck', function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                var firstPassword = $('#' + attrs.pwCheck);
                $("#password, #passwordconfirm, input[name=newPassword], input[name=newPasswordConfirmation]").on('keyup', function () {
                    scope.$apply(function () {
                        ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
                    });
                });
            }
        };
    })
    .directive('slideable', function () {
        return {
            restrict:'C',
            compile: function (element, attr) {
                // wrap tag
                var contents = element.html();
                element.html('<div class="slideable_content" style="margin:0 !important; padding:0 !important" >' + contents + '</div>');
                return function postLink(scope, element, attrs) {
                    // default properties
                    attrs.duration = (!attrs.duration) ? '1s' : attrs.duration;
                    attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;
                    element.css({
                        'overflow': 'hidden',
                        'transitionProperty': 'height',
                        'transitionDuration': attrs.duration,
                        'transitionTimingFunction': attrs.easing
                    });
                };
            }
        };
    })
    .directive('slideToggle', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var target, content;

                attrs.expanded = false;

                element.bind('click', function() {
                    if (!target) target = document.querySelector(attrs.slideToggle);
                    if (!content) content = target.querySelector('.slideable_content');

                    if(!attrs.expanded) {
                        content.style.border = '1px solid rgba(0,0,0,0)';
                        var y = content.clientHeight;
                        content.style.border = 0;
                        target.style.height = y + 'px';
                    } else {
                        target.style.height = '0px';
                    }
                    attrs.expanded = !attrs.expanded;
                });
            }
        }
    })
    .directive('setHeight', function($timeout, $window){
        return{
            link: function(scope, element, attrs){
                $timeout(function() {
                    var bannerHeight = $(element).parent().height() + 'px';
                    element.css("line-height", bannerHeight)
                    $(window).on("resize", function() {
                        var bannerHeight = $(element).parent().height() + 'px';
                    })
                })
            }
        }
    })
    .directive("showPassword", function() {
    return function linkFn(scope, elem, attrs) {
        scope.$watch(attrs.showPassword, function(newValue) {
            if (newValue) {
                elem.attr("type", "text");
            } else {
                elem.attr("type", "password");
            };
        });
        };
    })
    .directive('respond', function($timeout, $window){
        return{
            link: function(scope, element, attrs){
                $timeout(function() {
                    var H = $window.innerHeight;
                    var W = $window.innerWidth;
                    var ezBow, ezBowImg, contentBox, inputWrapper, input, homeButton, forgotPassword, or, orText, loginButton, guestLoginButton;
                    var ezBowTopOffset, ezBowLeftOffset, ezBowImgLeftOffset, ezBowImgLeftOffset, contentBoxPadding, contentBoxTopPadding, inputWrapperPadding, inputHeight, inputFontSize, homeButtonWidth, homeButtonHeight, orHeight, forgotPasswordFontSize, orFontSize, loginButtonMargins, guestLoginButtonMargins;
                    contentBox = $(element).find('.content-box');
                    inputWrapper = $(element).find('.content-box .item-input');
                    input = $(element).find('.content-box .item-input input');
                    homeButton = $(element).find('.ez-home-button');
                    forgotPassword = $(element).find('.forgot-link a');
                    or = $(element).find('.or');
                    orText = $(element).find('.or p');
                    loginButton = $(element).find('.ez-home-button:first');
                    guestLoginButton = $(element).find('.ez-home-button:nth-child(2)');
                    ezBow = $(element).find('.ez-bow');
                    ezBowImg = $(element).find('.ez-bow img');

                    contentBoxPadding = 0.0374812593703148 * H;
                    contentBoxTopPadding = 0.0629685157421289 * H;
                    inputWrapperPadding = 0.0089955022488756 * H + 'px 0 ' + 0.007496251874063 * H + 'px 0';
                    inputHeight = 0.0419790104947526 * H;
                    inputFontSize = homeButtonFontSize = 0.032 * W;
                    homeButtonWidth = 0.3925925925925926 * W;
                    homeButtonHeight = 0.0552371026576342 * H;
                    orHeight = 0.1521625846795206 * H + 'px';
                    forgotPasswordFontSize = orFontSize = 0.0266666666666667 * W;
                    loginButtonMargins = '0 ' + 0.0162037037037037 * W + 'px 0 ' + 0.05 * W + 'px';;
                    guestLoginButtonMargins = '0 ' + 0.05 * W + 'px 0 ' + 0.0162037037037037 * W + 'px'
                    ezBowLeftOffset = 'calc(50% - ' + 32.87037037037037 / 2 + '%)';
                    ezBowTopOffset = -(0.0828556539864513 * H);
                    ezBowImgLeftOffset = 'calc(50% - ' + 60.84507042253521 / 2 + '%)';
                    ezBowImgTopOffset = 0.0270974465867639 * H;
                    var margins = Math.round(0.0286607608129234 * $window.innerHeight)
                    + 'px auto '
                    + Math.round(0.0338718082334549 * $window.innerHeight)
                    + 'px auto';
                    contentBox.css("padding", contentBoxPadding);
                    contentBox.css("padding-top", contentBoxTopPadding);
                    inputWrapper.css("padding", inputWrapperPadding);
                    input.css("height", inputHeight);
                    input.css("font-size", inputFontSize);
                    homeButton.css("width", homeButtonWidth);
                    homeButton.css("height", homeButtonHeight);
                    homeButton.css("font-size", homeButtonFontSize);
                    forgotPassword.css("font-size", forgotPasswordFontSize);
                    or.css("height", orHeight);
                    or.css("line-height", orHeight);
                    orText.css("font-size", orFontSize);
                    loginButton.css("margin", loginButtonMargins);
                    guestLoginButton.css("margin", guestLoginButtonMargins);
                    ezBow.css("top", ezBowTopOffset);
                    ezBow.css("left", ezBowLeftOffset);
                    ezBowImg.css("left", ezBowImgLeftOffset);
                    ezBowImg.css("top", ezBowImgTopOffset);

                    $(element).find('.forgot-link a').css("margin", margins);
                    $(window).on("resize", function() {
                        input.css("height", inputHeight);
                        $(element).find('.forgot-link a').css("margin", margins);
                    })
                })
            }
        }
    })
})();
