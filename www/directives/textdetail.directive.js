'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('textDetail', function() {

        return {
            templateUrl: 'components/cms/templates/textdetail.html'
        };

    });
})();
