'use strict';
(function() {
    angular
    .module('ezdanApp')
    .factory('DB', DB);

    DB.$inject = ['$q', 'DB_CONFIG', '$ionicPlatform', '$cordovaSQLite'];

    function DB($q, DB_CONFIG, $ionicPlatform, $cordovaSQLite) {
        // $ionicPlatform.ready(function() {
        var self = this;
        self.db = null;

        self.update = function(tableName,ColumnName,newValue){
            var query = 'update ' + tableName + ' set ' + ColumnName + ' = "' + newValue +'"';
            self.query(query);
        }
        self.updateRow = function(tableName,ColumnName,newValue,key,keyValue){
            var query = 'UPDATE ' + tableName + ' SET ' + ColumnName + '="' + newValue +'" WHERE ' + key + '="' + keyValue + '"';
            // console.log(query);
            self.query(query);
        }
        self.delete = function(tableName){
            var query = 'delete from ' + tableName;
            self.query(query);
        }
        self.insert = function  (tableName,values,performDelete,postFunc) {
            var query = '';
            var deletequery = '';
            if (tableName == "user") {
                query = 'INSERT INTO user (email,language,notification,imageURL,mobile,fullName,title,unit,location,access_token,expires_in) VALUES(?,?,?,?,?,?,?,?,?,?,?)';
            }
            else if (tableName == "dictionary") {
                query = "INSERT INTO dictionary (key,english, arabic) VALUES (?,?,?)";
            }
            else if (tableName == "tempUser") {
                query = "INSERT INTO tempUser (id, title, email, name, phone, password, passwordConf, contractNum, AXAccountId, AXPartyRecId, AXLocation, AXUnitId, lang) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            }
            else if (tableName == "services") {
                query = "INSERT INTO services (parentRecordId, categoryType, recordId, caseCategory, description) VALUES (?,?,?,?,?)";
            }
            else if (tableName == "home") {
                query = 'INSERT INTO home (bannerImg, last_modified) VALUES(?,?)';
            }
            else if (tableName == "content") {
                query = "INSERT INTO content (key, value, last_modified) VALUES (?,?,?)";
            }
            else if (tableName == "propertiesList") {
                query = "INSERT INTO propertiesList (key, value, last_modified) VALUES (?,?,?)";
            }
            else if (tableName == "property") {
                query = "INSERT INTO property (key, value, last_modified) VALUES (?,?,?)";
            }
            else if (tableName == "propertiesFilters") {
                query = "INSERT INTO propertiesFilters (id, display, value, type) VALUES (?,?,?,?)";
            }
            else if (tableName == "newsList") {
                query = "INSERT INTO newsList (key, value, last_modified) VALUES (?,?,?)";
            }
            else if (tableName == "singleNews") {
                query = "INSERT INTO singleNews (key, value, last_modified) VALUES (?,?,?)";
            }
            else if (tableName == "albumsList") {
                query = "INSERT INTO albumsList (key, value, last_modified) VALUES (?,?,?)";
            }
            else if (tableName == "album") {
                query = "INSERT INTO album (key, value, last_modified) VALUES (?,?,?)";
            }
            else if (tableName == "DC") {
                query = "INSERT INTO DC (key, value, last_modified) VALUES (?,?,?)";
            }
            else if(tableName == "FilterObj"){
                query = "INSERT INTO DC (city, bedrooms, propertyType,isFurnished) VALUES (?,?,?,?)";
            }

            self.query(query,values).then(function(){
                if (postFunc!= null) postFunc();
            },function(error){
                console.error(error);
            })
        };

        self.find = function  (tableName,attribute,key) {
            // var funcResult = null;
            if(!attribute) {
                var query = 'SELECT * FROM ' + tableName;
            }
            else {
                var query = 'SELECT * FROM ' + tableName + ' WHERE ' + attribute + ' = "' + key + '"';
            }
            if(self.db == null){
                    //self.db = $cordovaSQLite.openDB({name : DB_CONFIG.name  , location : 'default'});

                if (window.cordova && window.SQLitePlugin) {
                    self.db = window.sqlitePlugin.openDatabase({name : DB_CONFIG.name  , location : 1});
                } else {
                    self.db = window.openDatabase(DB_CONFIG.name, '1.0', 'database', -1);
                }
                //self.db = $cordovaSQLite.openDB({name : 'DB'});
            }



            return self.query(query).then(function(result){
                return self.fetchAll(result);
            });
        };



        self.get = function (tableName) {
            if(self.db == null){
                    //self.db = $cordovaSQLite.openDB({name : DB_CONFIG.name  , location : 'default'});

                if (window.cordova && window.SQLitePlugin) {
                    self.db = window.sqlitePlugin.openDatabase({name : DB_CONFIG.name  , location : 1});
                } else {
                    self.db = window.openDatabase(DB_CONFIG.name, '1.0', 'database', -1);
                }
                //self.db = $cordovaSQLite.openDB({name : 'DB'});
            }
            return self.query('SELECT * FROM '+ tableName).then(function(result){
                return self.fetchAll(result);
            })
        };

        self.init = function() {
            console.log('initialize');
            //self.db = window.sqlitePlugin.openDB({name: DB_CONFIG.name});
            //self.db = $cordovaSQLite.openDB({name : DB_CONFIG.name  , location : 'default'});

            if (window.cordova && window.SQLitePlugin) {
                    self.db = window.sqlitePlugin.openDatabase({name : DB_CONFIG.name  , location : 1});
                } else {
                    self.db = window.openDatabase(DB_CONFIG.name, '1.0', 'database', -1);
                }
            angular.forEach(DB_CONFIG.tables, function(table) {
                var columns = [];
                angular.forEach(table.columns, function(column) {
                    columns.push(column.name + ' ' + column.type);
                });
                // var drop = 'drop table ' + table.name;
                // var drop = 'drop table propertiesFilters';
                // self.query(drop);
                var query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';

                self.query(query);
            });
        };

        self.query = function(query, bindings) {
            bindings = typeof bindings !== 'undefined' ? bindings : [];
            // alert(query);
            var deferred = $q.defer();

            $cordovaSQLite.execute(self.db,query,bindings).
            then(function(result){
                deferred.resolve(result);
            },function(error){
                deferred.reject(error);
            });
            // self.db.transaction(function(transaction) {
            //     transaction.executeSql(query, bindings, function(transaction, result) {

            //         deferred.resolve(result);
            //     }, function(transaction, error) {

            //         deferred.reject(error);
            //     });
            // });

            return deferred.promise;
        };

        self.fetchAll = function(result) {
            var output = [];

            for (var i = 0; i < result.rows.length; i++) {
                output.push(result.rows.item(i));
            }

            return output;
        };

        self.fetch = function(result) {
            return result.rows.item(0);
        };

        return self;
        // })
    }
})();
