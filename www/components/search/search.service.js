'use strict';
(function() {
    angular
        .module('ezdanApp')
        .factory('searchService', searchService);
        function searchService($resource) {
            return {
                search: function(keyword) {
                    var service = $resource(window.backendUrl + 'Account', {}, {
                        'onKeyup': {
                            method: 'POST', isArray: false,
                            url: window.backendUrl + 'search'
                        }
                    });
                    return service;
                }
            }
        }
})();
