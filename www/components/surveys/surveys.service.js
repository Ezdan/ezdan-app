'use strict';
(function() {
    angular
        .module('ezdanApp')
        .factory('SurveyService', SurveyService);

        SurveyService.$inject = ['$rootScope', '$resource', '$timeout', '$q', '$http', 'authentication', 'localStorageService', 'DB'];

        function SurveyService($rootScope, $resource, $timeout, $q, $http, authentication, localStorageService, DB) {
            var surveysService = {},
                checkedSurveys = [],
                surveyIds = [];
            var authorize = function(token) {
                var service = $resource(window.backendUrl+'survey', {}, {
                    'GetSurveyLocations': {
                        method: 'POST', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + token },
                        url:window.backendUrl+'survey/getSurveyLocations'
                    },
                    'GetSurveysPerLocation': {
                        method: 'POST', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + token },
                        url:window.backendUrl+'survey/getSurveysPerLocation'
                    },
                    'GetSurveyQuestionsPerLocation': {
                        method: 'POST', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + token },
                        url:window.backendUrl+'survey/getSurveyQuestionsPerLocation'
                    },
                    'SubmitSurvey': {
                    method: 'POST', isArray: false,
                    headers: { 'Authorization': 'Bearer ' + token },
                    url:window.backendUrl+'survey/submitSurvey'
                    }

                });
                return service;
            };
            var checkSchedules = function(surveys) {
                // checkedSurveys =[];
                var q = $q.defer();
                DB.get('user').then(function(data) {
                    var token = data[0].access_token;
                    var sessionKey = localStorageService.get('sessionKey');
                    var promises = [];
                    for(var i = 0; i < surveys.length; i++) {
                        var currSurvey = surveys[i];
                        if(currSurvey.taken) continue;
                        var promise = $http.post(
                            window.backendUrl + 'survey/getSurveyQuestionsPerLocation',
                            JSON.stringify({surveyId: currSurvey.id, sessionKey: sessionKey}),
                            {
                                headers: {'Authorization': 'Bearer ' + token, 'Content-type': 'application/json'}
                            }
                        )
                        promises.push(promise);
                    }
                    $q.all(promises).then(function(surveysQuestions) {
                        var checkedSurveysQuestions = [];
                        for(var i = 0; i < surveysQuestions.length; i++) {
                            var currSurveyQuestions = surveysQuestions[i].data;
                            if(currSurveyQuestions.surveyschedule
                                && currSurveyQuestions.surveyschedule instanceof Array 
                                && currSurveyQuestions.surveyschedule.length > 0) {
                                if(checkSchedule(currSurveyQuestions.surveyschedule) === 0) {
                                    checkedSurveysQuestions.push(currSurveyQuestions);    
                                }
                            }
                            else {
                                checkedSurveysQuestions.push(currSurveyQuestions);
                            }
                        }
                        checkedSurveys = checkedSurveysQuestions;
                        q.resolve(checkedSurveys);
                    })
                    .catch(function(error) {
                        checkedSurveys = surveys;
                        q.resolve(checkedSurveys);
                    });
                });
                return q.promise;
            };
            var daysOfWeek = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
            var checkSchedule = function(schedule) {
                var serverTimezoneOffset = 180,
                    today = new Date(),
                    day = today.getDate(),
                    dayOfWeek = daysOfWeek[today.getDay()],
                    clientTimeZoneOffset = today.getTimezoneOffset(),
                    timeZoneDifference = -((serverTimezoneOffset + clientTimeZoneOffset) * 60 * 1000);
                for(var i = 0; i < schedule.length; i++) {
                    var scheduleDayObj = schedule[i],
                        scheduleDay = scheduleDayObj.weekdays,
                        scheduleStartDate = scheduleDayObj.survey_startdate,
                        scheduleEndDate = scheduleDayObj.survey_enddate,
                        scheduleStartTime = scheduleDayObj.shift_first_fromtime,
                        scheduleEndTime = scheduleDayObj.shift_first_totime;
                    if(dayOfWeek === scheduleDay.toLowerCase() 
                        || day === parseInt(scheduleDayObj.weekdays)) {
                        var scheduleStartDateTime = new Date(scheduleStartDate + ' ' + scheduleStartTime),
                            scheduleEndDateTime = new Date(scheduleEndDate + ' ' + scheduleEndTime),
                            localScheduleStartDateTime = new Date(scheduleStartDateTime.getTime() + timeZoneDifference),
                            localScheduleEndDateTime = new Date(scheduleEndDateTime.getTime() + timeZoneDifference);
                        if(localScheduleStartDateTime.getTime() < today.getTime() 
                            && localScheduleEndDateTime.getTime() > today.getTime()) {
                            return 0; //Current time is within the schedule (OK)
                        }
                        else if(localScheduleEndDateTime.getTime() < today.getTime()) {
                            return 1; //Schedule has passed
                        }
                        else if(localScheduleStartDateTime.getTime() > today.getTime()) {
                            return 2; //Schedule has not started
                        }
                    }
                }
            }
            surveysService.authorize = authorize;
            surveysService.checkSchedules = checkSchedules;
            return surveysService;
        };
        
})();
