'use strict';
(function () {
  angular
    .module('ezdanApp')
    .controller('surveyQuestionsController', function ($rootScope, SurveyService, localStorageService, $scope,
      $stateParams, $state, authentication, $ionicModal, $timeout,$ionicPlatform) {
      $rootScope.token = authentication.authData.token;
      $scope.data = {};
      $scope.toggleLanguage = function () {
        console.log($rootScope.dict.settings);
      }
      $scope.language = $rootScope.lang;
      $scope.title = "Please answer the following questions";
      $scope.Id = $stateParams.surveyId;
      $scope.sessionKey = localStorageService.get('sessionKey');

      $ionicPlatform.onHardwareBackButton(function () {
        if(true) { // your check here
          $scope.hideModal();
        }
      })

      $scope.showTypeOfTest = function () {
        $ionicModal.fromTemplateUrl('./components/surveys/modal.html', {
          scope: $scope,
        }).then(function (modal) {
          $scope.modal = modal;
          $scope.modal.show("fadeInDown");
        });
      }

      $scope.hideModal = function () {
        $scope.modal.hide();
      }

      $scope.$watch(function () {
        return $rootScope.token;
      }, function (token) {
        if (token != "" && typeof (token) !== 'undefined')
          $scope.getSurveyQuestions(token);
      })

      $scope.questionsLoading = false;

      $scope.getSurveyQuestions = function (token) {
        $scope.questionsLoading = true;
        $rootScope.load();
        SurveyService.authorize(token).GetSurveyQuestionsPerLocation({ "surveyId": $scope.Id, "sessionKey": $scope.sessionKey,"seo":'x18' }).$promise.then(function (data) {
          $scope.questionsLoading = false;
          if (data) {
            $rootScope.unload();
            $scope.Questions = data;
            $scope.SurveyQuestions = $scope.Questions.items;
            for (var i = 0; i < $scope.SurveyQuestions.length; i++) {
              $scope.SurveyQuestions[i].answers_type = $scope.SurveyQuestions[i].sub_items[0].subitem_type;
            }
            $scope.showTypeOfTest();
          }
        }, function (error) {
          $rootScope.unload();
          $rootScope.popup(
            'error',
            $rootScope.langCounterpart('failure') || 'Failure!',
            $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
          );
        });
      }

      //---------------------------------------------------------------------------------------------------
      $scope.submitData = function (form) {
        $scope.finalArray = [];
        if (form.$valid) {
          if (typeof $scope.dataToSubmit == 'undefined') {
            $scope.finalArray = $scope.dataToSubmitSingleEntry;
          }
          else if (typeof $scope.dataToSubmitSingleEntry == 'undefined') {
            $scope.finalArray = $scope.dataToSubmit;
          }
          else {
            $scope.finalArray = $scope.dataToSubmit.concat($scope.dataToSubmitSingleEntry);
          }
          $scope.finalArray = $scope.prepareSurveyData($scope.finalArray);
          var finalDataToBeSubmitted = {
            "session_key": "",
            "survey_id": $scope.Id,
            "other_info": {
              "location_id": $stateParams.locationid, "language": $scope.language == "Arabic" ? "ar" : "en"
            },
            "results": $scope.finalArray
            ,"seo":'x18'
          }
          $scope.finalData = finalDataToBeSubmitted
          $rootScope.load();
          // return;
          SurveyService.authorize(authentication.authData.token).
            SubmitSurvey($scope.finalData).$promise.
            then(function (data) {
              $rootScope.unload();
              $rootScope.popup(
                'success',
                $rootScope.langCounterpart('request_submitted') || 'Request Submitted',
                $rootScope.langCounterpart('thanks') || 'Thank you for your time.'
              );
              $rootScope.goHome();
            }, function (error) {
              $rootScope.unload();
              $rootScope.popup(
                'error',
                $rootScope.langCounterpart('failure') || 'Failure!',
                $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
              );
            });
        }
        else {
          $rootScope.unload();
          $rootScope.popup(
            'error',
            $rootScope.langCounterpart('failure') || 'Failure!',
            $rootScope.langCounterpart('please_fill_required') || 'Please fill all * required fields.'
          );
        }
      }
      //---------------------------------------------------------------------------------------------------
      //append quesAnsOneItem to quesAns at the end before submitting
      var quesAnsOneItem = []
      $scope.changed = function (questionId, subitem_id, value, text, type) {
        if(type && type.toLowerCase().indexOf('others') != -1) {
          if($scope.othersChoicesIds.indexOf(questionId) == -1) {
            $scope.othersChoicesIds.push(questionId);
          }
          return;
        }
        if(type && type !== "TextBox" && $scope.othersChoicesIds.indexOf(questionId) > -1) {
          $scope.othersChoicesIds.splice($scope.othersChoicesIds.indexOf(questionId), 1);
          $scope.data[questionId] = null;
        }
        if (quesAnsOneItem.length === 0) {
          var item = {
            "subitem_id": subitem_id,
            "text": value,
            "value": text
          }
          var answerStructure = {
            "item_id": questionId,
            "responses": []
          }
          answerStructure.responses.push(item)
          quesAnsOneItem.push(answerStructure)
        }
        else {
          //check if the item id is found, if yes, remove reponses and add new responses
          //else add new item
          var item = {
            "subitem_id": subitem_id,
            "text": value,
            "value": text
          }
          var answerStructure = {
            "item_id": questionId,
            "responses": []
          }
          for (var i = 0; i < quesAnsOneItem.length; i++) {
            // var found = false
            if (quesAnsOneItem[i].item_id === answerStructure.item_id) {
              quesAnsOneItem[i].responses = []
              quesAnsOneItem[i].responses.push(item)
              break
            }
            else if (i === quesAnsOneItem.length - 1) {
              answerStructure.responses.push(item)
              quesAnsOneItem.push(answerStructure)
              break
            }

          }

        }
        $scope.dataToSubmitSingleEntry = quesAnsOneItem;
      }
      //---------------------------------------------------------------------------------------------------
      var finalResults = [];
      //array of objects with item id and reponses
      var questionAns = []
      $scope.checkboxList = []
      $scope.submitOneItem = function (questionId, subitem_id, value, text, type, event) {
        if(type && type.toLowerCase().indexOf('others') != -1) {
          if(event && event.target.checked && $scope.othersChoicesIds.indexOf(questionId) == -1) {
            $scope.othersChoicesIds.push(questionId);
            $scope.checkboxList.push(text);
          }
          else {
            $scope.othersChoicesIds.splice($scope.othersChoicesIds.indexOf(questionId), 1);
            $scope.data[questionId] = null;
            $scope.checkboxList.splice($scope.checkboxList.indexOf(text), 1);
            for (var k = 0; k < $scope.dataToSubmitSingleEntry.length; k++) {
              if($scope.dataToSubmitSingleEntry[k].responses) {
                for (var l = 0; l < $scope.dataToSubmitSingleEntry[k].responses.length; l++) {
                  if ($scope.dataToSubmitSingleEntry[k].responses[l].text 
                    === $scope.dataToSubmitSingleEntry[k].responses[l].value
                    && $scope.dataToSubmitSingleEntry[k].item_id === questionId) {
                    $scope.dataToSubmitSingleEntry.splice(k, 1);
                  }
                }
              }
            }
            for (var k = 0; k < $scope.dataToSubmit.length; k++) {
              for (var l = 0; l < $scope.dataToSubmit[k].responses.length; l++) {
                if ($scope.dataToSubmit[k].responses[l].text 
                  === $scope.dataToSubmit[k].responses[l].value
                  && $scope.dataToSubmit[k].item_id === questionId) {
                  $scope.dataToSubmit[k].responses.splice(l, 1);
                  console.log($scope.dataToSubmit);
                }
              }
            }
          }
          return;
        }
        // return;
        var index = $scope.checkboxList.indexOf(text);
        if (index > -1) {
          $scope.checkboxList.splice(index, 1);
        }
        else {
          $scope.checkboxList.push(text)
        }
        //check if the quesAns array is empty
        if (questionAns.length === 0) {
          var item = {
            "subitem_id": subitem_id,
            "text": value,
            "value": text
          }
          var answerStructure = {
            "item_id": questionId,
            "responses": []
          }
          //if empty, it pushes the id of the field
          answerStructure.responses.push(item);
          questionAns.push(answerStructure);
        }
        else {
          /*if quesAns already has data, there are two scenarios, that the item_id already exists, and we need to alter the
          in the responses array
          OR
          the item_id does not exist and we need to add it with the array of the new object
          */
          var item = {
            "subitem_id": subitem_id,
            "text": value,
            "value": type === "OthersCheckBox" ? 'Others' : text
          }
          var answerStructure = {
            "item_id": questionId,
            "responses": []
          }
          for (var j = 0; j < questionAns.length; j++) {
            var found = false;
            //the first scenario is true:
            if (questionAns[j].item_id === questionId) {
              if (questionAns[j].responses.length === 0) {
                questionAns[j].responses.push(item);
                found = true;
                break;
              }
              else {
                for (var i = 0; i < questionAns[j].responses.length; i++) {
                  if (questionAns[j].responses[i].subitem_id === item.subitem_id) {
                    questionAns[j].responses.splice(i, 1)
                    if(typeof(value) === 'undefined') {
                      questionAns[j].responses.push(item);
                    }
                    found = true;
                    break;
                  }
                  else if (i === questionAns[j].responses.length - 1) {
                    questionAns[j].responses.push(item)
                    break;
                  }
                }
              }
            }
            //The second scenario is true:
            else if (j === questionAns.length - 1) {
              answerStructure.responses.push(item);
              questionAns.push(answerStructure);  
              break;
            }
            if (found)
              break;
          }
        }
        $scope.dataToSubmit = questionAns;
      }

      $scope.othersChoicesIds = [];

      $scope.findOthersChoice = function(question) {
        for(var i = 0; i < question.sub_items.length; i++) {
          if(question.sub_items[i].subitem_type.toLowerCase().indexOf('others') != -1) {
            return true;
          }
        }
        return false;
      }

      $scope.prepareSurveyData = function(data) {
        var tempFinalArray = [];
        data.forEach(function (a) {
          if (!this[a.item_id]) {
            this[a.item_id] = { 
              item_id: a.item_id, 
              responses: a.responses 
            };
            tempFinalArray.push(this[a.item_id]);
          }
          else {
            Array.prototype.push.apply(this[a.item_id].responses, a.responses);
          }
          this[a.item_id].responses = this[a.item_id].responses.filter( function (obj, pos, arr) {
            return arr.map(function(mapObj) {return mapObj.subitem_id}).indexOf(obj.subitem_id) === pos;
          });
        }, Object.create(null));
        return tempFinalArray;
      }
    });
})();