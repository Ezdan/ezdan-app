'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('surveyPerLocationController', function ($rootScope, $timeout, SurveyService, localStorageService, $scope, $location,
       $stateParams, $state,authentication) {
        $scope.SurveysPerLocation = [];
        $scope.toggleLanguage = function() {
            console.log($rootScope.dict.settings);
        }
        $scope.language = authentication.authData.language;
        $scope.title="Select a survey";
        $scope.Id=$stateParams.locationId;
        $scope.location = $stateParams.location;
        $scope.sessionKey=localStorageService.get('sessionKey');
        $scope.$watch(function(){
          if(typeof($rootScope.user) !== 'undefined') {
            return $rootScope.user.token;
          }
        },function(token){
          if(token != "" && typeof(token) !== 'undefined' ) {
            $scope.getsurveyperlocation(token);
          }
        })

        $scope.getsurveyperlocation  = function(token){
          $rootScope.load();
          SurveyService.authorize(token).GetSurveysPerLocation(
            {
              "locationId":$scope.Id,
              "sessionKey":$scope.sessionKey,
              "seo":'x18'
            }
          )
          .$promise.then(function (data) {
            SurveyService.checkSchedules(data.surveys).then(function(checkedSurveys) {
              $rootScope.unload();
              if(checkedSurveys.length > 0) {
                $scope.SurveysPerLocation = checkedSurveys;
              }
              else {
                $rootScope.popup(
                  'error',
                  $rootScope.langCounterpart('no_surveys') || 'No Surveys',
                  $rootScope.langCounterpart('no_surveys_in_this_location') || 'There are currently no active surveys in this location.'
                );
                $rootScope.goBack();
              }
            });
          }, function(error) {
              $rootScope.unload();
              $rootScope.goBack();
              $rootScope.popup(
                'error',
                $rootScope.langCounterpart('failure') || 'Failure!',
                $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
              );
          });
        }

        $scope.submitLocation=function(Id,SessionKey) {};

        $scope.goSurveyQuestions = function(survey) {
         $location.path("survey/" + survey.id + "/" + $scope.Id);
        }
    });
})();
