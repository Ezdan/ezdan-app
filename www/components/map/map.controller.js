'use strict';
(function() {
  angular
  .module('ezdanApp')
  .controller('mapController', function($scope, $ionicLoading, $compile, $stateParams) {
    $scope.latt= parseFloat($stateParams.lat);
    $scope.long= parseFloat($stateParams.long);

    var options = {timeout: 10000, enableHighAccuracy: true};
    var latLng = new google.maps.LatLng($scope.latt, $scope.long);

    var mapOptions = {
      center: latLng,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var marker = new google.maps.Marker({
      position: latLng,
    });

    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
    marker.setMap($scope.map);

  });
})();
