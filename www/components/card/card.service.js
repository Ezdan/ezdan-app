'use strict';
(function() {
    angular
    .module('ezdanApp')
    .factory('cardService', cardService);

    function cardService($resource,$q,DB) {

      var cardService = {
        enrollDiscountCard : function (token,contactNumber) {
          var d = $q.defer();
          authService.authorize(token).enrollDiscountCard(contactNumber).$promise.then(function(data){
            d.resolve(data);
          },function(error){
            d.reject(error);
          });
          return d.promise;
        },
        getEDCOffers: function(){
          //check if there is a connection
          var q = $q.defer();
          if (true) {
            //get from service
            service.getEDCOffers().$promise.then(function(data){
              //save into localdb
              var offers = data.Table;
              return  q.resolve(offers);
              // for (var i = 0; i < offers.length; i++) {
              //   DB.insert('ECDOffers',[offers[i].Key,offers[i].English,offers[i].Arabic]);
              // }
              //  offlineService.getEDCOffers().then(function (data) {
              //   return  q.resolve(data);
              // },function(response){
              //     return q.reject(response)
              // });
            })
          }
          else {
            offlineService.getEDCOffers().then(function (data) {
              if(data.length > 0){
                q.resolve(data);
              }
              else {
                q.reject(data);
              }
            });
          }

          return q.promise;
        },
        getLimitedOffers: function(){
          //check if there is a connection
          var q = $q.defer();
          if (true) {
            //get from service
            service.getLimitedOffers().$promise.then(function(data){
              //save into localdb
              var offers = data.Table;
              return  q.resolve(offers);
              // for (var i = 0; i < offers.length; i++) {
              //   DB.insert('LimitedOffers',[offers[i].Key,offers[i].English,offers[i].Arabic]);
              // }
              //  offlineService.getLimitedOffers().then(function (data) {
              //   return  q.resolve(data);
              // },function(response){
              //     return q.reject(response)
              // });
            })
          }
          else {
            offlineService.getLimitedOffers().then(function (data) {
              if(data.length > 0){
                q.resolve(data);
              }
              else {
                q.reject(data);
              }
            });
          }

          return q.promise;
        },
        getPartnerOffers: function(){
          //check if there is a connection
          var q = $q.defer();
          if (true) {
            //get from service
            service.getPartnerOffers().$promise.then(function(data){
              //save into localdb
              var offers = data.Table;
              return  q.resolve(offers);
              // for (var i = 0; i < offers.length; i++) {
              //   DB.insert('PartnersOffers',[offers[i].Key,offers[i].English,offers[i].Arabic]);
              // }
              //  offlineService.getPartnerOffers().then(function (data) {
              //   return  q.resolve(data);
              // },function(response){
              //     return q.reject(response)
              // });
            })
          }
          else {
            offlineService.getPartnerOffers().then(function (data) {
              if(data.length > 0){
                q.resolve(data);
              }
              else {
                q.reject(data);
              }
            });
          }

          return q.promise;
        }
      };

      var service = $resource(window.backendUrl, {}, {
        'getEDCOffers': {
          method: 'GET', isArray: false,
          headers: [{'Content-Type': 'application/json'}],
          url: window.backendUrl + 'card/edOffers'
        },
        'getLimitedOffers': {
          method: 'GET', isArray: false,
          url : window.backendUrl + 'card/limitedOffers'
        },
        'getPartnerOffers': {
          method: 'GET', isArray: false,
          url: window.backendUrl + 'card/partnerOffers'
        }
      });
      var authService =  {
        authorize: function(token) {
          var svc = $resource(window.backendUrl, {},{
          'enrollDiscountCard':{
            method:'Post',isArray:false,
            headers: { 'Authorization': 'Bearer ' + token },
            url:window.backendUrl + 'card/enrollDiscountCard'
          }
        })
        return svc;
        }
      };
      var offlineService = {
        getEDCOffers : function(){
          return DB.get("ECDOffers");
        },
        getLimitedOffers :function(){
          return DB.get("LimitedOffers");
        },
        getPartnerOffers:function(){
          return DB.get("PartnersOffers");
        }
      }
      return cardService;
    }
})();
