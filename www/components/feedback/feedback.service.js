'use strict';
(function() {
    angular
        .module('ezdanApp')
        .factory('feedbackService', feedbackService);

        feedbackService.$inject = ['$resource'];

        function feedbackService($resource) {
            return {
                authorize: function(token) {
                    var service = $resource(window.backendUrl + 'Account', {}, {
                        'submitFeedback': {
                            method: 'POST', isArray: false,
                            headers: { 'Authorization': 'Bearer ' + token },
                            url: window.backendUrl + 'Feedback'
                        }
                    });

                    return service;
                }
            }
        }
})();
