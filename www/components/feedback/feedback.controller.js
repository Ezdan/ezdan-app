'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('feedbackController', function ($q, $scope, $rootScope, $state, $timeout, $ionicHistory, $stateParams, feedbackService, authentication, $cordovaFile, $cordovaImagePicker, $cordovaCamera, $cordovaFileTransfer, $cordovaActionSheet) {
        $scope.imagesLimit = 1;
        $scope.feedbackData = {
            mobileNumber: $rootScope.user.mobile,
            feedbackImages: [],
            storedFeedbackImages: [],
            typeId: $stateParams.id
        };
        switch($stateParams.id) {
            case 1:
            $scope.viewTitle = $rootScope.langCounterpart('submit_suggestion') || 'Suggestion Form';
            $scope.feedbackType = 'suggestion';
            break;
            case 2:
            $scope.viewTitle = $rootScope.langCounterpart('submit_complaint') || 'Complaint Form';
            $scope.feedbackType = 'complaint';
            break;
            case 3:
            $scope.viewTitle = $rootScope.langCounterpart('submit_inquiry') || 'Inquiry Form';
            $scope.feedbackType = 'inquiry';
            break;
        }

        $scope.feedbackSubmitted = false;

        $scope.submitFeedback = function(form) {
            $scope.feedbackSubmitted = true;
            if(form.$valid) {
                $rootScope.load();
                if($scope.feedbackData.feedbackImages.length > 0) {
                    $scope.submitWithImages();
                } else {
                    var requestParams = {
                        type: $scope.feedbackData.typeId,
                        title: $scope.feedbackData.title,
                        contactNumber: $scope.feedbackData.mobileNumber,
                        message: $scope.feedbackData.message,
                    }
                    $scope.sendFeedback(requestParams);
                }
            }
        };

        $scope.submitWithImages = function() {
            var feedbackImgsUploadPromises = [];
            var url = window.backendUrl + "feedback/uploadfeedbackimages";
            $scope.feedbackData.feedbackImages.forEach(function(img, key) {
                var filename = img.substr(img.lastIndexOf('/')+1).replace(/\s/g,'');
                var optionsUpload = {
                    fileKey: "file",
                    fileName: filename,
                    chunkedMode: false,
                    mimeType: "multipart/form-data",
                    params : {
                        'fileName': filename,
                        'dir': 'suggestions'
                    },
                    headers: {
                        Connection: "close",
                        Authorization: 'Bearer ' + $rootScope.user.token
                    }
                };
                feedbackImgsUploadPromises.push($cordovaFileTransfer.upload(url, img, optionsUpload));
            })
            $q.all(feedbackImgsUploadPromises).then(function(res) {
                for(var i = 0; i < res.length; i++) {
                    if(res[i].response) {
                        var responseObj = JSON.parse(res[i].response);
                        if(responseObj.imageURL) {
                            $scope.feedbackData.storedFeedbackImages.push(responseObj.imageURL);
                        }
                    }
                }
                var requestParams = {
                    type: $scope.feedbackData.typeId,
                    title: $scope.feedbackData.title,
                    contactNumber: $scope.feedbackData.mobileNumber,
                    message: $scope.feedbackData.message,
                    rating: $scope.feedbackData.ratingObj.rating,
                    images: $scope.feedbackData.storedFeedbackImages,
                }
                $scope.sendFeedback(requestParams);
            }, function (error) {
                $rootScope.unload();
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('error_occured') || 'An error has occured, please try again later.'
                );
            });
        };

        $scope.sendFeedback = function(requestParams) {
            feedbackService.authorize($rootScope.user.token).submitFeedback(requestParams)
            .$promise.then(function(data) {
                $rootScope.unload();
                $rootScope.popup(
                    'success',
                    $rootScope.langCounterpart('thank_you') || 'Thank You',
                    $rootScope.langCounterpart('we_have_received_your_' + $scope.feedbackType) || 'We have received your feedback.'
                );
                $rootScope.goBack();
            }, function(error) {
                $rootScope.unload();
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
            })
        }

        $scope.removeImage = function (image) {
            $scope.feedbackData.feedbackImages.splice(image, 1);
            // $scope.requestData.caseImage = null;
        };
        
        $scope.selectImages = function() {
            if($scope.feedbackData.feedbackImages.length === $scope.imagesLimit) {
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('you_can_only_attach_one_image') || 'You can only attach one image.'
                );
                return;
            }
            var optionsSelect = {
                title: 'Please select images source',
                buttonLabels: ['Load from gallery', 'Use camera'],
                addCancelButtonWithLabel: 'Cancel',
                androidEnableCancelButton : true,
            };
            $cordovaActionSheet.show(optionsSelect).then(function(btnIndex) {
                if (btnIndex === 1) {
                    var permissions = cordova.plugins.permissions;
                    permissions.checkPermission(permissions.READ_EXTERNAL_STORAGE, function( status ){
                        if ( status.hasPermission  ) {
                            $scope.selectImagesFromGallery();
                        }
                        else {
                            permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE,
                                function(status) {
                                if( !status.hasPermission ) {
                                    console.warn('Camera permission is not turned on');
                                } else {
                                    $scope.selectImagesFromGallery();
                                }
                            },function() {
                                console.warn('Camera permission is not turned on');
                            });
                        }
                    });
                } else if (btnIndex === 2) {
                    $scope.selectImageByCamera();
                }
            });

        };

        $scope.selectImagesFromGallery = function () {
            var optionsGallery = {
                maximumImagesCount: $scope.imagesLimit - $scope.feedbackData.feedbackImages.length,
                quality: 50
            };
            $cordovaImagePicker.getPictures(optionsGallery)
            .then(function (results) {
                if(results.length != 0) {
                    for(var i = 0; i < results.length; i++) {
                        var selectedImageIos = results[i].replace('file://','')
                        $scope.feedbackData.feedbackImages.push(selectedImageIos);
                    }
                }
            }, function(error) {});
        };

        $scope.selectImageByCamera = function () {
            if($scope.feedbackData.feedbackImages.length === $scope.imagesLimit) {
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('you_can_only_attach_one_image') || 'You can only attach one image.'
                );
                return;
            }
            var optionsCamera = {
                quality: 50,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
                // correctOrientation: true,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(optionsCamera).then(function(imagePath) {
                var currentName = imagePath.replace(/^.*[\\\/]/, '');
                var d = new Date(),
                n = d.getTime(),
                newFileName =  n + ".jpg";
                var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
                    var selectedImage = cordova.file.dataDirectory + newFileName;
                    var selectedImageIos = selectedImage.replace('file://','')
                    $scope.feedbackData.feedbackImages.push(selectedImageIos);
                    console.log("styling success");
                }, function(error){
                    $scope.showAlert('Error', error.exception);
                    console.log("styling error");
                });
            },
            function(err){})
        };

        $scope.feedbackData.ratingObj = {
            iconOn: 'ion-ios-star',
            iconOff: 'ion-ios-star-outline',
            iconOnColor: 'rgb(211, 63, 68)',
            iconOffColor:  'rgb(211, 63, 68)',
            rating: 0,
            minRating: 0,
            readOnly: false
            // callback: function(rating, index) {
            // $scope.feedbackData.ratingObj.rating = rating;
            // }
        };

        $scope.ratingArr = [{
            value: 1,
            icon: 'img/unfilledStar.png'
        }, {
            value: 2,
            icon: 'img/unfilledStar.png'
        }, {
            value: 3,
            icon: 'img/unfilledStar.png'
        }, {
            value: 4,
            icon: 'img/unfilledStar.png'
        }, {
            value: 5,
            icon: 'img/unfilledStar.png'
        }];

        $scope.regex = {
            phone: /^([33]{2,2}|[44]{2,2}|[55]{2,2}|[66]{2,2}|[77]{2,2})[0-9]{6,6}$/
        };

        $scope.textArea = {
            cols: 28,
            rows: 1
        };

        $scope.doResize = function () {
            var maxrows = 5;
            var txt = $scope.feedbackData.message;
            var cols = $scope.textArea.cols;

            var arraytxt = txt.split('\n');
            var rows = arraytxt.length;

            for (i = 0; i < arraytxt.length; i++)
            rows += parseInt(arraytxt[i].length / cols);
            if (rows > maxrows)
            $scope.textArea.rows = maxrows;
            else
            $scope.textArea.rows = rows;
        };

        $scope.setRating = function(val) {
            var rtgs = $scope.ratingArr;
            for (var i = 0; i < rtgs.length; i++) {
                if (i < val) {
                    rtgs[i].icon = 'img/filledStar.png';
                } else {
                    rtgs[i].icon = 'img/unfilledStar.png';
                }
            };
            $scope.feedbackData.ratingObj.rating = val;
        };

    });
})();
