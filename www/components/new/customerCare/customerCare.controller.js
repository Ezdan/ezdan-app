'use strict';
(function() {
    angular.module('ezdanApp')
    .controller('customerCareController', function ($scope, $rootScope, $state) {
        $scope.goFeedback = function(feedbackId) {
            $state.go('feedback', { id: feedbackId });
        }
    });
})();
