'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('plusRequestController', function ($scope, $rootScope, $state, $stateParams, $ionicHistory, plusService, authentication) {
        $scope.serviceName;
        $scope.loggedIn = false;
        $scope.plusData = {
            serviceName: $stateParams.serviceObj.title,
            serviceId: $stateParams.serviceObj.id
        };
        $scope.inquirySubmitted = false;

        $scope.$watch(function() {
            return $rootScope.user;
        }, function(user) {
            if(typeof(user) !== 'undefined') {
                $scope.loggedIn = user.loggedIn;
                $scope.plusData.email = user.email;
                $scope.plusData.name = user.fullName;
                $scope.plusData.mobileNumber = user.mobile;
            }
        });

        $scope.textArea = {
            cols: 28,
            rows: 1
        };

        $scope.doResize = function () {
            var maxrows = 5;
            var txt = $scope.plusData.inquiryDetails;
            var cols = $scope.textArea.cols;
            var arraytxt = txt.split('\n');
            var rows = arraytxt.length;
            for (i = 0; i < arraytxt.length; i++)
            rows += parseInt(arraytxt[i].length / cols);
            if (rows > maxrows)
            $scope.textArea.rows = maxrows;
            else
            $scope.textArea.rows = rows;
        };

        $scope.inquirePlus = function(form) {
            $scope.inquirySubmitted = true;
            if(form.$valid) {
                $scope.load();
                var requestObj = {
                    Name: $scope.plusData.name,
                    Email: $scope.plusData.email,
                    ContactNumber: $scope.plusData.mobileNumber,
                    Details: $scope.plusData.inquiryDetails,
                    Service: $scope.plusData.serviceName,
                }
                var assumedToken = $rootScope.user.token || "";
                plusService.authorize(assumedToken).inquirePlus(requestObj).$promise.then(function(data) {
                    $rootScope.unload();
                    if(data && data.Added) {
                        $scope.popup(
                            'success',
                            $rootScope.langCounterpart('request_submitted') || 'Request Submitted',
                            $rootScope.langCounterpart('we_have_received_your_inquiry') || 'We have received your inquiry.'
                        );
                        $rootScope.goBack();
                        $rootScope.deleteBackHistory();
                    }
                }, function(error) {
                    $rootScope.unload();
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('failure') || 'Failure!',
                        $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                    );
                })
            }
        };
    });
})();
