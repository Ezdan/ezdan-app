'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('announcementsController', function ($scope, $rootScope, $state, $stateParams, $location, $ionicHistory, plusService, authentication) {
        $scope.goPromotions = function() {
            $location.path('content/2');
        }
        $scope.goEvents = function() {
            $location.path('content/10');
        }
    });
})();
