'use strict';
(function() {
    angular.module('ezdanApp').controller("propertyDetailController", function($scope, $rootScope, $state, $timeout, $ionicHistory, $ionicModal, $ionicActionSheet, propertyService, $ionicSlideBoxDelegate, shareImages, DB) {
        $scope.buildingUnitId = $state.params.BuildingUnitId;
        $scope.property = {};
        $scope.images = [];
        $scope.imagesUrls = [];

        document.addEventListener("deviceready", function() {
            $scope.deviceUuid = device.uuid;
            //console.log($scope.deviceUuid);
        }, false);

        $scope.GetProperty = function () {
            $rootScope.loading = true;
            $rootScope.load();
            var propKey = $scope.buildingUnitId + '_' + $rootScope.lang;
            if (!$rootScope.isOnline) {
                DB.find('property', 'key', propKey).then(function(res){
                    if (res && res.length > 0) {
                        $scope.property = JSON.parse(res[0].value);
                        if($scope.property.PropertyImages 
                            && $scope.property.PropertyImages instanceof Array) {
                            for (var i = 0; i < $scope.property.PropertyImages.length; i++) {
                                var photo = {
                                    src: $scope.property.PropertyImages[i]
                                };
                                // photo['src'] = ;
                                $scope.images.push(photo);
                            };
                            shareImages.set($scope.images);
                        }
                        $ionicSlideBoxDelegate.update();
                        $rootScope.loading = false;
                        $rootScope.unload();
                    } else {
                        $rootScope.unload();
                        $rootScope.goBack();
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('failure') || 'Failure!',
                            $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                        );
                    }
                });
            } else {
                DB.find('property', 'key', propKey).then(function(res){
                    if (res && res.length > 0) {
                        var dateDifference = Math.floor((new Date() - new Date(res[0].last_modified)) / (1000*60));
                        if (dateDifference >= 30) {
                            $scope.getPropertyPromise();
                        } else {
                            //Not Updating
                            $scope.property = JSON.parse(res[0].value);
                            if($scope.property.PropertyImages 
                                && $scope.property.PropertyImages instanceof Array) {
                                for (var i = 0; i < $scope.property.PropertyImages.length; i++) {
                                    var photo = {
                                        src: $scope.property.PropertyImages[i]
                                    };
                                    $scope.images.push(photo);
                                };
                                shareImages.set($scope.images);
                            }
                            $ionicSlideBoxDelegate.update();
                            $rootScope.loading = false;
                            $rootScope.unload();
                        }
                    } else {
                        $scope.getPropertyPromise();
                    }
                });
            }
        };

        $scope.getPropertyPromise = function () {
            var propKey = $scope.buildingUnitId + '_' + $rootScope.lang;
            propertyService.properties.findBybBuilgingId({ buildingNumber: $scope.buildingUnitId })
            .$promise.then(function (data) {
                console.log(data);
                $rootScope.loading = false;
                $scope.property = data;
                if($scope.property.PropertyImages 
                    && $scope.property.PropertyImages instanceof Array) {
                    for (var i = 0; i < $scope.property.PropertyImages.length; i++) {
                        var photo = {
                            src: $scope.property.PropertyImages[i]
                        };
                        $scope.images.push(photo);
                    };
                    shareImages.set($scope.images);
                }
                $ionicSlideBoxDelegate.update();
                DB.find('property', 'key', propKey).then(function(res){
                    if (res && res.length == 0) {
                        DB.insert('property',[propKey,JSON.stringify($scope.property),new Date()]);
                    } else {
                        DB.updateRow('property','value',JSON.stringify($scope.property),'key',propKey);
                        DB.updateRow('property','last_modified',new Date(),'key',propKey);
                    }
                });
                $scope.imageForShare = $scope.property.PropertyImages.length > 0 ? encodeURI($scope.property.PropertyImages[0]) : null
                $rootScope.unload();
            }, function (error) {
                $rootScope.unload();
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
                $ionicHistory.goBack();
            });
        };

        $scope.GetProperty();

        $scope.goAppointments = function() {
            $state.go('appointments');
        }

        $scope.propertyPresentation = '';
        $scope.$watch('property', function(newV, oldV) {
            if(typeof(newV) !== 'undefiend' && Object.keys(newV).length > 0) {
                $scope.propertyPresentation = newV.PropertyType.toLowerCase() === 'commercial' ? 
                    $rootScope.langCounterpart('property_presentation_commercial') :
                    $rootScope.langCounterpart('property_presentation');
                $scope.propertyPresentation = $scope.propertyPresentation.replace('#1#', newV.NumberOfBedrooms)
                .replace('#2#', newV.PropertyType)
                .replace('#3#', newV.MarketingName);
            }
        });
        
        $scope.online = true;
        $scope.$watch(function() {
            return $rootScope.isOnline;
        }, function(isOnline) {
            $scope.online = isOnline;
        });

        $scope.shareFacebook = function() {
            console.log($scope.propertyLink);
            window.plugins.socialsharing.shareViaFacebook(
                null,
                null, /* img */
                $scope.propertyLink,
                function() {},
                function(error) {
                    $scope.popupMissingApp();
                }
            );
        };

        $scope.shareTwitter = function() {
            window.plugins.socialsharing.shareViaTwitter(
                $scope.propertyPresentationCopy,
                $scope.images[0].src,
                $scope.propertyLink,
                function() {},
                function(error) {
                    $scope.popupMissingApp();
                }
            );
        };

        $scope.shareWhatsApp = function() {
            window.plugins.socialsharing.shareViaWhatsApp(
                $scope.propertyPresentationCopy,
                $scope.images.length > 0 ? $scope.images[0].src : null,
                $scope.propertyLink,
                function() {},
                function(error) {
                    $scope.popupMissingApp();
                }
            );
        };

        $scope.shareMail = function() {
            var hasRoomsStr = $scope.property.NumberOfBedrooms > 0 ? 
            ($scope.property.NumberOfBedrooms 
            + ' '
            + ($rootScope.langCounterpart('bedrooms_in') || 'bedrooms in')
            + ' ') : 
            '';
            var subject = ($rootScope.langCounterpart('ezdan_real_estate') || 'Ezdan Real Estate') 
            + ' - ' 
            + hasRoomsStr
            + $scope.property.MarketingName;
            window.plugins.socialsharing.shareViaEmail(
                $scope.propertyPresentationCopy,
                subject,
                null,
                null,
                null,
                $scope.images[0].src,
                function() {},
                function(error) {
                    $scope.popupMissingApp();
                }
            );
        };

        $scope.prepareToShare = function() {
            if($scope.online) {
                console.log($scope.images);
                $scope.propertyLink = 'http://ezdanrealestate.qa/property-details.aspx?id=' + $scope.property._DocumnetHash;
                console.log($scope.propertyLink);
                $scope.propertyPresentationCopy = angular.copy($scope.propertyPresentation);
                $scope.concatPropertyDetails($scope.propertyPresentationCopy);
                // if($scope.imagesUrls.length === 0) {
                //     $rootScope.load();
                //     propertyService.properties.getPropertyInterceptedImages({
                //         id: $scope.property._DocumnetHash,
                //         uuid: $scope.deviceUuid || $scope.property._DocumnetHash
                //     })
                //     .$promise.then(function(data) {
                //         $rootScope.unload();
                //         if(data && data instanceof Array && data.length > 0) {
                //             $scope.imagesUrls = data;
                //         }
                //         else {
                //             $scope.imagesUrls = $scope.images.map(function(image) {
                //                 return image.src;
                //             });
                //         }
                //         //console.log($scope.imagesUrls);
                //         $scope.showActionsheet();
                //     }, function(error) {
                //         //console.log(error);
                //         $rootScope.unload();
                //         $scope.imagesUrls = $scope.images.map(function(image) {
                //             return image.src;
                //         });
                //     });
                // }
                // else {
                    $scope.showActionsheet();
                // }
            }
            else {
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_your_internet_connection') || 'Please check your internet connection.'
                );
            }
        }

        $scope.showActionsheet = function() {
            //console.log($scope.propertyPresentationCopy);
            $ionicActionSheet.show({
                titleText: 'Share Via',
                buttons: [
                    {
                        text: '<i class="icon ion-social-facebook"></i> Facebook'
                    },
                    {
                        text: '<i class="icon ion-social-twitter"></i> Twitter'
                    },
                    {
                        text: '<i class="icon ion-social-whatsapp"></i> WhatsApp'
                    },
                    {
                        text: '<i class="icon ion-ios-email"></i> Mail'
                    }
                ],
                // destructiveText: 'Delete',
                cancelText: 'Cancel',
                cancel: function() {},
                buttonClicked: function(index) {
                    switch(index) {
                        case 0:
                        $scope.shareFacebook();
                        break;
                        case 1:
                        $scope.shareTwitter();
                        break;
                        case 2:
                        $scope.shareWhatsApp();
                        break;
                        default:
                        $scope.shareMail();
                    }
                    return true;
                },
                destructiveButtonClicked: function() { return true; }
            });
        };

        $scope.popupMissingApp = function() {
            $rootScope.popup(
                'error',
                $rootScope.langCounterpart('failure') || 'Failure!',
                $rootScope.langCounterpart('please_check_that_this_app_is_installed') || 'Please check that this app is installed.'
            );
        };

        $scope.showImages = function(index) {
            $scope.activeSlide = index;
            $scope.showModal('templates/gallery-zoomview.html');
        };

        $scope.showModal = function(templateUrl) {
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: $scope
            }).then(function(modal) {
                $rootScope.modalActive = true;
                $rootScope.modal = modal;
                $rootScope.modal.show();
            });
        };

        $scope.concatPropertyDetails = function(presentation) {
            var p = $scope.property;
            if($rootScope.lang === 'Arabic') {
                var hasRoomsStrAr = p.NumberOfBedrooms > 0 ? 
                    ('عدد الغرف: ' 
                    + p.NumberOfBedrooms 
                    + '\n') : 
                    '';
                $scope.propertyPresentationCopy = presentation
                    + '\n\n'
                    + hasRoomsStrAr
                    + 'نوع الوحدة: '
                    + ($rootScope.langCounterpart(p.PropertyType.toLowerCase()) || p.PropertyType)
                    + '\nموقع الوحدة: ' 
                    + ($rootScope.langCounterpart(p.City.toLowerCase()) || p.City) 
                    + '\nمفروشة: ' 
                    + ($rootScope.langCounterpart(p.Furnished.toLowerCase()) || p.Furnished)
                    + '\nطريقة الدفع: ' 
                    + ($rootScope.langCounterpart((p.PriceFor + 'ly').toLowerCase()) || p.PriceFor + 'ly');
            }
            else {
                var hasRoomsStr = p.NumberOfBedrooms > 0 ? 
                ('Number of bedrooms: ' 
                + p.NumberOfBedrooms 
                + '\n') : 
                '';
                $scope.propertyPresentationCopy = presentation
                    + '\n\n'
                    + hasRoomsStr
                    + 'Property type: '
                    + p.PropertyType 
                    + '\nProperty Location: ' 
                    + p.City 
                    + '\nFurnished: ' 
                    + p.Furnished 
                    + '\nPayment method: ' 
                    + p.PriceFor + 'ly';
            }
        }

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            if(fromState.name === 'propertyDetail' 
                && typeof($scope.imagesUrls) !== 'undefined' 
                && $scope.imagesUrls.length > 0) 
                $scope.deleteInterceptedImages();
        });

        $scope.deleteInterceptedImages = function() {
            propertyService.properties.deletePropertyInterceptedImages({
                uuid: $scope.deviceUuid || $scope.property._DocumnetHash
            })
            .$promise.then(function(data) {}, function(error) {
                //console.log(error);
            });
        }
    });
})();
