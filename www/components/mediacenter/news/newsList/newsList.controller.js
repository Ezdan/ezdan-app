'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('newsListController', function($scope, $rootScope, newsService,$state,$location, DB) {

        $scope.getNews = function () {
            $rootScope.loading = true;
            $rootScope.load();
            var allNewsKey = "allNews_" + $rootScope.lang;
            if (!$rootScope.isOnline) {
                DB.find('newsList', 'key', allNewsKey).then(function(res){
                    if (res.length > 0) {
                        $scope.news = JSON.parse(res[0].value);
                        for (var i = 0; i < $scope.news.length; i++) {
                            var d = new Date($scope.news[i].newsdate);
                            if ($rootScope.lang === 'Arabic') {
                                $scope.news[i].newsdate = d.toLocaleDateString('ar-EG');
                            } else {
                                $scope.news[i].newsdate = d.toLocaleDateString('en-GB');
                            }
                        }
                        $rootScope.loading = false;
                        $rootScope.unload();
                    } else {
                        $rootScope.loading = false;
                        $rootScope.unload();
                        // $rootScope.goBack();
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('failure') || 'Failure!',
                            $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                        );
                    }
                });
            } else {
                DB.find('newsList', 'key', allNewsKey).then(function(res){
                    if (res.length > 0) {
                        var dateDifference = Math.floor((new Date() - new Date(res[0].last_modified)) / (1000*60));
                        if (dateDifference >= 30) {
                            $scope.getNewsPromise();
                        } else {
                            $scope.news = JSON.parse(res[0].value);
                            for (var i = 0; i < $scope.news.length; i++) {
                                var d = new Date($scope.news[i].newsdate);
                                if ($rootScope.lang === 'Arabic') {
                                    $scope.news[i].newsdate = d.toLocaleDateString('ar-EG');
                                } else {
                                    $scope.news[i].newsdate = d.toLocaleDateString('en-GB');
                                }
                            }
                            $rootScope.loading = false;
                            $rootScope.unload();
                        }
                    } else {
                        $scope.getNewsPromise();
                    }
                });

            }
        };

        $scope.getNewsPromise = function () {
            newsService.getNews({"lang":$rootScope.lang, "id":"0"}).$promise.then(function (data) {
                if (data.News && data.News.length > 0) {
                    //check if active is true is missing
                    $scope.news = data['News'];
                    for (var i = 0; i < $scope.news.length; i++) {
                        var d = new Date($scope.news[i].newsdate);
                        if ($rootScope.lang === 'Arabic') {
                            $scope.news[i].newsdate = d.toLocaleDateString('ar-EG');
                        } else {
                            $scope.news[i].newsdate = d.toLocaleDateString('en-GB');
                        }
                    }
                }
                var allNewsKey = "allNews_" + $rootScope.lang;
                DB.find('newsList', 'key', allNewsKey).then(function(res){
                    if (res.length == 0) {
                        DB.insert('newsList',[allNewsKey,JSON.stringify($scope.news),new Date()]);
                    } else {
                        DB.updateRow('newsList','value',JSON.stringify($scope.news),'key',allNewsKey);
                        DB.updateRow('newsList','last_modified',new Date(),'key',allNewsKey);
                    }
                });
                $rootScope.loading = false;
                $rootScope.unload();
            }, function(error) {
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
                $rootScope.loading = false;
                $rootScope.unload();
            });
        };

        $scope.getNews();

    });
})();
