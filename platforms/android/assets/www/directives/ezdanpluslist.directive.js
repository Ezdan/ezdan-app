'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('ezdanpluslist', function() {

        return{
            templateUrl: 'components/cms/templates/ezdanpluslist.html'
        };

    });
})();
