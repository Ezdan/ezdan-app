'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('thumbList', function() {

        return{
            templateUrl: 'components/cms/templates/thumblist.html'
        };

    });
})();
