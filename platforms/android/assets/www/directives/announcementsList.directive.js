'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('announcementsList', function() {

        return{
            templateUrl: 'components/cms/templates/announcementsList.html'
        };

    });
})();
