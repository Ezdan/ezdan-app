'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('classifiedDetail', function() {

        return{
            templateUrl: 'components/cms/templates/classifieddetail.html'
        };

    });
})();
