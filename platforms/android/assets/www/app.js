'use strict';
(function () {
    angular
        .module('ezdanApp')
        .config(function ($ionicConfigProvider) {
            $ionicConfigProvider.views.swipeBackEnabled(false);
        })
        .run(['$rootScope', '$location', '$ionicSideMenuDelegate', '$ionicPlatform', 'user', 'dict', '$cordovaSQLite', '$cordovaSplashscreen', 'appStart', 'DB', 'authentication', 'homeService', function ($rootScope, $location, $ionicSideMenuDelegate, $ionicPlatform, user, dict, $cordovaSQLite, $cordovaSplashscreen, appStart, DB, authentication, homeService) {
            $rootScope.dict = [];
            $rootScope.pendingPhoneAuth = false;
            $rootScope.bannerImgLoaded = false;
            $rootScope.bannerImg;
            $rootScope.countryCode = '+974';
            $ionicPlatform.ready(function () {
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
                    // if(window.StatusBar) {
                    //     StatusBar.styleDefault();
                    // }
                    cordova.plugins.Keyboard.disableScroll(true);
                    window.addEventListener('native.keyboardshow', $rootScope.keyboardShowHandler);
                    var popupBottomOffset = $(".popup-container.popup-showing .popup").css('bottom');
                    window.addEventListener('native.keyboardhide', $rootScope.keyboardHideHandler);

                }

                // if(window.StatusBar) {
                //     StatusBar.styleLightContent();
                // }

                $rootScope.appInited = false;

                if (!window.cordova) {
                    DB.init();
                    dict.GetDictionary().then(function (data) {
                        $rootScope.dict = data;
                        if (navigator && navigator.splashscreen) navigator.splashscreen.hide();
                    });
                    // authentication.setAuth();
                    appStart.startState();
                }
                else {
                    document.addEventListener("deviceready", function () {
                        DB.init();
                        dict.GetDictionary().then(function (data) {
                            $rootScope.dict = data;
                            if (navigator && navigator.splashscreen) navigator.splashscreen.hide();
                        });
                        // authentication.setAuth();
                        appStart.startState();
                        window.open = cordova.InAppBrowser.open;

                    }, false);
                }

                $ionicSideMenuDelegate.canDragContent(false);
            });

            $rootScope.keyboardShowHandler = function (e) {
                $(".keyboard-open-height").css('bottom', e.keyboardHeight);
                $(".popup-container.popup-showing .popup").css('bottom', e.keyboardHeight);
                $(".modal.active").css('bottom', e.keyboardHeight);
            }

            $rootScope.keyboardHideHandler = function (e) {
                $(".keyboard-open-height").css('bottom', 0);
                $(".popup-container.popup-showing .popup").css('bottom', '12vh');
                $(".modal.active").css('bottom', 0);
            }

            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) { });
        }])

        .controller('mainController', function ($rootScope, $location, $scope, $state, $timeout, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup, $ionicHistory, $ionicPlatform, $ionicFilterBar, $ionicModal, $ionicPickerI18n, localStorageService, user, DB, authentication) {
            $rootScope.regex = {
                email: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                number: /^(\d)$/,
                logInPassword: /^.{6,}$/,
                phone: /^[0-9]{8,8}$/,
                // phone: /^([33]{2,2}|[44]{2,2}|[55]{2,2}|[66]{2,2}|[77]{2,2})[0-9]{6,6}$/,
                fullName: /^[ِA-Za-z\u0621-\u064A]([-']?[A-Za-z\u0621-\u064A]+)*( [A-Za-z\u0621-\u064A]([-']?[A-Za-z\u0621-\u064A]+)*)+$/,
                password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d$@$!%*?!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{6,}$/
            };

            $scope.goAnnouncements = function () {
                $location.path("content/2");
            };

            $rootScope.goHome = function () {
                $rootScope.deleteBackHistory();
                $state.go('home');
            };

            $rootScope.langCounterpart = function (entry) {
                for (var i = 0; i < $rootScope.dict.length; i++) {
                    if ($rootScope.dict[i].key === entry) {
                        if ($rootScope.lang == "Arabic") {
                            return $rootScope.dict[i].arabic;
                        }
                        else {
                            return $rootScope.dict[i].english;
                        }
                    }
                }
            };

            $scope.langChangedFromMenu = false;

            $scope.$watch('langChangedFromMenu', function (newV, oldV) {
                $scope.$watch(function () {
                    return $ionicSideMenuDelegate.isOpen();
                },
                    function (isOpen) {
                        if (newV && !oldV && $scope.langChangedFromMenu && !isOpen) {
                            $rootScope.lang = $rootScope.lang === 'English' ? 'Arabic' : 'English';
                            if (typeof ($rootScope.user) !== 'undefined') {
                                DB.update('user', 'language', $rootScope.lang);
                            }
                            $scope.langChangedFromMenu = false;
                        }
                    });
            })

            $rootScope.switchLanguage = function (fromMenu) {
                if (fromMenu) {
                    $scope.langChangedFromMenu = true;
                    return;
                }
                $rootScope.lang = $rootScope.lang === 'English' ? 'Arabic' : 'English';
            }

            $rootScope.deleteBackHistory = function () {
                $ionicHistory.nextViewOptions({
                    historyRoot: true,
                    disableBack: true,
                    // disableAnimate: true
                });
                $ionicHistory.clearCache();
                $ionicHistory.clearHistory();
            };

            $rootScope.$ionicGoBack = function (backCount) {
                if ($state.is('propertyDetail')) {
                    $rootScope.deleteBackHistory();
                    $state.go('properties', { resetFilter: false });
                }
                else{
                    $ionicHistory.goBack(backCount);
                }
            };
            $rootScope.goBack = function () {
                var backView = $ionicHistory.backView();
                if (backView != null) {
                    backView.go();
                }
                else {
                    $ionicHistory.nextViewOptions({
                        historyRoot: true
                    });
                    $state.go('home');
                }
            };

            $ionicPlatform.registerBackButtonAction(function (e) {
                $ionicLoading.hide();
                if ($('.popup-container.active').length) {
                    e.preventDefault();
                    $('.popup-container.active').find('button.button-default').trigger('click');
                    return false;
                }
                if ($rootScope.modalActive == true) {
                    $rootScope.modalActive = false;
                    $scope.closeModal();
                } else if ($state.is('land') || ($state.is('home') && $rootScope.userType === 'loggedIn')) {
                    navigator.app.exitApp();
                } else if ($state.is('login') || $state.is('register') || ($state.is('home') && $rootScope.userType === 'guest')) {
                    $state.go('land');
                } else if ($state.is('setPassword')) {
                    $state.go('forgotPassword');
                } else if ($state.is('forgotPassword')) {
                    $state.go('login');
                } else if ($state.is('propertyDetail')) {
                    $rootScope.deleteBackHistory();                    
                    $state.go('properties', { resetFilter: false });
                } else if ($state.is('properties')) {
                    $state.go('home');
                } else if ($ionicHistory.backView() != null) {
                    $ionicHistory.goBack();
                } else {
                    $ionicHistory.nextViewOptions({
                        disableBack: true,
                        disableAnimate: true,
                        historyRoot: true
                    });
                    $state.go('home');
                }
            }, 600);

            $rootScope.menuDirection = 'left';

            $rootScope.phoneAuthModal;

            $scope.$watch(function () {
                return $rootScope.pendingPhoneAuth
            }, function (isPending) {
                if (isPending) {
                    $ionicModal.fromTemplateUrl('templates/phoneAuth.html', {
                        scope: $scope,
                    }).then(function (modal) {
                        $rootScope.phoneAuthModal = modal;
                        $rootScope.phoneAuthModal.show("fadeInDown");
                    });
                }
            });

            $scope.$watch(function () {
                return $rootScope.lang
            }, function (language) {
                $rootScope.menuDirection = language === 'Arabic' ? 'right' : 'left';
            });

            $scope.$watch(function () {
                return $rootScope.user
            }, function (user) {
                if (typeof (user) !== 'undefined') {
                    if (user.imageURL && user.imageURL !== 'null') {
                        $rootScope.userImage = user.imageURL;
                    }
                    else {
                        $rootScope.userImage = 'img/avatar-man.jpg';
                    }
                }
            });

            $rootScope.closeModal = function () {
                $scope.modal.hide();
                $scope.modal.remove()
            };

            $scope.logout = function () {
                DB.delete('user');
                authentication.setAuth();
                $ionicHistory.nextViewOptions({
                    historyRoot: true,
                    disableBack: true,
                    disableAnimate: true
                });
                $ionicHistory.clearCache();
                $ionicHistory.clearHistory();
                $rootScope.unload();
                // $state.go("chooseLanguage");
                $state.go("land");
                $scope.$on('$ionicView.enter', function (viewInfo, state) {
                    // if(state.stateName === 'chooseLanguage') {
                    if (state.stateName === 'land') {
                        $rootScope.userType = 'guest';
                    }
                });
            };

            $rootScope.go = function (state, params) {
                $state.go(state, params);
            };

            $rootScope.loading;
            $rootScope.load = function () {
                $ionicLoading.show({
                    template: '<p>' + ($rootScope.langCounterpart('loading') || 'Loading...') + '</p><ion-spinner icon="android"></ion-spinner>'
                });
            };

            $rootScope.loadArabic = function () {
                $ionicLoading.show({
                    template: '<p>' + 'تحميل...' + '</p><ion-spinner icon="android"></ion-spinner>'
                });
            };

            $rootScope.unload = function () {
                $ionicLoading.hide();
            };

            $rootScope.popupOpen = false;
            $rootScope.popup = function (type, title, template) {
                if (!$rootScope.popupOpen) {
                    $rootScope.popupOpen = true;
                    $ionicPopup.show({
                        template: template,
                        title: title,
                        cssClass: title == null ? 'popupNoHeader' : null,
                        buttons: [
                            {
                                text: '<b>' + ($rootScope.langCounterpart('ok') || 'OK') + '</b>',
                                type: 'button-positive',
                                onTap: function (e) {
                                    $rootScope.popupOpen = false;
                                }
                            }
                        ]
                    });
                }
            };

            $scope.toggleMenu = function () {
                $rootScope.lang === 'Arabic' ? $ionicSideMenuDelegate.toggleRight() : $ionicSideMenuDelegate.toggleLeft();
            }
        });
})();
//
