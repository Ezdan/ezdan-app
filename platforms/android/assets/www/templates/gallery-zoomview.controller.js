'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('galleryZoomViewController', function($scope, $rootScope, shareImages, $ionicBackdrop, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate, $state, $ionicPlatform, $ionicHistory) {

        $scope.zoomMin = 1;
        $scope.Images = new Array();
        $scope.Images = shareImages.get();
        console.log($scope.Images);

        $scope.closeModal = function() {
            $scope.modal.hide();
            $scope.modal.remove()
        };
        
        $scope.zoomOnDoubleTap = function(slide) {
            $scope.zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
            if ($scope.zoomFactor == $scope.zoomMin) {
                $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).zoomBy(2,true);
            } else {
                $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).zoomBy(0.5,true);
            }
        };

        $scope.updateSlideStatus = function(slide) {
            $scope.zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
            if ($scope.zoomFactor == $scope.zoomMin) {
                $ionicSlideBoxDelegate.enableSlide(true);
            } else {
                $ionicSlideBoxDelegate.enableSlide(false);
            }
        };

    })
    .directive('srcImg', function(){
        return function(scope, element, attrs){
            var url = attrs.srcImg;
            element.css({
                'background-image': 'url("' + url + '")'
            });
        };
    });
})();
