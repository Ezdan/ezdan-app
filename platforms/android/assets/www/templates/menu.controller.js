'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('menuController', function ($scope, contentService, $rootScope, $state, $ionicSideMenuDelegate, $ionicHistory, $ionicFilterBar, $timeout, searchService, DB) {
        $rootScope.menuDirection = 'left';
        $scope.$watch(function() {
            return $rootScope.user
        }, function(user) {
            if(typeof(user) !== 'undefined') {
                if(user.imageURL && user.imageURL !== 'null') {
                    $scope.profileImage = user.imageURL;
                }
                else {
                    $scope.profileImage = 'img/avatar-man.jpg';
                }
            }
        });
        $scope.$watch(function() {
            return $rootScope.lang
        }, function(language) {
            if(typeof(language) !== 'undefined') {
                $rootScope.menuDirection = language === 'Arabic' ? 'right' : 'left';
            }
        });
        $scope.menuItems = [
            {
                href: "#/home",
                icon: "img/icons/home.png",
                key: 'home',
                alt: "Home",
                offlineSupport: true
            },
            {
                href: "#/ourServices",
                icon: "img/icons/our-services.png",
                key: 'our_services',
                alt: "Our Services",
                offlineSupport: true
            },
            {
                href: "#/request",
                icon: "img/icons/service-request.png",
                key: 'service_request',
                alt: "Service Request",
                offlineSupport: false
            },
            {
                href: "#/properties/true",
                icon: "img/icons/property-search.png",
                key: 'property_search',
                alt: "Propert Search",
                offlineSupport: true
            },
            {
                href: "#/cardTypes",
                icon: "img/icons/cards.png",
                key: 'discount_cards',
                alt: "EDC Program",
                offlineSupport: true
            },
            {
                href: "#/appointments",
                icon: "img/icons/calendar.png",
                key: 'appointments',
                alt: "Appointments",
                offlineSupport: false
            },
            {
                href: "#/announcements",
                icon: "img/icons/announcments.png",
                key: 'announcements',
                alt: "Announcements",
                offlineSupport: false
            }
        ];

        $scope.guestMenuItems = [
            {
                href: "#/home",
                icon: "img/icons/home.png",
                key: 'home',
                alt: "Home",
                offlineSupport: true
            },
            {
                href: "#/ourServices",
                icon: "img/icons/our-services.png",
                key: 'our_services',
                alt: "Our Services",
                offlineSupport: true
            },
            {
                href: "#/properties/true",
                icon: "img/icons/property-search.png",
                key: 'property_search',
                alt: "Propert Search",
                offlineSupport: true
            },
            {
                href: "#/cardTypes",
                icon: "img/icons/cards.png",
                key: 'discount_cards',
                alt: "EDC Program",
                offlineSupport: true
            },
            {
                href: "#/appointments",
                icon: "img/icons/calendar.png",
                key: 'appointments',
                alt: "Appointments",
                offlineSupport: false
            },
            {
                href: "#/announcements",
                icon: "img/icons/announcments.png",
                key: 'announcements',
                alt: "Announcements",
                offlineSupport: false
            }
        ];

        $scope.$watch(function() {
            return $rootScope.userType
        }, function(userType) {
            if(typeof(userType) !== 'undefined') {
                if(userType === 'guest') {
                    $scope.chosenMenu = $scope.guestMenuItems;
                } else if (userType === 'loggedIn') {
                    $scope.chosenMenu = $scope.menuItems;
                }
            }
        });

        $scope.$watch(function() {
            return $rootScope.lang
        }, function(language) {
            if(typeof(language) !== 'undefined') {
                if(language === 'Arabic') {
                    $rootScope.rightMenuEnable = true;
                    $rootScope.leftMenuEnable = false;
                } else if (language === 'English') {
                    // console.log(language);
                    $rootScope.rightMenuEnable = false;
                    $rootScope.leftMenuEnable = true;                }
                }
            });

            $scope.selectMenuItem = function(index) {
                for(var i = 0; i < $scope.chosenMenu.length; i++) {
                    if(i === index) continue;
                    $scope.chosenMenu[i].selected = false;
                }
                $scope.chosenMenu[index].selected = true;
                $timeout(function(){
                    $scope.chosenMenu[index].selected = false;
                }, 500);
            };

            $scope.goProfile = function() {
                if ($rootScope.lang == 'Arabic') {
                    $ionicSideMenuDelegate.toggleRight();
                } else {
                    $ionicSideMenuDelegate.toggleLeft();
                }
                $state.go('userProfile');
            };

            $scope.callNumberPromise = function () {
                contentService.getContent(
                    {
                        "id": 6,
                        "lang": $rootScope.lang
                    })
                    .$promise.then(function (data) {
                        if (data) {
                            $scope.data = data;
                            $scope.contactNumber = $scope.data.content.properties.contactPhone.value;
                            location.href= 'tel: ' + $scope.contactNumber;
                            DB.find('content', 'key', 6).then(function(res){
                                if (res.length == 0) {
                                    DB.insert('content',[6,JSON.stringify(data),new Date()]);
                                } else {
                                    DB.updateRow('content','value',JSON.stringify(data),'key',6);
                                    DB.updateRow('content','last_modified',new Date(),'key',6);
                                }
                            });
                        }
                    });
                };

                $scope.callNow = function () {
                    if(!$rootScope.isOnline) {
                        DB.find('content', 'key', 6).then(function(res){
                            if (res.length > 0) {
                                $scope.data = JSON.parse(res[0].value);
                                $scope.contactNumber = $scope.data.content.properties.contactPhone.value;
                                location.href= 'tel: ' + $scope.contactNumber;

                            } else {
                                $rootScope.popup(
                                    'error',
                                    $rootScope.langCounterpart('failure') || 'Failure!',
                                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                                );
                            }
                        });

                    } else {
                        DB.find('content', 'key', 6).then(function(res){
                            if (res.length > 0) {
                                var dateDifference = Math.floor((new Date() - new Date(res[0].last_modified)) / (1000*60));
                                if (dateDifference >= 30) {
                                    $scope.callNumberPromise();
                                } else {
                                    $scope.data = JSON.parse(res[0].value);
                                    $scope.contactNumber = $scope.data.content.properties.contactPhone.value;
                                    location.href= 'tel: ' + $scope.contactNumber;
                                }
                            } else {
                                $scope.callNumberPromise();
                            }

                        });
                    }
                };

                $scope.goLand = function() {
                    $ionicHistory.nextViewOptions({
                        historyRoot: true
                    });
                    //  $ionicSideMenuDelegate.canDragContent(false);
                    $state.go('land');
                }

            });
        })();
