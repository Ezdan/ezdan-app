'use strict';
(function() {
    angular.module('ezdanApp')
    .factory('appStart', appStart);

    appStart.$inject = ['$resource','DB','$rootScope','user', 'authentication'];

    function appStart($resource,DB, $rootScope, user, authentication) {

        var UserService = {};
        var startState = function(){
            var localDbResultCount = 0;
            DB.get('user').then(function(data){
                localDbResultCount = data.length;
                if (localDbResultCount == undefined || localDbResultCount == 0) {

                    authentication.setAuth();
                    user.setUserType('guest');
                    // $rootScope.go("chooseLanguage");
                    $rootScope.go("land");
                    // if ($rootScope.isOnline) {
                    //     $rootScope.popup(
                    //         'warning',
                    //         $rootScope.langCounterpart('warning') || 'Warning!',
                    //         $rootScope.langCounterpart('warning_message') || 'You are using the app in offline mode, some features might not be supported.'
                    //     );
                    // }
                    DB.get('tempUser').then(function(data) {
                        if(data.length && data.length > 0) {
                            $rootScope.pendingPhoneAuth = true;
                        }
                    })
                } else {
                    $rootScope.user = data[0];
                    $rootScope.user.token = data[0].access_token;
                    if ($rootScope.isOnline) {
                        user.api().validateContract().$promise.then(function(validationData){
                            if(validationData.valid === true) {
                                DB.update('user','language',validationData.user.Language);
                                DB.update('user','notification',validationData.user.Notification);
                                DB.update('user','fullName',validationData.user.FullName);
                                DB.update('user','title',validationData.user.Title);
                                DB.update('user','mobile',validationData.user.PhoneNumber);
                                DB.update('user','imageURL',validationData.user.ImageUrl);
                                DB.update('user','email',validationData.user.Email);

                                authentication.setAuth();

                                user.setUserType('loggedIn');
                                $rootScope.go("home");
                            } else if (validationData.hasOwnProperty('error')) {
                                if (validationData.error === 'ezdna-error') {
                                    $rootScope.popup(
                                        'error',
                                        $rootScope.langCounterpart('expired_contract') || 'Contract Expired',
                                        $rootScope.langCounterpart('expired_contract_message') || 'Your contract has expired, please contact us to solve this issue.'
                                    );
                                } else if (validationData.error === 'Ax Exception') {
                                    $rootScope.popup(
                                        'error',
                                        $rootScope.langCounterpart('server_error') || 'Server Error!',
                                        $rootScope.langCounterpart('internal_server_problem') || 'Internal Server Error.'
                                    );
                                }
                                else {
                                    $rootScope.popup(
                                        'error',
                                        $rootScope.langCounterpart('failure') || 'Failure!',
                                        $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                                    );
                                }

                                DB.delete('user');
                                authentication.setAuth();
                                user.setUserType('guest');
                                $rootScope.deleteBackHistory();
                                // $rootScope.go("chooseLanguage");
                                $rootScope.go("land");
                            }
                        },function(error){
                            DB.delete('user');
                            authentication.setAuth();
                            user.setUserType('guest');
                            $rootScope.deleteBackHistory();
                            // $rootScope.go("chooseLanguage");
                            $rootScope.go("land");
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('unknown') || 'Unknown!',
                                $rootScope.langCounterpart('unknown_exception') || 'An unknown exception has occurred.'
                            );
                        });
                    } else {
                        authentication.setAuth();
                        user.setUserType('loggedIn');
                        $rootScope.go("home");
                        // $rootScope.popup(
                        //     'warning',
                        //     $rootScope.langCounterpart('warning') || 'Warning!',
                        //     $rootScope.langCounterpart('warning_message') || 'You are using the app in offline mode, some features might not be supported.'
                        // );
                    }
                }
                $rootScope.appInited = true;
            },function(error){

            });
        };


        appStart.startState = startState;
        return appStart;
    }
})();
