'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('albumsListController', function($scope, $rootScope, shareImages, photogalleryService, $ionicHistory, $ionicModal, DB) {
        if ($rootScope.lang === 'English') {
            $scope.language = '1';
        } else if ($rootScope.lang === 'Arabic') {
            $scope.language = '2';
        }

        $scope.listAlbums = function () {
            $rootScope.loading = true;
            $rootScope.load();
            var albumsKey = "allAlbums" + '_' + $rootScope.lang;
            if (!$rootScope.isOnline) {
                DB.find('albumsList', 'key', albumsKey).then(function(res){
                    if (res.length > 0) {
                        $scope.albums = JSON.parse(res[0].value);
                        $scope.loadImages($scope.albums[0].albumid);
                        $rootScope.loading = false;
                        $rootScope.unload();
                    } else {
                        $rootScope.loading = false;
                        $rootScope.unload();
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('failure') || 'Failure!',
                            $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                        );
                    }
                });
            } else {
                DB.find('albumsList', 'key', albumsKey).then(function(res){
                    if (res.length > 0) {
                        var dateDifference = Math.floor((new Date() - new Date(res[0].last_modified)) / (1000*60));
                        if (dateDifference >= 30) {
                            $scope.listAlbumsPromise();
                        } else {
                            $scope.albums = JSON.parse(res[0].value);
                            $scope.loadImages($scope.albums[0].albumid);
                            $rootScope.loading = false;
                            $rootScope.unload();
                        }
                    } else {
                        $scope.listAlbumsPromise();
                    }
                });
            }
        };

        $scope.listAlbumsPromise = function () {
            var albumsKey = "allAlbums" + '_' + $rootScope.lang;
            photogalleryService.getAlbums({"lang":$scope.language}).$promise.then(function (data) {

                $scope.albums = data['PhotoGalleryAlbums'];
                // $rootScope.loading = false;
                $scope.loadImages($scope.albums[0].albumid);
                // $rootScope.unload();

                DB.find('albumsList', 'key', albumsKey).then(function(res){
                    if (res.length == 0) {
                        DB.insert('albumsList',[albumsKey,JSON.stringify($scope.albums),new Date()]);
                    } else {
                        DB.updateRow('albumsList','value',JSON.stringify($scope.albums),'key',albumsKey);
                        DB.updateRow('albumsList','last_modified',new Date(),'key',albumsKey);
                    }
                });
                $rootScope.loading = false;
                $rootScope.unload();
            }, function(error) {
                $rootScope.loading = false;
                $rootScope.unload();
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
            });
        };

        $scope.setInitTab = function(){
            ($('.globalTabSub')[0]).className+= " active";
        };

        $scope.selectedIndex = function(index,event) {
            $('.globalTabSub').removeClass('active');
            $(event.target.parentElement).addClass('active');
        };

        $scope.loadImages = function (id) {
            $rootScope.loading = true;
            $rootScope.load();
            var albumKey = id + '_' + $rootScope.lang;
            if (!$rootScope.isOnline) {
                DB.find('album', 'key', albumKey).then(function(res){
                    if (res.length > 0) {
                        $scope.albumImages = JSON.parse(res[0].value);
                        $scope.albumTitle = $scope.albumImages[0]['albumtitle'];
                        var photos = new Array();
                        for (var i = 0; i < $scope.albumImages.length; i++) {
                            var photo = {};
                            photo['src'] = $scope.albumImages[i]['big'];
                            photo['sub'] = $scope.albumImages[i]['phototitle'];
                            photos.push(photo);
                        };
                        shareImages.set(photos);

                        $rootScope.loading = false;
                        $rootScope.unload();
                    } else {
                        $rootScope.loading = false;
                        $rootScope.unload();
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('failure') || 'Failure!',
                            $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                        );
                    }
                });
            } else {
                DB.find('album', 'key', albumKey).then(function(res){
                    if (res.length > 0) {
                        var dateDifference = Math.floor((new Date() - new Date(res[0].last_modified)) / (1000*60));
                        if (dateDifference >= 30) {
                            $scope.loadImagesPromise(id);
                        } else {
                            $scope.albumImages = JSON.parse(res[0].value);
                            $scope.albumTitle = $scope.albumImages[0]['albumtitle'];
                            var photos = new Array();
                            for (var i = 0; i < $scope.albumImages.length; i++) {
                                var photo = {};
                                photo['src'] = $scope.albumImages[i]['big'];
                                photo['sub'] = $scope.albumImages[i]['phototitle'];
                                photos.push(photo);
                            };
                            shareImages.set(photos);

                            $rootScope.loading = false;
                            $rootScope.unload();
                        }
                    } else {
                        $scope.loadImagesPromise(id);
                    }
                });
            }

        };

        $scope.loadImagesPromise = function (id) {
            var albumKey = id + '_' + $rootScope.lang;
            photogalleryService.getAlbumPhotos({"lang":$scope.language, "albumid":id}).$promise.then(function (data) {
                $scope.albumImages = data['PhotoGallery'];
                $scope.albumTitle = $scope.albumImages[0]['albumtitle'];
                var photos = new Array();
                for (var i = 0; i < $scope.albumImages.length; i++) {
                    var photo = {};
                    photo['src'] = $scope.albumImages[i]['big'];
                    photo['sub'] = $scope.albumImages[i]['phototitle'];
                    photos.push(photo);
                };
                shareImages.set(photos);

                DB.find('album', 'key', albumKey).then(function(res){
                    if (res.length == 0) {
                        DB.insert('album',[albumKey,JSON.stringify($scope.albumImages),new Date()]);
                    } else {
                        DB.updateRow('album','value',JSON.stringify($scope.albumImages),'key',albumKey);
                        DB.updateRow('album','last_modified',new Date(),'key',albumKey);
                    }
                });

                $rootScope.loading = false;
                $rootScope.unload();
            }, function(error) {
                $rootScope.loading = false;
                $rootScope.unload();
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
            });
        };

        $scope.showImages = function(index) {
            $scope.activeSlide = index;
            $scope.showModal('templates/gallery-zoomview.html');
        };

        $scope.showModal = function(templateUrl) {
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: $scope
            }).then(function(modal) {
                $rootScope.modalActive = true;
                $scope.modal = modal;
                $scope.modal.show();
            });
        };



    });
})();
