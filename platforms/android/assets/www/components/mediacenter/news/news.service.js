'use strict';
(function() {
    angular
        .module('ezdanApp')
        .factory('newsService', newsService);

    function newsService($resource) {
    //var baseUrl = window.backendUrl;
        var service = $resource(window.backendUrl, {}, {
            'getNews': {
                method: 'GET', isArray: false,
                url: window.backendUrl + 'news/getnews'
            }
        });

        return service;
    }
})();
