'use strict';
(function() {
    angular.module('ezdanApp').controller('landController', function ($scope, $rootScope, $state, $http, $stateParams, $ionicSideMenuDelegate, $ionicHistory, user, localStorageService, DB, authentication) {
        $ionicSideMenuDelegate.canDragContent(false);
        $rootScope.passwordReset = false;
        $scope.resetData = {};
        $scope.resetPasswordMailSubmitted = false;
        $scope.resetPasswordMailSent = false;
        $scope.resetPasswordText = "Reset";
        $scope.loginData = {};
        $scope.loginSubmitted = false;
        $scope.loginSent = false;
        $scope.loginText = "Login";
        $scope.errors = {};


        // if($state.is('chooseLanguage')){
        // if($state.is('land')) {
        //     $scope.$on('$ionicView.loaded', function() {
        //         ionic.Platform.ready( function() {
        //             if(navigator && navigator.splashscreen) navigator.splashscreen.hide();
        //         });
        //     });
        // }

        $scope.guestLogin = function() {
            $ionicHistory.nextViewOptions({
                historyRoot: true
            });
            $state.go("home");
        };

        $scope.goRegister = function() {
            $scope.loginData = $scope.errors = {};
            $scope.loginSubmitted = false;
            $state.go("register");
        };

        $scope.goLogin = function() {
            $scope.loginData = $scope.errors = {};
            $scope.loginSubmitted = false;
            $state.go("login");
        };
        $scope.goChooseLanguage = function() {
            $scope.loginData = $scope.errors = {};
            $scope.loginSubmitted = false;
            $state.go("chooseLanguage");
        };

        $scope.chooseLanguage = function(language) {
            $rootScope.lang = language;
            $rootScope.go('land');
        };

        $scope.flipLanguage = function() {
            return $rootScope.lang === 'English' ? 'عربي' : 'English';
        }

        $scope.arabicOffPopUp = function() {
            $rootScope.popup(
                'success',
                'Language is not supported',
                'Sorry, This language is currently not Supported.'
            );
        };

        $scope.bgLoaded = false;
        $scope.bg = new Image();
        $scope.bg.onload = function() {
            $scope.bgLoaded = true;
        }
        // $scope.bg.src = 'img/landLanguageLogo.png'
    })
})();
