'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('verifySetPasswordController', function ($scope, $rootScope, $ionicSideMenuDelegate, $state, $http, $stateParams, $ionicHistory, user, localStorageService) {

        $ionicSideMenuDelegate.canDragContent(false);
        $scope.verifyResetPasswordData={};
        $scope.goLand = function() {
            $scope.setPasswordData = {};
            $scope.newPasswordSubmitted = $scope.newPasswordSent = $scope.setPasswordProblem = false;
            $state.go("land");
        }
        $scope.errors = {};
        $scope.verificationCodeSubmitted = false;


        if($stateParams.email)
        $scope.resetPasswordMail = $stateParams.email.replace(/"/g, "");
        else
        $scope.goLand();

        $scope.verifySetPassword = function(form) {
            if($rootScope.isOnline) {
                $scope.verificationCodeSubmitted = true;
                if(form.$valid) {
                    $scope.verificationCodeSent = true;
                    $rootScope.load();
                    user.api().verifyResetPassword({
                        Email: $scope.resetPasswordMail,
                        Code: $scope.verifyResetPasswordData.verificationCode})
                        .$promise.then(function(data) {
                            $scope.verificationCodeSent = false;
                            $rootScope.unload();
                            if(data) {
                                $state.go("setPassword", { email: $scope.resetPasswordMail });
                            }
                        },
                        function(error) {
                            $scope.verificationCodeSent = false;
                            $rootScope.unload();
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('failure') || 'Failure!',
                                $rootScope.langCounterpart('wrong_verification') || 'Wrong Verification Code !.'
                            );
                        })
                    }
                } else {
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('failure') || 'Failure!',
                        $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                    );
                }
            };

        });
    })();
