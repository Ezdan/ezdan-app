'use strict';
(function() {
    angular
    .module('ezdanApp')
    .factory('user', user);

    user.$inject = ['$resource', 'DB','$rootScope', 'localStorageService'];
    function user($resource, DB, $rootScope, localStorageService) {
        return {
            api: function() {
                var service = $resource(window.backendUrl + 'Account', {}, {
                    'resetPassword': {
                        method: 'POST', isArray: false,
                        headers: [{'Content-Type': 'application/x-www-form-urlencoded'}],
                        url: window.backendUrl + 'Account/FindAccount'
                    },
                    'generateResetPasswordCode': {
                        method: 'POST', isArray: false,
                        headers: [{'Content-Type': 'application/x-www-form-urlencoded'}],
                        url: window.backendUrl + 'Account/GenerateResetPasswordCode'
                    },
                    'verifyResetPassword': {
                        method: 'POST', isArray: false,
                        headers: [{'Content-Type': 'application/x-www-form-urlencoded'}],
                        url: window.backendUrl + 'Account/VerifyResetPasswordCode'
                    },
                    'setPassword': {
                        method: 'POST', isArray: false,
                        headers: [{'Content-Type': 'application/json'}],
                        url: window.backendUrl + 'Account/SetPassword'
                    },
                    'authorizePhone': {
                        method: 'POST', isArray: false,
                        headers: [{'Content-Type': 'application/json'}],
                        url: window.backendUrl + 'Account/authorizephone'
                    },
                    'resendPhoneAuthorization': {
                        method: 'POST', isArray: false,
                        headers: [{'Content-Type': 'application/json'}],
                        url: window.backendUrl + 'Account/resendphoneauth'
                    },
                    'cancelPhoneAuthorization': {
                        method: 'POST', isArray: false,
                        headers: [{'Content-Type': 'application/json'}],
                        url: window.backendUrl + 'Account/cancelphoneauth'
                    },
                    'submitPhoneAuthorization': {
                        method: 'POST', isArray: false,
                        headers: [{'Content-Type': 'application/json'}],
                        url: window.backendUrl + 'Account/submitphoneauth'
                    },
                    'login': {
                        method: 'POST', isArray: false,
                        headers: [{'Content-Type': 'application/json'}],
                        url: window.backendUrl.replace("api/", "token")
                    },
                    'register': {
                        method: 'POST', isArray: false,
                        headers: [{'Content-Type': 'application/json'}],
                        url: window.backendUrl + 'Account/Register'
                    },
                    'getSettings': {
                        method: 'GET', isArray: false,
                        url: window.backendUrl + 'Account/GetUserSetting'
                    },
                    'validateContract': {
                        method: 'GET', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + $rootScope.user.token },
                        url: window.backendUrl + 'Account/validatecontract'
                    },
                    'userInfo': {
                        method: 'GET', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + $rootScope.user.token },
                        url: window.backendUrl + 'Account/userinfo'
                    },
                    'updateUser': {
                        method: 'POST', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + $rootScope.user.token },
                        url: window.backendUrl + 'Account/editprofile'
                    },
                    'updateLanugage': {
                        method: 'POST', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + $rootScope.user.token },
                        url: window.backendUrl + 'Account/UpdateSetting'
                    },
                    'updateNotifications': {
                        method: 'POST', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + $rootScope.user.token },
                        url: window.backendUrl + 'Account/UpdateSetting'
                    },
                    'changePassword': {
                        method: 'POST', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + $rootScope.user.token },
                        url: window.backendUrl + 'Account/ChangePassword'
                    }
                });
                return service;
            },
            store: function(key, param) {
                localStorageService.set(key, param);
            },
            retrieve: function(key) {
                return localStorageService.get(key);
            },
            leave: function() {
                localStorageService.clearAll();

            },
            getUser: function() {
                return DB.get('user');
            },
            setUserType: function(type) {
                $rootScope.userType = type;
            },
            getUserType: function() {
                return $rootScope.userType;
            }
        }
    }})();
