'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('forgotPasswordController', function ($scope, $rootScope, $ionicSideMenuDelegate, $state, $http, $stateParams, $ionicHistory, user, localStorageService) {

        $ionicSideMenuDelegate.canDragContent(false);
        $scope.resetData = {};

        $scope.goLand = function() {
            $scope.setPasswordData = {};
            $scope.newPasswordSubmitted = $scope.newPasswordSent = $scope.setPasswordProblem = false;
            $rootScope.passwordReset = false;
            $state.go("land");
        }

        $scope.verifySetPassword = function(form) {

            if($rootScope.isOnline) {
                $scope.newPasswordSubmitted = true;
                if(form.$valid) {
                    $scope.verificationCodeSent = true;
                    $rootScope.load();
                    user.api().generateResetPasswordCode({
                        Email: $scope.resetData.resetPasswordMail})
                        .$promise.then(function(data) {
                            $rootScope.unload();
                            if(data) {
                                $rootScope.passwordReset = false;
                                $state.go("verifySetPassword", { email: $scope.resetData.resetPasswordMail });
                            }
                            $scope.newPasswordSubmitted = $scope.verificationCodeSent = false;
                        },
                        function(error) {
                            $rootScope.unload();
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('invalid_email') || 'Invalid Email',
                                $rootScope.langCounterpart('email_doesnot_exist') || 'Email does not exist.'
                            );
                            $scope.newPasswordSubmitted = $scope.verificationCodeSent = false;
                        })
                    }
                } else {
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('failure') || 'Failure!',
                        $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                    );
                }


            };

            $scope.goLand = function () {
                $rootScope.goBack();
            };

            $scope.guestLogin = function() {
                $ionicHistory.nextViewOptions({
                    historyRoot: true
                });
                $state.go("home");
            };

        });
    })();
