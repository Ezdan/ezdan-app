'use strict';
(function() {
    angular.module('ezdanApp')
    .controller('ourServicesController', function ($scope, $rootScope, $location) {
        $scope.goEzdanPlus = function() {
            $location.path('/content/26');
        }
    });
})();
