'use strict';
angular
    .module('ezdanApp')
    .factory('classifiedService', classifiedService);

    function classifiedService($resource) {
        var classfiedModel = {};
        return {
            authorize: function(token) {
                var service = $resource(window.backendUrl, {}, {
		            'getUserAdverts': {
		                method: 'GET', isArray: true,
		                headers: { 'Authorization': 'Bearer ' + token },
		                url: window.backendUrl + 'advertisement/getuseradvertisements'
		            },
                    'getAdvertsCategories': {
		                method: 'GET', isArray: true,
		                headers: { 'Authorization': 'Bearer ' + token },
		                url: window.backendUrl + 'advertisement/getcategories'
		            },
                    'addNewClassified': {
		                method: 'POST', isArray: false,
		                headers: { 'Authorization': 'Bearer ' + token },
		                url: window.backendUrl + 'advertisement/createadvertisement'
		            },
                    'reportClassified': {
                        method: 'GET', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + token },
                        url: window.backendUrl + 'advertisement/reportadvertisement'
                    },
                    'editClassified': {
                        method: 'POST', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + token },
                        url: window.backendUrl + 'advertisement/editclassified'
                    }
       			 });
                return service;
            },
            setModel: function(model) {
                classfiedModel = model;
            },
            getModel: function() {
                return classfiedModel;
            }
        }

    }
