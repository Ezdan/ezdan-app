'use strict';
(function() {
  angular
  .module('ezdanApp')
  .controller('surveysController', function ($rootScope, $scope, $stateParams, $state, $location, SurveyService,
    localStorageService, shareSurveyLocationService, authentication) {
      $scope.toggleLanguage = function() {
        console.log($rootScope.lang);
        console.log($rootScope.dict.settings);
      }
      $scope.showLocations = true;
      $scope.locationsLoading = false;
      $scope.getSurveyLocations = function (token) {
        $scope.locationsLoading = true;
        $rootScope.load();
        SurveyService.authorize(token).GetSurveyLocations().$promise.then(function (data) {
          $scope.locationsLoading = false;
          $rootScope.unload();
          if (data) {
            $scope.SurveyLocations = data;
            localStorageService.set('sessionKey', data.session_key);
          }
        }, function(error) {
          $rootScope.unload();
          $rootScope.goBack();
          $rootScope.popup(
            'error',
            $rootScope.langCounterpart('failure') || 'Failure!',
            $rootScope.langCounterpart('internal_server_error') || 'We are facing some technical difficulties, please try again in a while.'
          );
        });
      }
      $scope.getLangCounterPart = function(str) {
        return $rootScope.langCounterpart(str.toLowerCase());
      }
      $scope.$watch(function(){
        if(typeof($rootScope.user) !== 'undefined') {
          return $rootScope.user.token;
        }
      },function(token){
        if(token != "" && typeof(token) !== 'undefined' )
        $scope.getSurveyLocations(token);
      })
      $scope.goSurveyLocation = function(location) {
        $location.path('locationsurveies/' + location.id + '/' + location.name);
      }
    })
    .factory('shareSurveyLocationService', function() {
      var location = '';
      return {
        getLocation: function() {
          return location;
        },
        setLocation: function(theLocation) {
          location = theLocation;
        }
      }
    });
  })();
