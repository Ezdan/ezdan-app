'use strict';
(function() {
    angular
    .module('ezdanApp')
    .directive('makeIt', function() {
        return {
            link: function(scope, element, attrs) {
                var theImage = element[0];
                theImage.onload = function() {
                    var b64 = window.btoa(theImage);
                    var dataURL = "data:image/png;base64," + b64;
                    scope.$apply(function() {
                        scope.$parent.imageData.dataURL = dataURL;
                    })
                }
            }
        }
    })
    .controller('contentController', function($scope, $location, $state, $ionicSideMenuDelegate, $ionicPopup, $timeout, $rootScope, $filter, $ionicTabsDelegate, contentService, classifiedService, shareImages, $stateParams,$ionicModal,$ionicScrollDelegate,$ionicSlideBoxDelegate,$ionicActionSheet, DB) {
        $scope.title = '';
        $scope.imageData = {
            dataURL: ""
        };
        $scope.loaded = false;
        $scope.$watch('imageData.dataURL', function(newVal) {
            if(newVal !== "") {
                $scope.loaded = true;
            }
        });
        $scope.noResults = false;
        $scope.filterationData = {
            date: null
        };
        $scope.dateHolder = $rootScope.langCounterpart('filter_by_date') || 'Filter By Date';
        $scope.filterEvents = function(obj) {
            if(!$scope.filterationData.date) {
                $scope.dateHolder = $rootScope.langCounterpart('filter_by_date') || 'Filter By Date';
                return true;
            }
            var currRawDateTime = obj.content.properties.listItems[0].value,
                currRawDate = new Date(Date.parse(currRawDateTime.split(' ')[0])),
                currDay = currRawDate.getDate(), 
                currMonth = currRawDate.getMonth(),
                currYear = currRawDate.getFullYear(),
                selectedDate = $scope.filterationData.date,
                selectedDateHolder = selectedDate.getDate() + '-' + (selectedDate.getMonth() + 1) + '-' + selectedDate.getFullYear();
            $scope.dateHolder = selectedDateHolder;
            var aMatch = currRawDate.getTime() == selectedDate.getTime();
            return aMatch;
        }
        $scope.clearDate = function() {
            $scope.filterationData.date = null; 
        }
        // $scope.online = true;
        // $scope.$watch(function() {
        //     return $rootScope.isOnline;
        // }, function(isOnline) {
        //     $scope.online = isOnline;
        // });

        $scope.$watch(function() {
            return $rootScope.lang;
        }, function(lang) {
            $scope.lang = lang;
        });

        $scope.editContentPropsModel = {
            "ContentPropertyId":null,
            "ContentId": null,
            "PropertyId": null,
            "EnglishValue": null,
            "ArabicValue": null
        };

        $scope.editClassifiedModel = {
            "content": {
                "ContentId": null,
                "ParentId": null,
                "ContentType": null,
                "EnglishName": null,
                "ArabicName": null,
                "Blocked": false,
                "Reported": false,
                "CreatedOn": null
            },
            "contentProps": [],
            "deletedImages" : []
        };

        $scope.contentloading = function () {
            $rootScope.load();
            // var contentKey = $stateParams.id + '_' + $rootScope.lang;
            // if(!$rootScope.isOnline) {
            //     DB.find('content', 'key', contentKey).then(function(res){
            //         if (res.length > 0) {
            //             $scope.data = JSON.parse(res[0].value);
            //             if($rootScope.lang == 'Arabic' && $scope.data.content.type == 'tabs') {
            //                 $rootScope.tabs = $scope.data.children.reverse();
            //             }else {
            //                 $rootScope.tabs = $scope.data.children;
            //             }
            //             if($scope.data.hasOwnProperty("children")) {
            //                 $scope.selectedItem = $scope.data.children[0];
            //             }
            //         } else {
            //             $rootScope.goBack();
            //             $rootScope.deleteBackHistory();
            //             $rootScope.popup(
            //                 'error',
            //                 $rootScope.langCounterpart('failure') || 'Failure!',
            //                 $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
            //             );
            //         }
            //         $rootScope.unload();
            //     });
            // } else {
            //     DB.find('content', 'key', contentKey).then(function(res){
            //         if (res.length > 0) {
            //             // alert('res');
            //             var dateDifference = Math.floor((new Date() - new Date(res[0].last_modified)) / (1000*60));
            //             //console.log(dateDifference);
            //             if (dateDifference >= 30) {
            //                 //console.log('updating');
            //                 $scope.contentPromise();
            //             } else {
            //                 $scope.data = JSON.parse(res[0].value);
            //                 if($rootScope.lang == 'Arabic' && $scope.data.content.type == 'tabs') {
            //                     $rootScope.tabs = $scope.data.children.reverse();
            //                 }else {
            //                     $rootScope.tabs = $scope.data.children;
            //                 }
            //                 if($scope.data.hasOwnProperty("children")) {
            //                     $scope.selectedItem = $scope.data.children[0];
            //                 }
            //                 if($scope.data.hasOwnProperty("parent")) {
            //                     $scope.pageTitle = $scope.data.parent.title;
            //                     console.log($scope.pageTitle);
            //                 } else {
            //                     $scope.pageTitle = $scope.data.content.title;
            //                     console.log($scope.pageTitle);
            //                 }

            //                 $rootScope.unload();
            //             }
            //         } else {
                        // alert('mesh res');
                        $scope.contentPromise();

            //         }

            //     });
            // }
        };

        $scope.contentPromise = function () {
            var contentKey = $stateParams.id + '_' + $rootScope.lang;
            contentService.getContent(
                {
                    "id": $stateParams.id,
                    "lang": $rootScope.lang
                })
                .$promise.then(function (data) {
                    if (data) {
                        $scope.data = data;
                        //console.log($scope.data);
                        if(data.children && data.children.length > 0) {
                            if(($rootScope.lang == 'Arabic' && $scope.data.content.type == 'tabs') || $scope.data.content.type == 'announcementslist') {
                                // $rootScope.tabs = $scope.data.children.reverse();
                                $rootScope.data = $scope.data.children.reverse();
                                //console.log('reverse: ');
                                //console.log($rootScope.tabs);
                            } else {
                                // $rootScope.tabs = $scope.data.children;
                                $rootScope.data = $scope.data.children;
                            }

                            for(var i = 0; i < $scope.data.children; i++) {
                                var child = $scope.data.children[i];
                                // if(child.content.properties.hasOwnProperty('eventStartingDate')) {
                                //     alert('hamada');
                                // }
                            }
    
                            if($scope.data.hasOwnProperty("children")) {
                                $scope.selectedItem = $scope.data.children[0];
                            }
                            if($scope.data.hasOwnProperty("parent")) {
                                $scope.pageTitle = $scope.data.parent.title;
                            } else {
                                $scope.pageTitle = $scope.data.content.title;
                            }
    
                            if ($scope.data.content.type != 'announcementslist'
                                && $scope.data.content.type != 'classifiedlist'
                                && $scope.data.content.type != 'textlist'
                                && $scope.data.content.type != 'classifieddetail'
                                && $scope.data.content.type != 'detailview') {
                                DB.find('content', 'key', contentKey).then(function(res){
                                    if (res.length == 0) {
                                        DB.insert('content',[contentKey,JSON.stringify(data),new Date()]);
                                    } else {
                                        DB.updateRow('content','value',JSON.stringify(data),'key',contentKey);
                                        DB.updateRow('content','last_modified',new Date(),'key',contentKey);
                                    }
                                });
                            }
                        }
                        else {
                            $scope.noResults = true;
                        }
                        $scope.tabTitle = $scope.data.content.title;
                        //console.log($scope.tabTitle);
                        //console.log(data);
                    }
                    //console.log($rootScope.tabs);
                    $rootScope.unload();
                }, function(error) {
                    //console.log(error);
                    $rootScope.unload();
                    $rootScope.goBack();
                    $rootScope.deleteBackHistory();
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('failure') || 'Failure!',
                        $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                    );
                });
            };

            $scope.navigateToContent = function(selectedContent){
                if(selectedContent.type=="map"){
                    // $state.go("map", {long:selectedContent.properties.longitude.value , latt:selectedContent.properties.latitude.value}, {reload: true});
                    $location.path("map/" + selectedContent.properties.longitude.value + "/" + selectedContent.properties.latitude.value);
                }
                else {
                    $state.go("content", {id:selectedContent.id}, {reload: true})
                }
            };

            $scope.editClassified = function () {
                classifiedService.setModel($scope.data);
                $state.go("editClassifiedForm");
            };

            $scope.shareImages = function (photos) {
                shareImages.set(photos);
            };

            $scope.showImages = function(index) {
                $scope.activeSlide = index;
                $scope.showModal('templates/gallery-zoomview.html');
            };

            $scope.showModal = function(templateUrl) {
                $ionicModal.fromTemplateUrl(templateUrl, {
                    scope: $scope
                }).then(function(modal) {
                    $rootScope.modalActive = true;
                    $scope.modal = modal;
                    $scope.modal.show();
                });
            };

            $scope.setInitTab = function(){
                if($rootScope.lang == 'Arabic') {
                    var length = $rootScope.tabs.length;
                    if ($('.globalTab')[length - 1] != undefined) {
                        ($('.globalTab')[length - 1]).className+= " active";
                        $ionicScrollDelegate.$getByHandle('tabHandle').scrollBottom();
                    }
                } else {
                    ($('.globalTab')[0]).className+= " active";
                }
            };





            $scope.goToFirstSlide = function(){
                $scope.swiper.slideTo(0);
              };

              $scope.swiperOptions = {
                /* Whatever options */
                effect: 'slide',
                initialSlide: 0,
                autoplay: 3500,
                loop: true,
                autoplayDisableOnInteraction: false,
                /* Initialize a scope variable with the swiper */
                onInit: function(swiper){
                $scope.swiper = swiper;
                // Now you can do whatever you want with the swiper

                },
                onSlideChangeEnd: function(swiper){
                }
              };

            $scope.selectedIndex = function(index) {
                $scope.selectedItem = $scope.data.children[index];
                $('.globalTab').removeClass('active');
                ($('.globalTab')[index]).className+= " active";
            };

            $scope.changeSlide = function (index,event) {
                $scope.selectedItem = $scope.data.children[index];
                $('.globalTab').removeClass('active');
                $(event.target.parentElement).addClass('active');
                if(index != 0) {
                    $ionicSideMenuDelegate.canDragContent(false);
                    $location.hash($scope.data.children[index - 1].content.title);
                    $ionicScrollDelegate.$getByHandle('tabHandle').anchorScroll();
                } else {
                    // $ionicSideMenuDelegate.canDragContent(true);
                    $ionicScrollDelegate.$getByHandle('tabHandle').scrollTop();
                }
                $ionicSlideBoxDelegate.slide(index);
            }

            $scope.initSlider = function () {
                // $ionicSlideBoxDelegate.enableSlide(false);
                $ionicSideMenuDelegate.canDragContent(false);
                if ($rootScope.lang == 'Arabic') {
                    $ionicSlideBoxDelegate.slide($rootScope.tabs.length - 1);
                }

            };

            $scope.onDragRight = function () {
                if($ionicSlideBoxDelegate.currentIndex() > 1) {
                    $location.hash($scope.data.children[$ionicSlideBoxDelegate.currentIndex() - 1].content.title);
                    $ionicScrollDelegate.$getByHandle('tabHandle').anchorScroll();
                } else {
                    $ionicScrollDelegate.$getByHandle('tabHandle').scrollTop();
                }
            };

            $scope.onDragLeft = function () {
                if ($ionicSlideBoxDelegate.currentIndex() < $ionicSlideBoxDelegate.slidesCount() - 1 && $ionicSlideBoxDelegate.currentIndex() != 0) {
                    $location.hash($scope.data.children[$ionicSlideBoxDelegate.currentIndex() - 1].content.title);
                    $ionicScrollDelegate.$getByHandle('tabHandle').anchorScroll();

                } else if($ionicSlideBoxDelegate.currentIndex() >= $ionicSlideBoxDelegate.slidesCount() - 2) {
                    $ionicScrollDelegate.$getByHandle('tabHandle').scrollBottom();
                }
            };

            $scope.letDrag = function() {
                $ionicSlideBoxDelegate.enableSlide(true);
            };

            $scope.stopDrag = function() {
                //console.log('stopDrag');
                $ionicSlideBoxDelegate.enableSlide(false);
            };

            $scope.setSlide = function(index) {
                $scope.selectedItem = $scope.data.children[index];
                $ionicSlideBoxDelegate.slide(index);
            };

            $scope.newDate = [];
            $scope.toggleDateLang = function (dateInput) {
                var d = new Date(dateInput);

                var dateElement;
                if ($rootScope.lang === 'Arabic') {
                    dateElement = d.toLocaleDateString('ar-EG');
                } else {
                    dateElement = d.toLocaleDateString('en-GB');
                }
                $scope.newDate.push(dateElement);
                //console.log($scope.newDate);
            };

            $scope.call = function (number) {
                location.href= 'tel: +974' + number;
            };

            $scope.email = function (mail) {
                location.href= 'mailto: ' + mail;
            };

            $scope.reportClassified = function (id) {
                $rootScope.load();
                classifiedService.authorize($rootScope.user.token).reportClassified(
                    {
                        "id": id
                    })
                    .$promise.then(function (data) {
                        if (data.reported) {
                            $rootScope.popup(
                                'confirmation',
                                $rootScope.langCounterpart('confirmation') || 'Confirmation',
                                $rootScope.langCounterpart('report_successful') || 'Thank you for reporting this classified!'
                            );
                        } else {
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('multiple_report') || 'Multiple Report',
                                $rootScope.langCounterpart('report_failure') || 'You have reported ths classified before.'
                            );
                        }
                        $rootScope.unload();
                    });
                };

                $scope.reportClassifiedPopUp = function (id) {
                    $ionicPopup.show({
                        template: '<span>Are you sure you want to report this classified?</span>',
                        title: 'Confirmation!',
                        scope: $scope,
                        buttons: [
                            { text: 'Cancel' },
                            {
                                text: '<b>Confirm</b>',
                                type: 'button-positive',
                                onTap: function(e) {
                                    $scope.reportClassified(id);
                                }
                            }
                        ]
                    });
                };

                $scope.shareFacebook = function() {
                    window.plugins.socialsharing.shareViaFacebook(
                        $scope.data.content.properties.text,
                        $scope.data.content.properties.image /* img */,
                        null /* url */,
                        function() { /*console.log('shared on facebook') */ },
                        function(error) {
                            $scope.popupMissingApp();
                        }
                    );
                };
                $scope.shareTwitter = function() {
                    window.plugins.socialsharing.shareViaTwitter(
                        $scope.data.content.properties.text,
                        $scope.data.content.properties.image /* img */,
                        null /* url */,
                        function() { /*console.log('shared on twitter')*/ },
                        function(error) {
                            $scope.popupMissingApp();
                        }
                    );
                };
                $scope.shareWhatsApp = function() {
                    window.plugins.socialsharing.shareViaWhatsApp(
                        $scope.data.content.properties.text,
                        $scope.data.content.properties.image /* img */,
                        null /* url */,
                        function() { /*console.log('shared on whatsapp')*/ },
                        function(error) {
                            $scope.popupMissingApp();
                        }
                    );
                };
                $scope.shareMail = function() {
                    window.plugins.socialsharing.shareViaEmail(
                        $scope.data.content.properties.text,
                        'Ezdan - ' + $scope.title,
                        null,
                        null,
                        null,
                        [$scope.data.content.properties.image],
                        function() { /*console.log('shared on email')*/ },
                        function(error) {
                            $scope.popupMissingApp();
                        }
                    );
                };
                $scope.showActionsheet = function() {
                    //console.log($scope.online);
                    if($scope.online) {
                        $ionicActionSheet.show({
                            titleText: 'Share Via',
                            buttons: [
                                {
                                    text: '<i class="icon ion-social-facebook"></i> Facebook'
                                },
                                {
                                    text: '<i class="icon ion-social-twitter"></i> Twitter'
                                },
                                {
                                    text: '<i class="icon ion-social-whatsapp"></i> WhatsApp'
                                },
                                {
                                    text: '<i class="icon ion-ios-email"></i> Mail'
                                }
                            ],
                            // destructiveText: 'Delete',
                            cancelText: 'Cancel',
                            cancel: function() {},
                            buttonClicked: function(index) {
                                switch(index) {
                                    case 0:
                                    $scope.shareFacebook();
                                    break;
                                    case 1:
                                    $scope.shareTwitter();
                                    break;
                                    case 2:
                                    $scope.shareWhatsApp();
                                    break;
                                    default:
                                    $scope.shareMail();
                                }
                                return true;
                            },
                            destructiveButtonClicked: function() { return true; }
                        });
                    }
                    else {
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('failure') || 'Failure!',
                            $rootScope.langCounterpart('please_check_your_internet_connection') || 'Please check your internet connection.'
                        );
                    }
                };
                $scope.popupMissingApp = function() {
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('failure') || 'Failure!',
                        $rootScope.langCounterpart('please_check_that_this_app_is_installed') || 'Please check that this app is installed.'
                    );
                };

                $scope.requestPlus = function(serviceObj) {
                    $state.go('plusRequest', { serviceObj: serviceObj });
                }
                // $scope.setInitTab();

                $scope.$watch('pageTitle', function(newVal, oldVal) {
                    if(typeof(newVal) !== 'undefined') {
                        $timeout(function() {
                            switch(newVal) {
                                case 'Announcements':
                                $scope.title = $rootScope.langCounterpart('promotions') || 'Promotions';
                                break;
                                case 'Discover':
                                $scope.title = $rootScope.langCounterpart('upcoming_events') || 'Upcoming Events';
                                break;
                                default:
                                $scope.title = $rootScope.langCounterpart(newVal) || newVal;
                            }
                        })
                    }
                })
            });
        })();
