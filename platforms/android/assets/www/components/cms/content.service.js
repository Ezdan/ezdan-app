'use strict';
(function() {
    angular
        .module('ezdanApp')
        .factory('contentService', contentService);

        function contentService($resource, $rootScope) {
            var service = $resource(window.backendUrl, {}, {
                'getContent': {
                    method: 'GET', isArray: false,
                    url: window.backendUrl + 'cms/getcontent'
                }
            });
            return service;
        }
})();
