'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('tabs', function() {
        return{
            templateUrl: 'components/cms/templates/tabs.html'
        };
    });
})();
