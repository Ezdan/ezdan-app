'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('textThumbDetail', function() {

        return{
            templateUrl: 'components/cms/templates/textThumbDetail.html'
        };

    });
})();
