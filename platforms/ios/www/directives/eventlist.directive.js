'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('eventList', function() {

        return{
            templateUrl: 'components/cms/templates/eventlist.html'
        };

    });
})();
