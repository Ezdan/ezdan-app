'use strict';
(function () {
    angular
        .module('ezdanApp')
        .controller("propertiesController", function ($scope, $rootScope, $state, $timeout, $location, $ionicHistory, $ionicScrollDelegate, propertyService, DB) {
            var resetFilter = $state.params.resetFilter;
            if (resetFilter == 'true') {
                window.localStorage.removeItem('filterObj');
            }
            var filterObj = window.localStorage.getItem('filterObj');
            if (filterObj != null && filterObj != undefined && filterObj.length > 0) {
                filterObj = JSON.parse(filterObj);
                $scope.filterObj = {
                    city: filterObj.city,
                    bedrooms: filterObj.bedrooms,
                    propertyType: filterObj.propertyType,
                    isFurnished: filterObj.isFurnished
                }
            } else {
                $scope.filterObj = {
                    city: '',
                    bedrooms: '',
                    propertyType: '',
                    isFurnished: ''
                }
            }
            $scope.properties = [];
            $scope.filters = {};
            $scope.propertiesTotal = 0;
            $scope.pageNumber = 1;
            $scope.hasMore = true;
            $scope.hasResults = false;
            $scope.disposeTotalNumeric = false;
            $scope.backFromDetails = false;
            $scope.propertiesLoading = false;
            $scope.filter = function () {
                $scope.pageNumber = 1;
                $scope.properties = [];
                $scope.findProperties(false, true);

                // $location.path("properties/" + $scope.filterObj.city 
                // + '/' + $scope.filterObj.bedrooms + '/' + $scope.filterObj.propertyType + '/' + 
                // $scope.filterObj.isFurnished);
                
                window.localStorage.setItem('filterObj', JSON.stringify($scope.filterObj));
            }
            $scope.get_more = function () {
                $scope.pageNumber++;
                $scope.findProperties(true, false);
                // $scope.$broadcast('scroll.infiniteScrollComplete');
            }
            $scope.findProperties = function (more, filtered) {
                if (filtered) {
                    $ionicScrollDelegate.scrollTop();
                }
                if ((!more && filtered) || (!more && !filtered)) {
                    $scope.propertiesLoading = true;
                }
                $rootScope.load();
                var propsKey = $scope.filterObj.city + '_' + $scope.filterObj.bedrooms + '_' + $scope.filterObj.propertyType + '_' + $scope.filterObj.isFurnished + '_' + $scope.pageNumber + '_' + $rootScope.lang;
                if (!$rootScope.isOnline) {
                    //OFFLINE
                    DB.find('propertiesList', 'key', propsKey).then(function (res) {
                        $rootScope.unload();
                        if (res.length > 0 && JSON.parse(res[0].value).length > 0) {
                            var offlineProperties = JSON.parse(res[0].value);
                            if (offlineProperties.length > 0) {
                                $scope.propertiesLoading = false;
                                $scope.properties = offlineProperties;
                                $scope.hasResults = $scope.properties.length === 0 ? false : true;
                                $scope.propertiesLoading = false;
                                if (res.length <= 30) {
                                    $scope.hasMore = false;
                                }
                            }
                        } else {
                            //OFFLINE
                            $scope.hasMore = false;
                            $scope.pageNumber--;
                            $rootScope.unload();
                            // $rootScope.goBack();
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('failure') || 'Failure!',
                                $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                            );
                        }
                    });
                } else {
                    // ONLINE
                    $scope.findPropsPromise(more, filtered);
                    // DB.find('propertiesList', 'key', propsKey).then(function (res) {
                        // if (res.length > 0 && JSON.parse(res[0].value).length > 0) {
                        //     var offlineProperties = JSON.parse(res[0].value);
                        //     var dateDifference = Math.floor((new Date() - new Date(res[0].last_modified)) / (1000 * 60));
                        //     if (dateDifference >= 30) {
                        //         $scope.findPropsPromise(more, filtered);
                        //     } else {
                        //         $scope.propertiesLoading = false;
                        //         $scope.properties = offlineProperties;
                        //         $scope.hasResults = $scope.properties.length === 0 ? false : true;
                        //         $rootScope.unload();
                        //     }
                        // }
                        // else {
                            // $scope.findPropsPromise(more, filtered);
                        // }
                    // });
                }
            };

            $scope.getFilters = function () {
                propertyService.getFilters().then(function (filters) {
                    if (Object.keys(filters).length > 0) {
                        $scope.filters = filters;
                    }
                });
            }

            $scope.findPropsPromise = function (more, filtered) {
                propertyService.properties.find({
                    pagenumber: $scope.pageNumber,
                    bedrooms: $scope.filterObj.bedrooms,
                    city: $scope.filterObj.city,
                    propertytype: $scope.filterObj.propertyType,
                    isFurnished: $scope.filterObj.isFurnished,
                    seo:"x18",
                })
                    .$promise.then(function (data) {
                        $rootScope.unload();
                        $scope.propertiesLoading = false;
                        $scope.hasMore = data.result.length === 0 || data.noMore ? false : true;
                        $scope.properties = $scope.properties.concat(data.result);
                        $scope.hasResults = $scope.properties.length === 0 ? false : true;
                        var propsKey = $scope.filterObj.city + '_' + $scope.filterObj.bedrooms + '_' + $scope.filterObj.propertyType + '_' + $scope.filterObj.isFurnished + '_' + $scope.pageNumber + '_' + $rootScope.lang;
                        DB.find('propertiesList', 'key', propsKey).then(function (res) {
                            if ($scope.properties.length > 0) {
                                if (res.length == 0) {
                                    if ($scope.properties.length <= 90) {
                                        DB.insert('propertiesList', [propsKey, JSON.stringify($scope.properties), new Date()]);
                                    }
                                } else {
                                    DB.updateRow('propertiesList', 'value', JSON.stringify($scope.properties), 'key', propsKey);
                                    DB.updateRow('propertiesList', 'last_modified', new Date(), 'key', propsKey);
                                }
                            }
                        });
                        if (!more && !filtered && $scope.properties.length === 0) {
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('failure') || 'Failure!',
                                $rootScope.langCounterpart('no_properties_found') || 'No properties found.'
                            );
                            // $rootScope.goBack();
                        }
                    },
                        function (error) {
                            $rootScope.unload();
                            $scope.pageNumber--;
                            // $rootScope.goBack();
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('failure') || 'Failure!',
                                $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                            );

                        });
            };

            $scope.findProperties(false, false);

            $scope.getFilters();

            $scope.propertyLangCounterpart = function () {
                if ($rootScope.lang === 'English') {
                    return 'Properties';
                }
                else {
                    var propertiesTotal = $scope.properties.length;
                    if (propertiesTotal === 1) {
                        $scope.disposeTotalNumeric = true;
                        return 'وحدة واحدة';
                    }
                    else if (propertiesTotal === 2) {
                        $scope.disposeTotalNumeric = true;
                        return 'وحدتين';
                    }
                    else if (propertiesTotal > 2 && propertiesTotal < 11) {
                        return 'وحدات';
                    }
                    else if (propertiesTotal < 1 || propertiesTotal >= 11) {
                        return 'وحدة';
                    }
                }
            };

            $scope.goProperty = function (property) {
                $location.path("property/" + property._DocumnetHash);
            }
        });
})();
