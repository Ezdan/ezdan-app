'use strict';
angular
    .module('ezdanApp')
    .factory('appointmentService', appointmentService);

    appointmentService.$inject = ['$resource'];

    function appointmentService($resource) {
      return {
          authorize: function(token) {
        var service = $resource(window.backendUrl+'appointments', {}, {
            'GetDiaryEvents': {
                method: 'POST', isArray: false,
                headers: { 'Authorization': 'Bearer ' + token },
                url:window.backendUrl+'Appointments/GetDiaryEvents'
            },
            'SaveGuestAppointment': {
                method: 'POST', isArray: false,
                url:window.backendUrl+'Appointments/SaveGuestAppointment'
            },
            'GetGuestCalendar': {
                method: 'POST', isArray: false,
                url:window.backendUrl+'Appointments/GetGuestCalendar'
            },
            'CreateAppointment':{
              method: 'POST', isArray: false,
              headers: { 'Authorization': 'Bearer ' + token },
              url:window.backendUrl+'Appointments/SaveAppointment'
            },
            'CancelAppointment':{
              method: 'POST', isArray: false,
              headers: { 'Authorization': 'Bearer ' + token },
              url:window.backendUrl+'Appointments/CancelAppointment'
            },
            'EditAppointment':{
              method: 'POST', isArray: false,
              headers: { 'Authorization': 'Bearer ' + token },
              url:window.backendUrl+'Appointments/EditAppointment'
            }
        });

        return service;
    }
}
}
