'use strict';
(function() {
    angular
    .module('ezdanApp')
    .factory('authentication', authentication);

    authentication.$inject = ['DB','$rootScope'];
    function authentication(DB, $rootScope) {
        var AuthObj = {};
        var _authentication = {
            isAuth : false,
            fullName : '',
            email  : '',
            token : '',
            language : 'English',
            notification :false,
            imageURL: '',
            mobile: '',
            title: ''
        }
        var setAuth = function(){
            DB.get('user').then(function (userData) {
                var user = userData[0];
                if(user != undefined){
                    _authentication.isAuth =  true;
                    _authentication.fullName = user.fullName ;
                    _authentication.email = user.email ;
                    _authentication.token = user.access_token ;
                    _authentication.language = user.language ;
                    _authentication.notification = user.notification;
                    _authentication.imageURL = user.imageURL;
                    _authentication.mobile = user.mobile;
                    _authentication.title = user.title;
                    _authentication.location = user.location;
                    _authentication.unit = user.unit;
                }
                else {
                    _authentication.isAuth =  false;
                    _authentication.fullName = '' ;
                    _authentication.email = '' ;
                    _authentication.token = '' ;
                    _authentication.language = 'English' ;
                    _authentication.notification = false;
                    _authentication.imageURL = null;
                    _authentication.mobile = null;
                    _authentication.title = null;
                    _authentication.location = null;
                    _authentication.unit = null;
                }
                $rootScope.user = {};
                $rootScope.user.fullName = _authentication.fullName;
                $rootScope.user.email = _authentication.email;
                $rootScope.user.token = _authentication.token;
                $rootScope.user.imageURL = _authentication.imageURL;
                $rootScope.user.mobile = _authentication.mobile;
                // $rootScope.user.lang = _authentication.language;
                $rootScope.user.notifications = _authentication.notification;
                $rootScope.user.title = _authentication.title;
                $rootScope.user.location = _authentication.location;
                $rootScope.user.unit = _authentication.unit;
                $rootScope.user.loggedIn = _authentication.isAuth;
                $rootScope.lang = _authentication.language;
            });
        }
        AuthObj.setAuth = setAuth;
        AuthObj.authData =_authentication;
        return AuthObj;
    }

    // 'use strict';
    // angular
    // .module('ezdanApp')
    // .factory('authentication', authentication);
    //
    // authentication.$inject = ['DB', '$rootScope'];
    // function authentication(DB, $rootScope) {
    //     var authObj = {
    //         isAuth : false,
    //         fullName : '',
    //         email  : '',
    //         token : '',
    //         language : 'English',
    //         notification :false
    //     };
    //     return {
    //         setAuth: function() {
    //             DB.get('user').then(function (userData) {
    //                 var user = userData[0];
    //                 if(user != undefined){
    //                     authObj.isAuth =  true;
    //                     authObj.fullName = user.fullName ;
    //                     authObj.email = user.email ;
    //                     authObj.token = user.access_token ;
    //                     authObj.language = user.language ;
    //                     authObj.notification = user.notifcation;
    //                 }
    //                 else {
    //                     authObj.isAuth =  false;
    //                     authObj.fullName = '' ;
    //                     authObj.email = '' ;
    //                     authObj.token = '' ;
    //                     authObj.language = 'English' ;
    //                     authObj.notification = false;
    //                 }
    //                 $rootScope.fullName = authObj.fullName;
    //                 $rootScope.email = authObj.email;
    //                 $rootScope.token = authObj.token;
    //                 $rootScope.lang = authObj.language;
    //                 $rootScope.notifications = authObj.notification;
    //             });
    //         },
    //         getToken: function() {
    //             return authObj.token;
    //         }
    //     }
    // }
})();
