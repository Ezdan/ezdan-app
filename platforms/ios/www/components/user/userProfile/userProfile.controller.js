'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('userProfileController', function ($scope, DB, authentication, $ionicPopup, $rootScope, $state, user, $cordovaFile, $cordovaImagePicker, $cordovaCamera, $cordovaFileTransfer, $cordovaActionSheet) {
        $scope.userModel = {
            "name": $rootScope.user.fullName,
            "email": $rootScope.user.email,
            "mobile": $rootScope.user.mobile
        };
        $scope.profileImage = $rootScope.userImage;

        $scope.updatedProfileImage = null;
        $scope.profileFormSubmitted = false;

        // $scope.getUserModel = function () {
        //
        //     $rootScope.load();
        //     user.api().userInfo().$promise.then(function(data) {
        //         $scope.userInfo = data;
        //         DB.update('user','email', data.Email);
        //         $rootScope.user.email = data.Email;
        //         $scope.userModel.email = data.Email;
        //
        //         DB.update('user','fullName', data.Name);
        //         $rootScope.user.fullName = data.Name;
        //         $scope.userModel.name = data.Name;
        //
        //         DB.update('user','mobile', data.Mobile);
        //         $rootScope.user.mobile = data.Mobile;
        //         $scope.userModel.mobile = data.Mobile;
        //
        //
        //         $rootScope.unload();
        //     }, function(error) {
        //         $rootScope.unload();
        //         $rootScope.popup('error', 'failure');
        //     });
        // };

        $scope.goAccountSettings = function () {
            $state.go("accountSettings");
        };

        $scope.updateProfile = function (form) {
            $scope.profileFormSubmitted = true;
            if(form.$valid) {
                $rootScope.load();
                user.api().updateUser($scope.userModel).$promise.then(function(data) {
                    DB.update('user', 'fullName', data.UpdatedInfo.fullName);
                    DB.update('user', 'email', data.UpdatedInfo.email);
                    DB.update('user', 'mobile', data.UpdatedInfo.mobile);
                    authentication.setAuth();
                    if($scope.updatedProfileImage != null) {
                        $scope.uploadImage();
                    } else {
                        $rootScope.popup(
                            'success',
                            $rootScope.langCounterpart('success') || 'Success',
                            $rootScope.langCounterpart('profile_updated') || 'Profile has been updated.'
                        );
                        // $scope.showProfileUpdateConfirmation();
                        $rootScope.unload();
                    }
                    $scope.profileFormSubmitted = false;
                }, function(error) {
                    if(error.status === 409) {
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('email_exists') || 'Email Exists!',
                            $rootScope.langCounterpart('email_exists_template') || 'Sorry! The email is already registered.'
                        );
                    }
                    else {
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('server_error') || 'Server Error!',
                            $rootScope.langCounterpart('internal_server_problem') || 'Internal Server Error.'
                        );
                    }
                    $rootScope.unload();
                    $scope.profileFormSubmitted = false;
                });
            }
        };

        $scope.uploadImage = function() {
            var url = window.backendUrl + "Account/saveimage";
            if($scope.updatedProfileImage != null) {
                var img = $scope.updatedProfileImage;
                var targetPath = $scope.updatedProfileImage;
                // File name only
                var filename = targetPath.substr(targetPath.lastIndexOf('/')+1).replace(/\s/g,'');
                var optionsUpload = {
                    fileKey: "file",
                    fileName: filename,
                    chunkedMode: false,
                    mimeType: "multipart/form-data",
                    params : {
                        'fileName': filename
                    },
                    headers: {
                        Connection: "close",
                        Authorization: 'Bearer ' + $rootScope.user.token
                    }
                };
                $cordovaFileTransfer.upload(url, targetPath, optionsUpload).then(function(result) {
                    $rootScope.unload();
                    var responseObj = JSON.parse(result.response);
                    if(responseObj.status == "True") {
                        DB.update('user','imageURL', responseObj.imageURL);
                        $rootScope.user.imageURL = $rootScope.userImage = responseObj.imageURL;
                        $rootScope.popup(
                            'success',
                            $rootScope.langCounterpart('success') || 'Success',
                            $rootScope.langCounterpart('profile_updated') || 'Profile has been updated.'
                        );
                    } else {
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('failure') || 'Failure!',
                            $rootScope.langCounterpart('profile_updated_without_image') || 'Your profile has been updated successfully, but your profile image was not updated. Please try again.'
                        );
                    }
                }, function (error) {
                    $rootScope.unload();
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('failure') || 'Failure!',
                        $rootScope.langCounterpart('error_uploading_image') || 'An error occured while trying to upload your new profile image. Please check your network and try again.'
                    );
                });
            }
        };

        $scope.selectImages = function() {
            var optionsSelect = {
                title: 'Please select images source',
                buttonLabels: ['Load from gallery', 'Use camera'],
                addCancelButtonWithLabel: 'Cancel',
                androidEnableCancelButton : true,
            };
            $cordovaActionSheet.show(optionsSelect).then(function(btnIndex) {
                if (btnIndex === 1) {
                    var permissions = cordova.plugins.permissions;
                    permissions.checkPermission(permissions.READ_EXTERNAL_STORAGE, function( status ){
                        if ( status.hasPermission  ) {
                            $scope.selectImagesFromGallery();
                        }
                        else {
                            permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE,
                                function success(status) {
                                if( !status.hasPermission ) {
                                    console.warn('Camera permission is not turned on');
                                } else {
                                    $scope.selectImagesFromGallery();
                                }
                            },
                            function error(status) {
                              console.warn('Camera permission is not turned on');
                            }
                            );
                        }
                    });
                } else if (btnIndex === 2) {
                    $scope.selectImageByCamera();
                }
            });

        };

        $scope.selectImagesFromGallery = function () {
            var optionsGallery = {
                maximumImagesCount: 1,
                quality: 10
            };
            $cordovaImagePicker.getPictures(optionsGallery)
            .then(function (results) {
                if(results.length != 0) {
                    $scope.updatedProfileImage = results[0];
                    $scope.profileImage = results[0];

                }
            }, function(error) {

            });
        };

        $scope.selectImageByCamera = function () {
            var optionsCamera = {
                quality: 10,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA,
                correctOrientation: true,
                saveToPhotoAlbum: false
            };

            $cordovaCamera.getPicture(optionsCamera).then(function(imagePath) {
                var currentName = imagePath.replace(/^.*[\\\/]/, '');
                var d = new Date(),
                n = d.getTime(),
                newFileName =  n + ".jpg";
                var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);

                $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
                    $scope.updatedProfileImage = cordova.file.dataDirectory + newFileName;
                    $scope.profileImage = cordova.file.dataDirectory + newFileName;
                }, function(error){
                    // $scope.showAlert('Error', error.exception);
                });

            },
            function(err){

            })
        };

        $scope.showProfileUpdateConfirmation = function () {
            $rootScope.unload();
            $ionicPopup.show({
                template: 'Your profile has been updated successfully!',
                title: 'Confirmation!',
                scope: $scope,
                buttons: [
                    {
                        text: '<b>Confirm</b>',
                        type: 'button-positive',
                        onTap: function(e) {}
                    }
                ]
            });
        };

        $scope.profileUpdateImageFail = function () {
            $rootScope.unload();
            $ionicPopup.show({
                template: 'Your profile has been updated successfully, but your profile image was not updated. Please try again.',
                title: 'Confirmation!',
                scope: $scope,
                buttons: [
                    {
                        text: '<b>Confirm</b>',
                        type: 'button-positive',
                        onTap: function(e) {}
                    }
                ]
            });
        };

        $scope.uploadError = function () {
            $rootScope.unload();
            $ionicPopup.show({
                template: 'An error occured while trying to upload your new profile image. Please check your network and try again.',
                title: 'Confirmation!',
                scope: $scope,
                buttons: [
                    {
                        text: '<b>Confirm</b>',
                        type: 'button-positive',
                        onTap: function(e) {}
                    }
                ]
            });
        };

    });
})();
