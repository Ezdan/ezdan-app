'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('loginController', function ($scope, $rootScope, $state, $http, $stateParams, $ionicNavBarDelegate, $ionicSideMenuDelegate, $ionicHistory, user, localStorageService, DB, authentication) {
        $ionicSideMenuDelegate.canDragContent(false);
        $scope.loginShowPassword = false;
        $rootScope.passwordReset = false;
        $scope.resetData = {};
        $scope.resetPasswordMailSubmitted = false;
        $scope.resetPasswordMailSent = false;
        $scope.resetPasswordText = "Reset";
        $scope.loginData = {};
        $scope.loginSubmitted = false;
        $scope.loginSent = false;
        $scope.loginText = "Login";
        $scope.errors = {};
        $scope.togglePasswordReset = function() {
            $scope.loginData = $scope.resetData = $scope.errors = {};
            $scope.resetPasswordMail = null;
            $scope.loginSubmitted = $scope.resetPasswordMailSubmitted = false;
            $rootScope.passwordReset = !$rootScope.passwordReset;
        }
        $scope.resetPassword = function(form) {
            $scope.resetPasswordMailSubmitted = true;
            if(form.$valid) {
                $scope.resetPasswordMailSent = true;
                $rootScope.load();
                $scope.resetPasswordMailSent = true;
                var resetPasswordMail = '"' + $scope.resetData.resetPasswordMail + '"';
                user.api().resetPassword(resetPasswordMail).$promise.then(function(data) {
                    if(data && data.found) {
                        $state.go("setPassword", { email: resetPasswordMail });
                    }
                    else {
                        $scope.errors.incorrectMail = true;
                    }
                    $rootScope.unload();
                    $scope.resetPasswordMailSubmitted = $scope.resetPasswordMailSent = false;
                }, function(error) {
                    $rootScope.unload();
                    $rootScope.popup('error', 'failure');
                    $scope.resetPasswordMailSubmitted = $scope.resetPasswordMailSent = false;
                    $scope.errors.resetPasswordProblem = true;
                })
            }
        }
        $scope.login = function (form) {
            $scope.loginSubmitted = true;
            if(form.$valid) {

                $scope.loginSent = true;
                $rootScope.load();
                var model = {};
                model.Body = "grant_type=password&username=" + $scope.loginData.email + "&password="
                + encodeURIComponent($scope.loginData.password) + "";
                localStorageService.remove('accessToken');
                return $http.post(window.backendUrl.replace("api/", "token"),
                model.Body, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
                .success(function (data) {
                    $rootScope.unload();
                    $scope.loginSent = $scope.loginSubmitted = false;
                    $scope.loginText = "Login";
                    DB.insert('user', [data.UserName, data.Language, data.Notification, data.ImageURL, data.Mobile, data.FullName, data.Title, data.UnitId, data.Location, data.access_token, data.expires_in], true, function(){
                        authentication.setAuth()
                    });
                    $ionicHistory.nextViewOptions({
                        disableBack: true,
                        historyRoot: true
                    });


                    // $scope.$on("$ionicView.beforeEnter", function(event, data){
                    //
                    //   $state.reload();
                    //
                    // });
                    // $rootScope.userType = 'loggedIn';
                    $ionicHistory.clearCache();
                    $ionicHistory.clearHistory();
                    $rootScope.userType = 'loggedIn';
                    // $ionicNavBarDelegate.showBackButton(false);
                    $rootScope.unload();
                    $state.go("home");
                })
                .error(function (error) {
                    $scope.loginSent = $scope.loginSubmitted = false;
                    $rootScope.unload();
                    if(error != null && error.error === "invalid_grant") {
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('invalid_credentials') || 'Invalid Credentials',
                            $rootScope.langCounterpart('invalid_credentials_message') || 'The email or password you entered is not valid.'
                        );
                    } else if(error != null && error.error === "ezdna-error") {
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('expired_contract') || 'Contract Expired',
                            $rootScope.langCounterpart('expired_contract_message') || 'Your contract has expired, please contact us to solve this issue.'
                        );
                    } else if(error != null && error.error === "Ax Exception") {
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('server_error') || 'Server Error!',
                            $rootScope.langCounterpart('internal_server_problem') || 'Internal Server Error.'
                        );
                    }
                    else {
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('failure') || 'Failure!',
                            $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                        );
                    }
                });
            }
        };
        $scope.resetLoginProblem = function() {
            $scope.invalidLogin = false;
        };

        $scope.goForgotPassword = function() {
            if($rootScope.isOnline) {
                $state.go("forgotPassword");
            } else {
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
            }

        };

        $scope.prevState;
        $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
            if(to.url.replace(/\//g, '') === 'login' && typeof(from) !== 'undefined') {
                $rootScope.prevState = from.url.replace(/\//g, '');
            }
        });

        $scope.goLand = function() {
            $scope.loginData = $scope.errors = {};
            $scope.loginSubmitted = false;
            if(typeof($scope.prevState === 'undefined')) {
                $state.go("land");
            }
            else {
                if($scope.prevState === 'land') {
                    $state.go("land");
                } 
                else {
                    $ionicHistory.nextViewOptions({
                        historyRoot: true
                    });
                    $state.go($rootScope.prevState);
                }
            }
        };

        $scope.guestLogin = function() {
            $ionicHistory.nextViewOptions({
                historyRoot: true
            });
            $state.go("home");
        };
    });
})();
