'use strict';
(function() {
    angular.module('ezdanApp')
    .controller('homeController', function ($scope, $rootScope, $state, $timeout, $location, $ionicSideMenuDelegate, $ionicHistory, $ionicNavBarDelegate, $ionicLoading, homeService) {

        homeService.SEOFaster();
        homeService.getBannerImg();
        $ionicHistory.nextViewOptions({
            disableBack: true,
            disableAnimate: true,
            historyRoot: true
        });
        $scope.imagesURl = window.backendUrl.replace("api","Admin");
        $scope.goAbout = function() {
            $location.path("content/7");
        };
        $scope.goDiscover = function() {
            $location.path("content/3");
        };
        $scope.goSakin = function() {
            $location.path("content/5");
        };
        $scope.goRequest = function() {
            $location.path("request");
        };
        $scope.goProperties = function() {
            $location.path("properties");
        };
    });
})();
