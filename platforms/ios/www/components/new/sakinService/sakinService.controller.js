'use strict';
(function() {
    angular.module('ezdanApp')
    .controller('sakinServiceController', function 
        ($scope, $rootScope, contentService, $state, $location, $timeout, DB) {
            $scope.getSakinDetails = function() {
                if(window.cordova){
                $scope.isIos = cordova.platformId =="ios";
                }
                $rootScope.load();
                contentService.getContent({
                    "id": 5,
                    "lang": $rootScope.lang
                })
                .$promise.then(function (data) {
                    $scope.sakinDetails = data.content;
                    $scope.getSakinLocations();
                }, function(error) {
                    $rootScope.unload();
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('failure') || 'Failure!',
                        $rootScope.langCounterpart('internal_server_error') || 'Internal server error.'
                    );
                    $rootScope.goBack();
                    $rootScope.deleteBackHistory();
                });
            }
            $scope.title = '';
            $scope.$watch('sakinDetails', function(newV, oldV) {
                $timeout(function() {
                    if(typeof(newV) !== 'undefined') $scope.title = newV.title;
                }, 300);
            });
            $scope.sakinLocations = [];
            $scope.getSakinLocations = function() {
                contentService.getContent({
                    "id": 182,
                    "lang": $rootScope.lang
                })
                .$promise.then(function (data) {
                    for(var i = 0; i < data.children.length; i++) {
                        var location = data.children[i];
                        location.thumb = 'https://maps.googleapis.com/maps/api/staticmap?center=' 
                            + location.content.properties.latitude.value 
                            + ',' 
                            + location.content.properties.longitude.value 
                            + '&markers=color:red%7Clabel:C%7C40.718217,-73.998284&zoom=12&size=600x400';
                        $scope.sakinLocations.push(location);
                    }
                    $rootScope.unload();
                }, function(error) {
                    $rootScope.unload();
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('failure') || 'Failure!',
                        $rootScope.langCounterpart('internal_server_error') || 'Internal server error.'
                    );
                    $rootScope.goBack();
                    $rootScope.deleteBackHistory();
                });
            }
            $scope.getSakinDetails();
            $scope.openSakinLocation = function(lat, long) {
                $state.go("map", { lat: lat, long: long });
            }
    });
})();