'use strict';
(function() {
    angular.module('ezdanApp')
    .controller('contactUsController', function 
        ($scope, $rootScope, $state, contentService) {
            $scope.contactInfo = {};
            $scope.contactInfoLoaded = false;
            $scope.getContactUsInfo = function() {
                $rootScope.load();
                contentService.getContent({
                    "id": 6,
                    "lang": $rootScope.lang
                })
                .$promise.then(function (data) {
                    if (data) {
                        $scope.contactInfoLoaded = true;
                        var contactInfo = data.content.properties;
                        $scope.contactInfo = {
                            phone: contactInfo.contactPhone.value,
                            fax: contactInfo.contactFax.value,
                            pobox: contactInfo.contactPOBox.value,
                            poboxAddress: contactInfo.contactPOBoxLocation.value,
                            poboxLat: contactInfo.latitude.value,
                            poboxLong: contactInfo.longitude.value,
                            infoEmail: contactInfo.contactEmail.value,
                            facebook: contactInfo.contactFacebook.value,
                            twitter: contactInfo.contactTwitter.value,
                            instagram: contactInfo.contactInstagram.value,
                            youtube: contactInfo.contactYoutube.value,
                            whatsapp: contactInfo.contactWhatsapp.value,
                            google: contactInfo.contactGoogle.value
                        }
                    }
                    $rootScope.unload();
                }, function(error) {
                    $rootScope.unload();
                    $rootScope.goBack();
                    $rootScope.deleteBackHistory();
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('failure') || 'Failure!',
                        $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                    );
                });
            };
            $scope.getContactUsInfo();
            $scope.navigateToSocialMedia = function(url, target, location) {
                window.open(url, target, location);
            }
            $scope.openPoboxLocation = function(lat, long) {
                $state.go("map", { lat: lat, long: long });
            }
    });
})();
// window.open('fb://page/392749097496450', "_system");