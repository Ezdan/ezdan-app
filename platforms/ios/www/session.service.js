'use strict';
(function() {
    angular
    .module('ezdanApp')
    .factory('sessionService', sessionService);

    // sessionService.$inject = ['$http'];

    function sessionService(){
    return {
       set:function(key,value){
          return localStorage.setItem(key,JSON.stringify(value));
       },
       get:function(key){
         return JSON.parse(localStorage.getItem(key));
       },
       destroy:function(key){
         return localStorage.removeItem(key);
       },
     };
 };
})();
