'use strict';
(function() {
    angular.module('ezdanApp')
    .factory('shareImages', shareImages);

    function shareImages() {
        // var imagesFun = function() {
            // Private variable
            var images = new Array();

            // The object which will be expose as service api
            // var service = {};
            // service.setImages = setImages;
            // service.getImages = getImages;
            //
            // return service;

            var setImages = function (imagesArray) {
                images = imagesArray;
                // return true;
            };

            var getImages = function () {
                return images;
            };
        // };

        shareImages.get = getImages;
        shareImages.set = setImages;

        return shareImages;
    };
})();
