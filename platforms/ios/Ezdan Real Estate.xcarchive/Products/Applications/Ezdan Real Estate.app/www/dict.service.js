'use strict';
(function() {
    angular.module('ezdanApp')
    .factory('dict', dictionary);

    dictionary.$inject = ['$resource','DB','$q'];

    function dictionary($resource,DB,$q) {
        var DictionaryService = {};
        var GetDictionary = function(){
            //check if there isn't data in the localdb || time to refresh localdb
            var q = $q.defer();
            var dictdata = [];
            var localDbResultCount = 0;
            getDictionaryOffline().then(function(data){
                localDbResultCount = data.length;
                if (localDbResultCount == undefined || localDbResultCount == 0) {//-----------------------
                    service.getDict().$promise.then(function(data){
                        //delete previous records
                        DB.delete('dictionary');
                        for (var i = 0; i < data.length; i++) {
                            DB.insert('dictionary',[data[i].Key,data[i].English,data[i].Arabic]);
                        }
                        getDictionaryOffline().then(function(data){
                            dictdata = data;
                            if(dictdata.length > 0){
                                q.resolve(dictdata);
                            }
                            else {
                                q.reject(dictdata);
                            }
                        });
                    })
                }//-----------------------
                else {
                    dictdata = data;
                    q.resolve(dictdata);
                }//-----------------------
            });

            return q.promise;
        };
        //API Service
        var service = $resource(window.backendUrl + 'Dictionary', {}, {
            'getDict': {
                method: 'GET', isArray: true,
                url: window.backendUrl + 'Dictionary/Get'
            }
        });
        //Local Database
        function getDictionaryOffline (){
            return  DB.get("dictionary");
        };
        DictionaryService.GetDictionary = GetDictionary;
        return DictionaryService;
    }
})();
