'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('altThumbList', function() {

        return{
            templateUrl: 'components/cms/templates/altthumblist.html'
        };

    });
})();
