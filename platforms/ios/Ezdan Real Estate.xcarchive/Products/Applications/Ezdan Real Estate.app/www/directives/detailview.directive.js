'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('detailView', function() {

        return{
            templateUrl: 'components/cms/templates/detailsView.html'
        };

    });
})();
