'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('unorderedList', function() {

        return{
            templateUrl: 'components/cms/templates/unorderedlist.html'
        };

    });
})();
