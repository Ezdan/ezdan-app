'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('faqList', function() {

        return{
            templateUrl: 'components/cms/templates/faqlist.html'
        };

    });
})();
