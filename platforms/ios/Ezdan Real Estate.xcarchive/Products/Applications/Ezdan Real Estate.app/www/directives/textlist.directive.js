'use strict';
(function() {
    angular.module('ezdanApp')
    .directive('textList', function() {

        return{
            templateUrl: 'components/cms/templates/textlist.html'
        };

    });
})();
