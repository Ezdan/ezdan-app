'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('phoneAuthController', function($scope, $rootScope, shareImages, $ionicBackdrop, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate, $state, $ionicPlatform, $ionicHistory, DB, user, authentication) {
        $scope.tempUserObj = {}
        DB.get('tempUser').then(function(data) {
            if(data.length && data.length > 0) {
                console.log(data);
                $scope.tempUserObj = data[0];
                $scope.phoneAuthPara1 = ($rootScope.langCounterpart('phone_auth_para_1') || 'An SMS message has been sent to') + ' ';
            }
        })
        $scope.resendPhoneAuthorization = function() {
            $scope.phoneAuthResendSent = true;
            $rootScope.load();
            var phoneAuthIdObj = { 
                id: $scope.tempUserObj.id,
                phoneNumber: $scope.tempUserObj.phone
            };
            user.api().resendPhoneAuthorization(phoneAuthIdObj).$promise.then(function(data) {
                console.log(data);
                if(data.phoneAuthId && Math.floor(data.phoneAuthId) > 0) {
                    DB.update('tempUser', 'id', data.phoneAuthId);
                    $scope.tempUserObj.id = data.phoneAuthId;
                    $rootScope.popup(
                        'success',
                        $rootScope.langCounterpart('new_phone_auth_code') || 'New Verification Code',
                        ($rootScope.langCounterpart('new_phone_auth_code_has_been_sent') || 'An SMS message with a new verification code has been sent.')
                    );
                }
                $rootScope.unload();
                $scope.phoneAuthResendSent = false;
            }, function(error) {
                console.log(error);
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('server_error') || 'Server Error!',
                    $rootScope.langCounterpart('internal_server_problem') || 'Internal Server Error.'
                );
                $rootScope.unload();
            });
        }
        $scope.cancelPhoneAuthorization = function() {
            $scope.phoneAuthCancelSent = true;
            $rootScope.load();
            var phoneAuthIdObj = { id: $scope.tempUserObj.id };
            console.log(phoneAuthIdObj);
            user.api().cancelPhoneAuthorization(phoneAuthIdObj).$promise.then(function(data) {
                console.log(data);
                DB.delete('tempUser');
                $rootScope.pendingPhoneAuth = false;
                $rootScope.phoneAuthModal.hide();
                $scope.phoneAuthCancelSent = false;
                $rootScope.unload();
            }, function(error) {
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('an_error_has_occured') || 'An error has occured.'
                );
                $rootScope.unload();
            });
        }
        $scope.submitPhoneAuthorization = function(form) {
            $scope.phoneAuthSubmitted = true;
            if(form.$valid) {
                $scope.phoneAuthSent = true;
                $rootScope.load();
                var phoneAuthObj = { 
                    id: $scope.tempUserObj.id,
                    code: $scope.tempUserObj.phoneAuthCode
                }
                console.log(phoneAuthObj);
                user.api().submitPhoneAuthorization(phoneAuthObj).$promise.then(function (data) {
                    console.log(data);
                    if(data.message) {
                        switch(data.message) {
                            case 'mismatch':
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('failure') || 'Failure!',
                                $rootScope.langCounterpart('incorrect_code') || 'Incorrect verification code.'
                            );
                            $scope.phoneAuthSent = $scope.phoneAuthSubmitted = false;
                            $rootScope.unload();
                            break;
                            case 'success':
                            $scope.register();
                            break;
                        }
                    }
                }, function (error) {
                    console.log(error);
                    $scope.phoneAuthSent = $scope.phoneAuthSubmitted = false;
                    $rootScope.unload();
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('server_error') || 'Server Error!',
                        $rootScope.langCounterpart('internal_server_problem') || 'Internal Server Error.'
                    );
                });
            }
        }
        $scope.register = function() {
            var modelObj = {
                Title: $scope.tempUserObj.title,
                FullName: $scope.tempUserObj.email,
                Email: $scope.tempUserObj.name,
                PhoneNumber: $scope.tempUserObj.phone,
                Password: $scope.tempUserObj.password,
                ConfirmPassword: $scope.tempUserObj.passwordConf,
                ContractNumber: $scope.tempUserObj.contractNum,
                Language: $scope.tempUserObj.lang,
                Account: $scope.tempUserObj.AXAccountId,
                PartyRecId: Math.floor($scope.tempUserObj.AXPartyRecId),
                Location: $scope.tempUserObj.AXLocation,
                Unit: $scope.tempUserObj.AXUnitId
            }
            console.log(modelObj);
            user.api().register(modelObj).$promise.then(function (data) {
                $scope.phoneAuthSent = $scope.phoneAuthSubmitted = false;
                $rootScope.unload();
                console.log(data);
                if(data.access_token && data.UserName) {
                    console.log(data);
                    DB.insert('user',
                        [
                            data.UserName,
                            data.Language,
                            data.Noifitcation, 
                            null, 
                            data.PhoneNumber, 
                            data.FullName, 
                            data.Title, 
                            data.UnitId, 
                            data.Location, 
                            data.access_token,
                            data.expires_in
                        ],
                        true,
                        function() {
                            authentication.setAuth();
                        });
                    DB.delete('tempUser');
                    $rootScope.pendingPhoneAuth = false;
                    $rootScope.phoneAuthModal.hide();
                    // $scope.registrationData = {};
                    $ionicHistory.nextViewOptions({
                        disableBack: true,
                        historyRoot: true
                    });
                    $ionicHistory.clearCache();
                    $ionicHistory.clearHistory();
                    authentication.setAuth();
                    $rootScope.userType = 'loggedIn';
                    $rootScope.unload();
                    $state.go("home");
                }
            }, function (error) {
                console.log(error);
                $scope.phoneAuthSent = $scope.phoneAuthSubmitted = false;
                $rootScope.unload();
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('server_error') || 'Server Error!',
                    $rootScope.langCounterpart('internal_server_problem') || 'Internal Server Error.'
                );
            });
        }
    });
})();









