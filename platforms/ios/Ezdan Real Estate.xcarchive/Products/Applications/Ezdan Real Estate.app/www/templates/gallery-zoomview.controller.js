'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('galleryZoomViewController', function($scope, $rootScope, shareImages, $ionicBackdrop, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate, $state, $ionicPlatform, $ionicHistory) {

        $scope.zoomMin = 1;
        $scope.Images = new Array();
        $scope.Images = shareImages.get();
        console.log($scope.Images);

        $scope.closeModal = function() {
            $scope.modal.hide();
            $scope.modal.remove()
        };

        $ionicPlatform.registerBackButtonAction(function () {
            if ($rootScope.modalActive == true) {
                $rootScope.modalActive = false;
                $scope.closeModal();
            // } else if ($state.is('chooseLanguage') || ($state.is('home') && $rootScope.userType === 'loggedIn')){
            } else if ($state.is('land') || ($state.is('home') && $rootScope.userType === 'loggedIn')){
                navigator.app.exitApp();
            } else if($state.is('login') || $state.is('register') || ($state.is('home') && $rootScope.userType === 'guest')) {
                $state.go('land');
            // } else if($state.is('land')) {
            //     $state.go('chooseLanguage');
            } else if($state.is('setPassword')) {
                $state.go('forgotPassword');
            } else if($state.is('forgotPassword')) {
                $state.go('login');
            } else if ($ionicHistory.backView() != null) {
                $ionicHistory.goBack();
            } else {
                $ionicHistory.nextViewOptions({
                    disableBack: true,
                    disableAnimate: true,
                    historyRoot: true
                });
                $state.go('home');
            }
        }, 600);

        $scope.zoomOnDoubleTap = function(slide) {
            $scope.zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
            if ($scope.zoomFactor == $scope.zoomMin) {
                $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).zoomBy(2,true);
            } else {
                $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).zoomBy(0.5,true);
            }
        };

        $scope.updateSlideStatus = function(slide) {
            $scope.zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
            if ($scope.zoomFactor == $scope.zoomMin) {
                $ionicSlideBoxDelegate.enableSlide(true);
            } else {
                $ionicSlideBoxDelegate.enableSlide(false);
            }
        };

    })
    .directive('srcImg', function(){
        return function(scope, element, attrs){
            var url = attrs.srcImg;
            element.css({
                'background-image': 'url("' + url + '")'
            });
        };
    });
})();
