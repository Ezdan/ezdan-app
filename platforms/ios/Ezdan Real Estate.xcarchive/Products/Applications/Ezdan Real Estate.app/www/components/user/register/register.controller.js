'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('registerController', function ($scope, $rootScope, $state, user, localStorageService, $ionicSideMenuDelegate, $ionicHistory,DB,authentication) {
        $ionicSideMenuDelegate.canDragContent(false);
        $scope.showPassword = false;
        // $scope.pass = "John Doe";
        $scope.regex = {
            contract: /^([a-zA-Z0-9]){4,}$/,
        };
        $scope.registrationData = {};
        $scope.registerText = "Register";
        $scope.registrationSubmitted = false;
        $scope.registrationSent = false;
        $scope.errors = {};
        $scope.authorizePhone = function(form) {
            $scope.registrationSubmitted = true;
            if(form.$valid) {
                $scope.registrationSent = true;
                $rootScope.load();
                var phoneObj = { 
                    Email: $scope.registrationData.email, 
                    Password: $scope.registrationData.password, 
                    ConfirmPassword: $scope.registrationData.passwordConfirm, 
                    ContractNumber: $scope.registrationData.contractNum, 
                    PhoneNumber: $scope.registrationData.phone 
                };
                user.api().authorizePhone(phoneObj).$promise.then(function (data) {
                    if(data.phoneAuthId && data.phoneAuthId > 0) {
                        DB.insert('tempUser',
                            [
                                data.phoneAuthId,
                                $scope.registrationData.title,
                                $scope.registrationData.name,
                                $scope.registrationData.email,
                                $scope.registrationData.phone,
                                $scope.registrationData.password,
                                $scope.registrationData.passwordConfirm,
                                $scope.registrationData.contractNum,
                                data.AXUser.AccountNumber,
                                data.AXUser.PartyRecId,
                                data.AXUser.City,
                                data.AXUser.UnitId,
                                $rootScope.lang
                            ],
                            true,
                            function() {
                                DB.delete('user');
                                $rootScope.pendingPhoneAuth = true;
                            });
                    }
                    $scope.registrationSubmitted = $scope.registrationSent = false;
                    $rootScope.unload();
                }, function (error) {
                    $scope.registrationSubmitted = $scope.registrationSent = false;
                    $rootScope.unload();
                    if(error.data.error) {
                        switch(error.data.error) {
                            case 'passwordStrength':
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('weak_password') || 'Weak Password',
                                $rootScope.langCounterpart('password_strength_tip') || 'Password must be at least 6 characters long, contain a lowercase letter, an uppercase letter, a digit and a special character (e.g. @).'
                            );
                            break;
                            case 'duplicateEmail':
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('email_exists') || 'Email Exists!',
                                $rootScope.langCounterpart('email_address_already_registered') || 'Sorry! The email is already registered.'
                            );
                            break;
                            case 'duplicatePhone':
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('phone_exists') || 'Phone Exists',
                                $rootScope.langCounterpart('phone_number_already_registered') || 'This phone number is already registered.'
                            );
                            break;
                            case 'duplicateContract':
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('contract_exists') || 'Contract Exists!',
                                $rootScope.langCounterpart('contract_number_already_registered') || 'Sorry! The contract number is already registered.'
                            );
                            break;
                            case 'invalidContract':
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('failure') || 'Failure!',
                                $rootScope.langCounterpart('invalid_contract_number') || 'Invalid contract number.'
                            );
                            break;
                            case 'SMSError':
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('server_error') || 'Server Error!',
                                $rootScope.langCounterpart('internal_server_problem') || 'Internal Server Error.'
                            );
                            case 'AXDisconnection':
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('failure') || 'Failure!',
                                $rootScope.langCounterpart('could_not_connect_server') || 'Could not connect to server.'
                            );
                            break;
                            break;
                            case 'phoneMismatch':
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('mismatch') || 'Mismatch',
                                $rootScope.langCounterpart('phone_and_contract_mismatch') || 'Phone number does not match contract number.'
                            );
                            break;
                        }
                        return;
                    }
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('server_error') || 'Server Error!',
                        $rootScope.langCounterpart('internal_server_problem') || 'Internal Server Error.'
                    );
                });
            }
        }
        $scope.goLand = function() {
            $scope.registrationData = {
                title: 'Mr.',
                email: null
            };
            $scope.registrationSubmitted = false;
            $state.go("land");
        }
    });
})();
