'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('accountSettingsController', function ($scope , $ionicPopup, $rootScope, $state, $ionicSideMenuDelegate, $ionicHistory, user, localStorageService, DB, authentication) {
        var isTrueSet = ($rootScope.user.notifications === 'true');
        $scope.oldNotificationValue = {
            "value": isTrueSet
        };
        $scope.passwordModel = {};
        $scope.changePasswordSubmitted = false;

        $rootScope.loading = false;
        $scope.toggleLanguage = function(oldValue) {
            $rootScope.load();
            user.api().updateLanugage({
                ChangedSetting: 1,
                UpdatedValue: $rootScope.lang === 'English' ? '1' : '2'
            }).$promise.then(function(data) {
                if(data.Updated) {
                    DB.update('user','language',$rootScope.lang);
                    authentication.setAuth();
                }
                $rootScope.unload();
            }, function(error) {
                $rootScope.lang = oldValue;
                $rootScope.unload();
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
            })
        };

        $scope.toggleNotifications = function() {

            $rootScope.load();
            user.api().updateNotifications({
                ChangedSetting: 2,
                UpdatedValue: $scope.oldNotificationValue.value
            }).$promise.then(function(data) {
                if(data.Updated) {
                    DB.update('user','notification', $scope.oldNotificationValue.value);
                    $rootScope.user.notifications = $scope.oldNotificationValue.value;
                }
                $rootScope.unload();
            }, function(error) {
                $rootScope.unload();
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
            });
        };

        $scope.changePassword = function (form) {
            $scope.changePasswordSubmitted = true;
            $rootScope.load();
            if (form.$valid) {
                user.api().changePassword($scope.passwordModel).$promise.then(function(data) {
                    $rootScope.unload();
                    $scope.showChangePassConfirmation();
                }, function(error) {
                    if (error.status == 400) {
                        $rootScope.unload();
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('password_incorrect') || 'Password Incorrect',
                            $rootScope.langCounterpart('password_incorrect') || 'Your old password is incorrect, please try again using the correct password.'
                        );
                    } else {
                        $rootScope.unload();
                        $rootScope.popup('error', 'failure');
                    }

                });
            } else {
                $rootScope.unload();
                // $scope.changePasswordSubmitted = false;
            }
        };

        $scope.showChangePassConfirmation = function () {
            $ionicPopup.show({
                template: 'Your password has been changed successfully!',
                title: 'Confirmation!',
                scope: $scope,
                buttons: [
                    {
                        text: '<b>Confirm</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            $rootScope.deleteBackHistory();
                            $state.go("home");
                        }
                    }
                ]
            });
        };

    });
})();
