'use strict';
(function () {
    angular
        .module('ezdanApp')
        .controller('appointmentController', function ($rootScope, appointmentService, $scope, $stateParams, $state, $cookies, $http, $ionicPopup, $window, authentication, uiCalendarConfig) {
            $scope.language = $rootScope.lang;
            $scope.title = "Appointments";
            $scope.eventSources = [];
            $scope.events = [];
            $scope.loaded = true;
            $scope.userName = authentication.authData.fullName;
            $scope.historyButton = "globalTab";
            $scope.activeButton = "globalTab";
            $scope.bookButton = "globalTab active";
            $scope.MyAppointmentsClass = "button  button-block button-assertive";
            $scope.ViewCalendarClass = "globalTab";
            $scope.activeAppointments = false;
            $scope.historyEvents = [];
            $scope.activeEvents = [];
            $scope.tomorrow = new Date(+new Date() + 86400000);
            $scope.showHistory = function () {
                $scope.activeAppointments = false;
                $scope.activeButton = "globalTab";
                $scope.bookButton = "globalTab";
                $scope.historyButton = "globalTab active";
                $scope.HideCalendar();
            }
            $scope.showActive = function () {
                $scope.activeAppointments = true;
                $scope.activeButton = "globalTab active";
                $scope.bookButton = "globalTab";
                $scope.historyButton = "globalTab";
                $scope.HideCalendar();
            }
            $scope.today = new Date().toISOString();

            $scope.$watch(function () {
                return $rootScope.user;
            }, function (user) {
                if (user != "" && typeof (user) !== 'undefined') {
                    if (user.token != "") {
                        $scope.GetDiaryEvents(user.token);
                    }
                    else {
                        $scope.GetGuestCalendar();

                    }
                }

            })
            $scope.GetHistoryAndActiveEvents = function () {
                for (var i = 0; i < $scope.events.length; i++) {
                    if ($scope.events[i].start <= $scope.today)
                        $scope.historyEvents.push($scope.events[i]);
                    else {
                        $scope.activeEvents.push($scope.events[i]);

                    }
                }
            }
            $scope.GetGuestCalendar = function () {
                $rootScope.load();
                appointmentService.authorize().GetGuestCalendar().$promise.then(function (data) {
                    if (data) {
                        $rootScope.unload();
                        $scope.events = [];
                        $scope.weekEnds = data.weekEnds;
                        $scope.businessHours = data.businessHours;
                        $scope.holidays = data.holidays;
                        $scope.isRegistered = false;
                        var date = new Date();
                        var d = date.getDate();
                        var m = date.getMonth();
                        var y = date.getFullYear();
                        $scope.eventSource = {
                            className: 'gcal-event',
                            currentTimezone: 'auto'
                        };
                        $scope.isHoliday = false;
                        $scope.checkHolidays = function (date, holidaysArray) {
                            for (var i = 0; i < holidaysArray.length; i++) {
                                var holidayStartWithoutTime = holidaysArray[i].start.substring(0, 10);
                                var holidayEndWithoutTime = holidaysArray[i].end.substring(0, 10);
                                if (date >= holidayStartWithoutTime && date < holidayEndWithoutTime) {
                                    $scope.isHoliday = true;
                                    break;
                                } else {
                                    $scope.isHoliday = false;
                                }
                            }
                        };
                        // On day click
                        $scope.alertOnDayClick = function (date, jsEvent, view) {
                            var dateClicked = date.format('YYYY-MM-DD');
                            var now = moment().add(12, 'hours').format('YYYY-MM-DD HH:mm:ss');
                            var inputDate = moment().format(date.format('YYYY-MM-DD HH:mm:ss'));
                            $scope.checkHolidays(dateClicked, $scope.holidays);
                            if (inputDate < now) {
                                $scope.showAlert(
                                    $rootScope.langCounterpart("you_have_to_book_before_12_hours") || "Sorry! You have to book before 12 hours",
                                    '', 
                                    false
                                )
                            } else {
                                if ($scope.isHoliday) {
                                    $scope.showAlert(
                                        $rootScope.langCounterpart("holiday_excl") || "Holiday!",
                                        '', 
                                        false
                                    )
                                } else if (!(date.format('HH:mm:ss') >= $scope.businessHours[2] && date.format('HH:mm:ss') < $scope.businessHours[3])) {
                                    $scope.SelectedDate = date.format('YYYY/MM/DD HH:mm:ss');
                                    if (view.name == "month")
                                        $('#calendar').fullCalendar('changeView', 'agendaDay', date);
                                    else
                                        $scope.showPopupGuest($scope.SelectedDate);
                                } else {
                                    $scope.showAlert(
                                        $rootScope.langCounterpart("break_hour_excl") || "Break Hour!", 
                                        '', 
                                        false
                                    )
                                }
                            }
                        };
                        $scope.eventRender = function (event, element, view) {
                            element.attr({
                                'tooltip': event.title,
                                'tooltip-append-to-body': true
                            });
                        };
                        $scope.eventSources = [$scope.events, $scope.eventSource];
                        $scope.eventSources2 = [$scope.events];
                        // Calendar Configuration
                        $scope.uiConfig = {
                            calendar: {
                                customButtons: {
                                    tomorrow: {
                                        text: 'Tomorrow',
                                        click: function () {
                                            $('#calendar').fullCalendar('gotoDate', $scope.tomorrow);

                                        }
                                    }
                                },
                                header: {
                                    left: 'prev',
                                    center: 'tomorrow,month,agendaWeek,agendaDay,title',
                                    right: 'next'
                                },
                                buttonText: {
                                    today: 'Today',
                                    day: 'Day',
                                    month: 'Month',
                                    week: 'Week'
                                },
                                defaultDate: $scope.tomorrow,
                                theme: true,
                                selectable: true,
                                defaultView: 'agendaDay',
                                editable: true,
                                height: 'auto',
                                hiddenDays: $scope.weekEnds,
                                ignoreTimezone: false,
                                businessHours: [
                                    {
                                        start: $scope.businessHours[0],
                                        end: $scope.businessHours[2],
                                        dow: [0, 1, 2, 3, 4, 5, 6]
                                    },
                                    {
                                        start: $scope.businessHours[3],
                                        end: $scope.businessHours[1],
                                        dow: [0, 1, 2, 3, 4, 5, 6]
                                    }
                                ],
                                noEventsMessage: "You have no appointments this week!",
                                minTime: $scope.businessHours[0],
                                maxTime: $scope.businessHours[1],
                                defaultEventMinutes: 60,
                                allDaySlot: false,
                                timezone: 'local',
                                dayClick: $scope.alertOnDayClick,
                            }
                        };
                    }
                },
                function (error) {
                    $rootScope.unload();
                });
            }
            $scope.GetDiaryEvents = function (token) {
                $rootScope.load();
                appointmentService.authorize(token).GetDiaryEvents().$promise.then(function (data) {
                    if (data) {
                        $rootScope.unload();
                        $scope.events = data.events;
                        $scope.GetHistoryAndActiveEvents();
                        $scope.weekEnds = data.weekEnds;
                        $scope.businessHours = data.businessHours;
                        $scope.holidays = data.holidays;
                        $scope.isRegistered = true;
                        var date = new Date();
                        var d = date.getDate();
                        var m = date.getMonth();
                        var y = date.getFullYear();
                        $scope.eventSource = {
                            className: 'gcal-event',
                            currentTimezone: 'auto'
                        };
                        $scope.isHoliday = false;
                        $scope.checkHolidays = function (date, holidaysArray) {
                            for (var i = 0; i < holidaysArray.length; i++) {
                                var holidayStartWithoutTime = holidaysArray[i].start.substring(0, 10);
                                var holidayEndWithoutTime = holidaysArray[i].end.substring(0, 10);
                                if (date >= holidayStartWithoutTime && date < holidayEndWithoutTime) {
                                    $scope.isHoliday = true;
                                    break;
                                } else {
                                    $scope.isHoliday = false;
                                }
                            }
                        };
                        // On day click
                        $scope.alertOnDayClick = function (date, jsEvent, view) {
                            var dateClicked = date.format('YYYY-MM-DD');
                            var now = moment().add(12, 'hours').format('YYYY-MM-DD HH:mm:ss');
                            var inputDate = moment().format(date.format('YYYY-MM-DD HH:mm:ss'));
                            $scope.checkHolidays(dateClicked, $scope.holidays);
                            if (inputDate < now) {
                                $scope.showAlert(
                                    $rootScope.langCounterpart("you_have_to_book_before_12_hours") || "Sorry! You have to book before 12 hours",
                                    '', 
                                    false
                                )
                            } else {
                                if ($scope.isHoliday) {
                                    $scope.showAlert(
                                        $rootScope.langCounterpart("holiday_excl") || "Holiday!",
                                        '', 
                                        false
                                    )
                                } else if (!(date.format('HH:mm:ss') >= $scope.businessHours[2] && date.format('HH:mm:ss') < $scope.businessHours[3])) {
                                    $scope.SelectedDate = date.format('YYYY/MM/DD HH:mm:ss');
                                    if (view.name == "month")
                                        $('#calendar').fullCalendar('changeView', 'agendaDay', date);
                                    else
                                        $scope.showPopup($scope.SelectedDate);
                                } else {
                                    $scope.showAlert(
                                        $rootScope.langCounterpart("break_hour_excl") || "Break Hour!", 
                                        '', 
                                        false
                                    )
                                }
                            }
                        };
                        // On event click
                        $scope.alertOnEventClick = function (date, jsEvent, view) {
                            if (view.name.substring(0, 4) == 'list')
                                $('#calendar').fullCalendar('changeView', 'agendaDay', date.start);
                            else {
                                $scope.cancelEvent(date);
                            }
                        };
                        $scope.ShowAppointments = function () {
                            $scope.loaded = false;
                            $scope.ViewCalendarClass = "button button-outline  button-block button-assertive";
                            $scope.MyAppointmentsClass = "button button-block button-assertive";
                        }
                        $scope.ShowCalendar = function () {
                            $scope.loaded = true;
                            $scope.activeButton = "globalTab";
                            $scope.bookButton = "globalTab active";
                            $scope.historyButton = "globalTab";
                        }
                        $scope.HideCalendar = function () {
                            $scope.loaded = false;

                        }
                        $scope.onMyAppointmentsClick = function (event) {
                            //var date=new Date(event.start);
                            $scope.cancelEvent(event)
                        }
                        $scope.alertOnDrop = function (event, delta, revertFunc, jsEvent, ui, view) {
                            $rootScope.load();
                            $scope.dropDate = event.start.format();
                            $scope.dropId = event.id;
                            // Handle Break Hour
                            if (!(event.start.format('HH:mm:ss') >= $scope.businessHours[2] && event.start.format('HH:mm:ss') < $scope.businessHours[3])) {
                                appointmentService.authorize($rootScope.user.token).EditAppointment({
                                    "subject": "Edit an appointment",
                                    "date": $scope.dropDate,
                                    "id": $scope.dropId
                                })
                                .$promise.then(function (data) {
                                    $rootScope.unload();
                                    if (data.response == true) {
                                        $rootScope.popup(
                                            'success',
                                            null,
                                            $rootScope.langCounterpart('appointment_rescheduled') || 'Your appointment has been reschedueled successfully! Please wait for a confirmation mail.' 
                                            + ' ' + $rootScope.langCounterpart('we_will_contact_you') || 'We will contact you soon.'
                                        );
                                    }
                                });
                            } else {
                                $rootScope.unload();
                                revertFunc();
                                $scope.showAlert(
                                    $rootScope.langCounterpart("break_hour_excl") || "Break Hour!", 
                                    '', 
                                    false
                                )
                            }
                        };
                        $scope.alertOnResize = function (event, delta, revertFunc, jsEvent, ui, view) {
                            $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
                        };
                        $scope.eventRender = function (event, element, view) {
                            element.attr({
                                'tooltip': event.title,
                                'tooltip-append-to-body': true
                            });
                        };
                        $scope.eventSources = [$scope.events, $scope.eventSource];
                        $scope.eventSources2 = [$scope.events];
                        // Calendar Configuration
                        $scope.tomorrow = new Date(+new Date() + 86400000);
                        $scope.uiConfig = {
                            calendar: {
                                customButtons: {
                                    tomorrow: {
                                        text: 'Tomorrow',
                                        click: function () {
                                            $('#calendar').fullCalendar('gotoDate', $scope.tomorrow);

                                        }
                                    }
                                },
                                header: {
                                    left: 'prev',
                                    center: 'tomorrow,month,agendaWeek,agendaDay,title',
                                    right: 'next'
                                },

                                buttonText: {
                                    today: 'Today',
                                    day: 'Day',
                                    month: 'Month',
                                    week: 'Week'
                                },
                                defaultDate: $scope.tomorrow,
                                theme: true,
                                selectable: true,
                                defaultView: 'agendaDay',
                                editable: true,
                                height: 'auto',
                                hiddenDays: $scope.weekEnds,
                                ignoreTimezone: false,
                                businessHours: [
                                    {
                                        start: $scope.businessHours[0],
                                        end: $scope.businessHours[2],
                                        dow: [0, 1, 2, 3, 4, 5, 6]
                                    },
                                    {
                                        start: $scope.businessHours[3],
                                        end: $scope.businessHours[1],
                                        dow: [0, 1, 2, 3, 4, 5, 6]
                                    }],
                                noEventsMessage: "You have no appointments this week!",
                                minTime: $scope.businessHours[0],
                                maxTime: $scope.businessHours[1],
                                defaultEventMinutes: 60,
                                allDaySlot: false,
                                timezone: 'local',
                                eventDragStart: $scope.drag,
                                dayClick: $scope.alertOnDayClick,
                                eventClick: $scope.alertOnEventClick,
                                eventDrop: $scope.alertOnDrop,
                                eventResize: $scope.alertOnResize,
                                eventRender: $scope.eventRender,
                                eventAfterRender: function (event, element, view) {
                                    if (event.color == "green") {
                                        element.css('background-color', '#168405');
                                    } else {
                                        element.css('background-color', '#c12127');


                                    }
                                },
                            }
                        };
                    }
                }, function (error) {
                    $rootScope.unload();
                });

            }
            $scope.forms = { appointmentForm: {} };
            $scope.surveySubmitted = false;
            // To Create Appointment
            $scope.showPopupGuest = function (date) {
                $scope.data = {
                    datetimeValue: date
                };
                // cordova.plugins.Keyboard.disableScroll(false);
                var appointmentPopup = $ionicPopup.show({
                    templateUrl: 'templates/survey-submission.html',
                    title: $rootScope.langCounterpart("book_an_appointment") || "Book an Appointment",
                    scope: $scope,
                    buttons: [
                        { text: $rootScope.langCounterpart("cancel") || "Cancel" },
                        {
                            text: $rootScope.langCounterpart("book") || "Book",
                            type: 'button-assertive',
                            onTap: function (e) {
                                e.preventDefault();
                                $scope.surveySubmitted = true;
                                if($scope.forms.appointmentForm.$valid) {
                                    $rootScope.load();
                                    appointmentService.authorize().CreateAppointment({
                                        "subject": "Book an appointment",
                                        "date": $scope.data.datetimeValue,
                                        "name": $scope.data.name,
                                        "email": $scope.data.email,
                                        "title": $scope.data.title,
                                        "mobile": $scope.data.mobile
                                    })
                                    .$promise.then(function (data) {
                                        $scope.surveySubmitted = false;
                                        $rootScope.unload();
                                        if (data.response == true) {
                                            $rootScope.popup(
                                                'success',
                                                $rootScope.langCounterpart('thank_you') || 'Thank You',
                                                $rootScope.langCounterpart('we_will_contact_you') || 'We will contact you soon.'
                                            );
                                        }
                                    }, function (error) {
                                        $scope.surveySubmitted = false;
                                        $rootScope.unload();
                                        $rootScope.popup(
                                            'error',
                                            $rootScope.langCounterpart('server_error') || 'Server Error!',
                                            $rootScope.langCounterpart('internal_server_problem') || 'Internal Server Error.'
                                        );
                                    });
                                    appointmentPopup.close();
                                } 
                            }
                        }
                    ]
                });
                appointmentPopup.then(function(res) {
                    // cordova.plugins.Keyboard.disableScroll(true);
                    console.log('Tapped!', res);
                });
            };
            $scope.showPopup = function (date) {
                $scope.data = {
                    datetimeValue: date,
                    name: $rootScope.user.fullName,
                    mobile: $rootScope.user.mobile,
                    email: $rootScope.user.email,
                };
                var appointmentPopup = $ionicPopup.show({
                    templateUrl: 'templates/survey-submission.html',
                    title: $rootScope.langCounterpart("book_an_appointment") || "Book an Appointment",
                    scope: $scope,
                    buttons: [
                        { text: $rootScope.langCounterpart("cancel") || "Cancel" },
                        {
                            text: $rootScope.langCounterpart("book") || "Book",
                            type: 'button-assertive',
                            onTap: function (e) {
                                e.preventDefault();
                                $scope.surveySubmitted = true;
                                if($scope.forms.appointmentForm.$valid) {
                                    $rootScope.load();
                                    appointmentService.authorize($rootScope.user.token).CreateAppointment({
                                        "subject": "Book an appointment",
                                        "date": $scope.data.datetimeValue,
                                        "name": $scope.data.name,
                                        "email": $scope.data.email,
                                        "title": $scope.data.title,
                                        "mobile": $scope.data.mobile,
                                        "location": $rootScope.user.location,
                                        "unit": $rootScope.user.unit
                                    })
                                    .$promise.then(function (data) {
                                        $scope.surveySubmitted = false;
                                        $rootScope.unload();
                                        if (data.response == true) {
                                            $rootScope.popup(
                                                'success',
                                                $rootScope.langCounterpart('thank_you') || 'Thank You',
                                                $rootScope.langCounterpart('we_will_contact_you') || 'We will contact you soon.'
                                            );
                                            $state.reload();
                                        }
                                    }, function (error) {
                                        $scope.surveySubmitted = false;
                                        $rootScope.unload();
                                        $rootScope.popup(
                                            'error',
                                            $rootScope.langCounterpart('server_error') || 'Server Error!',
                                            $rootScope.langCounterpart('internal_server_problem') || 'Internal Server Error.'
                                        );
                                    });
                                    appointmentPopup.close();
                                }
                            }
                        }
                    ]
                });
            };
            $scope.showAlert = function(title,message,reload) {
                // cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
                var alertPopup = $ionicPopup.alert({
                    title: title,
                    template: message,
                    buttons:[
                        {
                            text: $rootScope.langCounterpart('ok') || 'OK',
                            type: 'button-assertive',
                            onTap: function(e) {
                            if(reload==true)
                                $state.reload();
                            }
                        }
                    ]
                }).then(function(res) {
                    // cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                });
            };
            $scope.cancelEvent = function (event) {
                $scope.event = event;
                var alertPopup = $ionicPopup.alert({
                    title: '',
                    template: '',
                    cssClass: 'popupNoHeader',
                    buttons: [
                        { text: $rootScope.langCounterpart("cancel") || "Cancel" },
                        {
                            text: $rootScope.langCounterpart("delete") || "Delete",
                            type: 'button-assertive',
                            onTap: function (e) {
                                var alertPopup = $ionicPopup.alert({
                                    title: $scope.event.title === 'No title' ? $rootScope.langCounterpart("cancel_appointment") || "Cancel Appointment" : $scope.event.title,
                                    template: $rootScope.langCounterpart("cancel_appointment_confirmation") || "Are you sure you want to cancel this appointment?",
                                    buttons: [
                                        { text: $rootScope.langCounterpart("no") || "No" },
                                        {
                                            text: $rootScope.langCounterpart("yes") || "Yes",
                                            type: 'button-assertive',
                                            onTap: function (e) {
                                                $rootScope.load();
                                                appointmentService.authorize($rootScope.user.token).CancelAppointment({ 
                                                    "id": event.id, 
                                                    "Subject": "Cancel Appointment", 
                                                    "name": $scope.userName, 
                                                    "email": $scope.userEmail, 
                                                })
                                                .$promise.then(function (data) {
                                                    if (data.response == true) {
                                                        $rootScope.unload();
                                                        $rootScope.popup(
                                                            'success',
                                                            null,
                                                            $rootScope.langCounterpart('appointment_cancelled') || 'Appointment has been cancelled.'
                                                        );
                                                        $state.reload();
                                                    }
                                                });
                                            }
                                        }
                                    ]
                                });
                            }
                        }
                    ]
                });
            }
        });
})();
