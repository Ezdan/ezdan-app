'use strict';
(function() {
    angular
        .module('ezdanApp')
        .factory('plusService', plusService);

        plusService.$inject = ['$resource'];

        function plusService($resource) {
            return {
                authorize: function(token) {
                    var service = $resource(window.backendUrl + 'Account', {}, {
                        'inquirePlus': {
                            method: 'POST', isArray: false,
                            headers: { 'Authorization': 'Bearer ' + token },
                            url: window.backendUrl + 'PlusInquiries'
                        }
                    });
                    return service;
                }
            }
        }
})();
