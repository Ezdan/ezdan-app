'use strict';
(function() {
    angular
        .module('ezdanApp')
        .factory('photogalleryService', photogalleryService);

        photogalleryService.$inject = ['$resource'];

        function photogalleryService($resource) {
            var service = $resource(window.backendUrl+'photogallery', {}, {
                'getAlbums': {
                    method: 'GET', isArray: false,
                    url:window.backendUrl+'photogallery/getalbums'
                },
                'getAlbumPhotos': {
                    method: 'GET', isArray: false,
                    url:window.backendUrl+'photogallery/getalbumphotos'
                }

            });
            return service;
        }
})();
