'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('searchController', function ($scope, user, $rootScope, $state, $ionicHistory, $ionicFilterBar,  searchService) {
        $scope.searchList = [];
        $scope.searchUsed = false;
        $scope.searchResults = false;
        $scope.searchedInEnglish = false;
        $scope.searchModel = {
            userType: null,
            keyword: ""
        };
        $scope.$watch(function() {
            return $rootScope.userType;
        }, function(newType) {
            $scope.searchModel.userType = newType;
        });
        $scope.updateSearch = function(searchText) {
            $scope.searchModel.keyword = searchText;
            searchService.search().onKeyup($scope.searchModel).$promise.then(function(data) {
                if(data.Results && data.Results instanceof Array) {
                    $scope.searchUsed = true;
                    if(data.Results.length > 0) {
                        $scope.searchResults = true;
                        $scope.searchList = data.Results;
                        $scope.searchedInEnglish = data.SearchedInEnglish;
                    }
                    else {
                        $scope.searchResults = false;
                        $scope.searchList = [
                            {
                                Name: "No results."
                            }
                        ];
                    }
                    $scope.search($scope.searchList);
                }
            }, function(error) {
            });
        };
        $scope.search = function(searchList) {
            $ionicFilterBar.show({
                items: searchList || [],
                update: function (filteredItems, filterText) {
                    console.log(filteredItems.length);
                    if (filterText) {
                        console.log($scope.searchResults);
                        $scope.updateSearch(filterText);
                    }
                    else {
                        $scope.searchUsed = false;
                    }
                },
                cancel: function() {
                    $scope.searchUsed = false;
                },
                expression: function(filterText,value,index,array){
                    return value.name.toLowerCase().indexOf(filterText.toLowerCase()) > -1;
                    // This function is called for every ParentData object.
                    // "value" will be the current ParentData object,
                    // you'll need to iterate over all the "child" array in order to
                    // check if any of the "child_type" keys contains the "filterText"
                    // so if "parent_type" contains the "filterText" and any(? or all)
                    // of the the "child[x].child_type" values also contains the
                    // "filterText" return true
                }
            });
        };
        // $scope.searchList = [
        //     {
        //         "id": 1,
        //         "name": "Properties",
        //         "href": "#/properties"
        //     },
        //     {
        //         "id": 2,
        //         "name": "Services",
        //         "href": "#/sakin"
        //     },
        //     {
        //         "id": 3,
        //         "name": "Ezdan Plus",
        //         "href": "#/plus"
        //     },
        //     {
        //         "id": 4,
        //         "name": "Surveys",
        //         "href": "#/surveys"
        //     },
        //     {
        //         "id": 5,
        //         "name": "Feedback",
        //         "href": "#/feedback"
        //     },
        //     {
        //         "id": 6,
        //         "name": "Settings",
        //         "href": "#/settings"
        //     },
        //     {
        //         "id": 7,
        //         "name": "Discount Cards",
        //         "href": "#/cardTypes"
        //     },
        //     {
        //         "id": 8,
        //         "name": "Announcements",
        //         "href": "#/announcements"
        //     },
        //     {
        //         "id": 9,
        //         "name": "Service Request",
        //         "href": "#/request"
        //     },
        //     {
        //         "id": 10,
        //         "name": "FAQs",
        //         "href": "#/faqs"
        //     },
        //     {
        //         "id": 11,
        //         "name": "About",
        //         "href": "#/about"
        //     }
        // ];
    }).filter('orderSearchResultsBy', function() {
        return function(items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function(item) {
                filtered.push(item);
            });
            // filtered.sort(function (a, b) {
            //     return (a[field] > b[field] ? 1 : -1);
            // });
            // if(reverse) filtered.reverse();
            return filtered;
        };
    });
})();
