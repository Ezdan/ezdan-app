'use strict';
(function() {
    angular
        .module('ezdanApp')
        .factory('requestService', requestService);
        function requestService($rootScope, $resource, DB, $q) {
            var servicesService = {};
            var getServices = function(token){
                var q = $q.defer();
                var services = [];
                getServicesOffline().then(function(data){
                    // if (data.length === 0) {
                        authorize(token).getServices().$promise.then(function(data){
                            //delete previous records
                            DB.delete('services');
                            // for (var i = 0; i < data.length; i++) {
                            //     DB.insert('services',
                            //         [
                            //             data[i].parentRecordId,
                            //             data[i].categoryType,
                            //             data[i].recordId,
                            //             data[i].caseCategory,
                            //             data[i].description
                            //         ]
                            //     );
                            //     getServicesOffline().then(function(data) {
                            //         services = data;
                            //     });
                            // }
                            // getServicesOffline().then(function(data){
                            //     services = data;
                            //     if(services.length > 0){
                            //         q.resolve(services);
                            //     }
                            //     else {
                            //         q.reject(services);
                            //     }
                            // });
                            q.resolve(data);
                        }, function(error)   { 
                            console.log(error) 
                            q.reject(error);
                        });
                    // }
                    // else {
                    //     //Get Service from local database
                    //     services = data;
                    //     q.resolve(services);
                    // }
                });
                return q.promise;
            };
            //API Service
            var authorize = function(token) {
                var service = $resource(window.backendUrl + 'Account', {}, {
                    'getServices': {
                        method: 'GET', isArray: true,
                        headers: { 'Authorization': 'Bearer ' + token },
                        url: window.backendUrl + 'servicerequest/getcategorieslist'
                    },
                    'submitRequest': {
                        method: 'POST', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + token },
                        url: window.backendUrl + 'servicerequest/sendservicerequest'
                    },
                    'submitRequestWithImages': {
                        method: 'POST', isArray: false,
                        headers: { 'Authorization': 'Bearer ' + token },
                        url: window.backendUrl + 'servicerequest/sendrequestimage'
                    }
                });
                return service;
            }
            //Local Database
            function getServicesOffline() {
                return  DB.get("services");
            };
            servicesService.getServices = getServices;
            servicesService.authorize = authorize;
            return servicesService;
        }
})();
