'use strict';
(function() {
    angular 
        .module('ezdanApp')
        .factory('propertyService', property);
        function property($rootScope, $resource, $q, DB) {
            var propertiesService = {};
            var getFilters = function() {
                var q = $q.defer();
                var filters = {};
                DB.get("propertiesFilters").then(function(data){
                    if (data.length === 0) {
                        properties.getFilters().$promise.then(function(data){
                            if(data && data instanceof Object) {
                                DB.delete('propertiesFilters');
                                for(var prop in data) {
                                    if(data.hasOwnProperty(prop) 
                                        && data[prop] instanceof Array) {
                                        for(var i = 0; i < data[prop].length; i++) {
                                            DB.insert('propertiesFilters', [
                                                data[prop][i].ID, 
                                                data[prop][i].Display, 
                                                data[prop][i].Value,
                                                prop
                                            ]);
                                        }
                                    }
                                }
                                q.resolve(data);
                            }
                        }, function(error) {
                            $rootScope.popup(
                                'error',
                                $rootScope.langCounterpart('failure') || 'Failure!',
                                $rootScope.langCounterpart('no_filters_found') || 'No filters found.'
                            );
                        })
                    }
                    else {
                        // Getting filters from database
                        for(var i = 0; i < data.length; i++) {
                            if(!filters.hasOwnProperty(data[i].type)) {
                                filters[data[i].type] = [];
                            }
                            filters[data[i].type].push({
                                ID: data[i].id,
                                Display: data[i].display,
                                Value: data[i].value
                            });
                        }
                        q.resolve(filters);
                    }
                });
                return q.promise;
            };
            var properties = $resource(window.backendUrl, {}, {
                'get': {
                    method: 'GET', isArray: false,
                    url: window.backendUrl + 'Property'
                },
                'find': {
                  method: 'GET', isArray: false,
                  url : window.backendUrl + 'Property/Find'
                },
                'getFilters': {
                    method: 'GET', isArray: false,
                    url : window.backendUrl + 'Property/GetFilters'
                },
                'findBybBuilgingId': {
                    method: 'GET', isArray: false,
                    url: window.backendUrl + 'Property/GetPropertyById'
                },
                'getPropertyInterceptedImages': {
                    method: 'GET', isArray: true,
                    url: window.backendUrl + 'Property/GetPropertyTempImages'
                },
                'deletePropertyInterceptedImages': {
                    method: 'GET', isArray: false,
                    url: window.backendUrl + 'Property/DeletePropertyTempImages'
                }
            });
            propertiesService.properties = properties;
            propertiesService.getFilters = getFilters;
            return propertiesService;
        }
})();
