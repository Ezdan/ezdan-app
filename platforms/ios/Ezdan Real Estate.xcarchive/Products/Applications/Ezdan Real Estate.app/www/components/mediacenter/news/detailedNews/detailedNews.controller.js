'use strict';
(function() {
    angular
    .module('ezdanApp')
    .directive('makeIt', function() {
        return {
            link: function(scope, element, attrs) {
                var theImage = element[0];
                theImage.onload = function() {
                    var b64 = window.btoa(theImage);
                    var dataURL = "data:image/png;base64," + b64;
                    scope.$apply(function() {
                        scope.$parent.imageData.dataURL = dataURL;
                    })
                }
            }
        }
    })
    .controller('detailedNewsController', function($scope, $rootScope, newsService, $sce, $stateParams, $ionicActionSheet, DB) {
        $scope.imageData = {
            dataURL: ""
        };
        $scope.loaded = false;
        $scope.$watch('imageData.dataURL', function(newVal) {
            if(newVal !== "") {
                $scope.loaded = true;
            }
        });
        $scope.online = true;
        $scope.$watch(function() {
            return $rootScope.isOnline;
        }, function(isOnline) {
            $scope.online = isOnline;
        })


        $scope.getSingleNews = function () {
            $rootScope.load();
            var newsKey = $stateParams.newsId + '_' + $rootScope.lang;
            if (!$rootScope.isOnline) {
                DB.find('singleNews', 'key', newsKey).then(function(res){
                    if (res.length > 0) {
                        $scope.singleNews = JSON.parse(res[0].value);
                        $scope.singleNewsId = $scope.singleNews[0].id;
                        $scope.singleNewsTitle = $scope.singleNews[0].title;
                        $scope.singleNewsDescription = $scope.singleNews[0].description;
                        $scope.singleNewsDescriptionHTML = $sce.trustAsHtml($scope.singleNewsDescription);
                        $scope.singleNewsImage =  $scope.singleNews[0].newsimage;
                        $scope.singleNewsDate = $scope.singleNews[0].newsdate;
                        var date = $scope.singleNewsDate.split(' ');
                        $scope.singleNewsDate = date[0];
                        $rootScope.unload();
                    } else {
                        $rootScope.unload();
                        $rootScope.goBack();
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('failure') || 'Failure!',
                            $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                        );
                    }
                });
            } else {
                DB.find('singleNews', 'key', newsKey).then(function(res){
                    if (res.length > 0) {
                        var dateDifference = Math.floor((new Date() - new Date(res[0].last_modified)) / (1000*60));
                        if (dateDifference >= 30) {
                            $scope.getSingleNewsPromise();
                        } else {
                            $scope.singleNews = JSON.parse(res[0].value);
                            $scope.singleNewsId = $scope.singleNews[0].id;
                            $scope.singleNewsTitle = $scope.singleNews[0].title;
                            $scope.singleNewsDescription = $scope.singleNews[0].description;
                            $scope.singleNewsDescriptionHTML = $sce.trustAsHtml($scope.singleNewsDescription);
                            $scope.singleNewsImage =  $scope.singleNews[0].newsimage;
                            $scope.singleNewsDate = $scope.singleNews[0].newsdate;
                            var date = $scope.singleNewsDate.split(' ');
                            $scope.singleNewsDate = date[0];
                            $rootScope.unload();
                        }
                    } else {
                        $scope.getSingleNewsPromise();
                    }
                });
            }
        };

        $scope.getSingleNewsPromise = function () {
            var newsKey = $stateParams.newsId + '_' + $rootScope.lang;
            newsService.getNews({"lang":$rootScope.lang, "id":$stateParams.newsId}).$promise.then(function (data) {
                if (data) {
                    //check if active is true
                    $scope.singleNews = data['News'];
                    $scope.singleNewsId = $scope.singleNews[0].id;
                    $scope.singleNewsTitle = $scope.singleNews[0].title;
                    $scope.singleNewsDescription = $scope.singleNews[0].description;
                    $scope.singleNewsDescriptionHTML = $sce.trustAsHtml($scope.singleNewsDescription);
                    $scope.singleNewsImage =  $scope.singleNews[0].newsimage;
                    $scope.singleNewsDate = $scope.singleNews[0].newsdate;
                    var date = $scope.singleNewsDate.split(' ');
                    $scope.singleNewsDate = date[0];
                    DB.find('singleNews', 'key', newsKey).then(function(res){
                        if (res.length == 0) {
                            DB.insert('singleNews',[newsKey,JSON.stringify($scope.singleNews),new Date()]);
                        } else {
                            DB.updateRow('singleNews','value',JSON.stringify($scope.singleNews),'key',newsKey);
                            DB.updateRow('singleNews','last_modified',new Date(),'key',newsKey);
                        }
                    });
                    $rootScope.unload();

                }
            }, function (error) {
                $rootScope.unload();
                $rootScope.goBack();
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
            });
        };




        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        }

        $scope.shareFacebook = function() {
            window.plugins.socialsharing.shareViaFacebook(
                $scope.singleNewsDescription,
                $scope.singleNewsImage /* img */,
                null /* url */,
                function() { console.log('shared on facebook') },
                function(error) {
                    $scope.popupMissingApp();
                }
            );
        };
        $scope.shareTwitter = function() {
            window.plugins.socialsharing.shareViaTwitter(
                $scope.singleNewsDescription,
                $scope.singleNewsImage /* img */,
                null /* url */,
                function() { console.log('shared on twitter') },
                function(error) {
                    $scope.popupMissingApp();
                }
            );
        };
        $scope.shareWhatsApp = function() {
            window.plugins.socialsharing.shareViaWhatsApp(
                $scope.singleNewsDescription,
                $scope.singleNewsImage /* img */,
                null /* url */,
                function() { console.log('shared on whatsapp') },
                function(error) {
                    $scope.popupMissingApp();
                }
            );
        };
        $scope.shareMail = function() {
            window.plugins.socialsharing.shareViaEmail(
                $scope.singleNewsDescription,
                'Ezdan New - ' + $scope.title,
                null,
                null,
                null,
                [$scope.singleNewsImage],
                function(error) {
                    $scope.popupMissingApp();
                }
            );
        };
        $scope.showActionsheet = function() {
            if($scope.online) {
                $ionicActionSheet.show({
                    titleText: 'Share Via',
                    buttons: [
                        {
                            text: '<i class="icon ion-social-facebook"></i> Facebook'
                        },
                        {
                            text: '<i class="icon ion-social-twitter"></i> Twitter'
                        },
                        {
                            text: '<i class="icon ion-social-whatsapp"></i> WhatsApp'
                        },
                        {
                            text: '<i class="icon ion-ios-email"></i> Mail'
                        }
                    ],
                    // destructiveText: 'Delete',
                    cancelText: 'Cancel',
                    cancel: function() {},
                    buttonClicked: function(index) {
                        switch(index) {
                            case 0:
                            $scope.shareFacebook();
                            break;
                            case 1:
                            $scope.shareTwitter();
                            break;
                            case 2:
                            $scope.shareWhatsApp();
                            break;
                            default:
                            $scope.shareMail();
                        }
                        return true;
                    },
                    destructiveButtonClicked: function() { return true; }
                });
            }
            else {
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_your_internet_connection') || 'Please check your internet connection.'
                );
            }
        };
        $scope.popupMissingApp = function() {
            $rootScope.popup(
                'error',
                $rootScope.langCounterpart('failure') || 'Failure!',
                $rootScope.langCounterpart('please_check_that_this_app_is_installed') || 'Please check that this app is installed.'
            );
        };

        $scope.getSingleNews();
    });
})();
