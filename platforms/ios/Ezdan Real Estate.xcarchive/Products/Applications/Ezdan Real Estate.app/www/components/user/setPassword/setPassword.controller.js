'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller('setPasswordController', function ($scope, $rootScope, $ionicSideMenuDelegate, $state, $http, $stateParams, $ionicHistory, user, localStorageService) {
        $ionicSideMenuDelegate.canDragContent(false);
        $scope.regex = {
            password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d$@$!%*?!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{6,}$/
        };

        $scope.setPasswordData = {};
        $scope.resetPasswordMail = $stateParams.email.replace(/"/g, "");
        $scope.newPasswordSubmitted = false;
        $scope.newPasswordSent = false;
        $scope.setPasswordProblem = false;
        $scope.setPasswordText = "Reset";
        $scope.errors = {};
        $scope.setPassword = function(form) {
            if($rootScope.isOnline) {
                $scope.newPasswordSubmitted = true;
                if(form.$valid) {
                    $scope.newPasswordSent = true;
                    $rootScope.load();
                    user.api().setPassword({
                        Email: $scope.resetPasswordMail,
                        NewPassword: $scope.setPasswordData.newPassword,
                        ConfirmPassword: $scope.setPasswordData.newPasswordConfirmation
                    }).$promise.then(function(data) {
                        $rootScope.unload();
                        if(data && data.updated) {
                            $rootScope.newPasswordSent = false;
                            $rootScope.go('land');
                            $rootScope.popup(
                                'success',
                                $rootScope.langCounterpart('password_has_been_reset') || 'Password Has Been Reset',
                                $rootScope.langCounterpart('you_can_continue_to_login') || 'You can now continue to login.'
                            );
                        }
                        $scope.newPasswordSubmitted = $scope.newPasswordSent = false;
                    }, function(error) {
                        $rootScope.unload();
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('failure') || 'Failure!',
                            $rootScope.langCounterpart('error_occurred') || 'Error Occurred !.'
                        );
                        $scope.newPasswordSubmitted = $scope.newPasswordSent = false;
                        // $scope.setPasswordProblem = true;
                    })
                }
            } else {
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
            }
        }
        $scope.goLand = function() {
            $scope.setPasswordData = {};
            $scope.newPasswordSubmitted = $scope.newPasswordSent = $scope.setPasswordProblem = false;
            $rootScope.passwordReset = false;
            $state.go("land");
        }
    });
})();
