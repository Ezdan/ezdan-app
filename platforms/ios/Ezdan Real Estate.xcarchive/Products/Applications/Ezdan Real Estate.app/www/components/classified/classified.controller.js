'use strict';
(function() {
    angular.module('ezdanApp')
    .controller('classifiedController', function($scope, $state, $rootScope, classifiedService, authentication, $cordovaFile, $cordovaImagePicker, $ionicPopup, $cordovaCamera, $cordovaFileTransfer, $cordovaActionSheet) {

        // $scope.allImages = new Array();
        $scope.textArea = {
            cols: 28,
            rows: 1
        };
        $scope.classifiedModel = {
            "images": [],
            "name": null,
            "category": null,
            "description": "",
            "price": null,
            "phone": $rootScope.user.mobile,
            "email": $rootScope.user.email
        };

        $scope.editContentPropsModel = function(){
            return{
            "ContentPropertyId":null,
            "ContentId": null,
            "PropertyId": null,
            "EnglishValue": null,
            "ArabicValue": null
        }
        };

        $scope.editClassifiedModel = {
            "content": {
                "ContentId": null,
                "ParentId": null,
                "ContentType": null,
                "EnglishName": null,
                "ArabicName": null,
                "Blocked": false,
                "Reported": false,
                "CreatedOn": null
            },
            "contentProps": [],
            "deletedImages" : []
        };

        $scope.regex = {
            name: /^(?![0-9\u0660-\u0669\s!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]*$)(.){2,50}$/,
            description: /^(?![0-9\u0660-\u0669\s!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]*$)(.){15,499}$/,
            phone: /^[0-9]{8,8}$/,
            email: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
        };
        $scope.classifiedSubmit = false;
        $scope.editClassifiedSubmit = false;

        $scope.setTwoNumberDecimal = function () {
            if($scope.classifiedModel.price != undefined){
                $scope.classifiedModel.price = parseFloat($scope.classifiedModel.price.toFixed(2));
            }
        };
        $scope.editInit = function () {
            $scope.toBeEdited = classifiedService.getModel();
            $scope.selected_category = {};

            $scope.addedImages = [];
            $scope.classifiedCurrentImages = $scope.toBeEdited.content.properties['images'];
            $scope.classifiedTitle = $scope.toBeEdited.content.title;
            $scope.categoriesLoading();
        }
        $scope.selectCategory = function (cat) {
            $scope.selected_category = cat.id;
        };
        $scope.setEditClassifiedModel = function () {
            $scope.editClassifiedModel.content.ContentId = $scope.toBeEdited.content.id;
            $scope.editClassifiedModel.content.ParentId = $scope.selected_category;
            $scope.editClassifiedModel.content.ContentType = $scope.toBeEdited.content.type;
            $scope.editClassifiedModel.content.EnglishName = $scope.toBeEdited.content.title;
            $scope.editClassifiedModel.content.ArabicName = $scope.toBeEdited.content.title;
            $scope.editClassifiedModel.content.Blocked = $scope.toBeEdited.content.blocked;
            $scope.editClassifiedModel.content.Reported = $scope.toBeEdited.content.reported;
            $scope.editClassifiedModel.content.CreatedOn = $scope.toBeEdited.content.created_on;

            for (var property in $scope.toBeEdited.content.properties) {
                if (property != 'posted_on' && property != 'posted_by' && property != 'images' && property != 'listItems') {
                    var contentprop = $scope.editContentPropsModel();
                    contentprop.ContentPropertyId = $scope.toBeEdited.content.properties[property].ContentPropertyId;
                    contentprop.ContentId = $scope.toBeEdited.content.id;
                    contentprop.PropertyId = $scope.toBeEdited.content.properties[property].PropertyId;
                    contentprop.EnglishValue = $scope.toBeEdited.content.properties[property].value;
                    contentprop.ArabicValue = $scope.toBeEdited.content.properties[property].value;
                    // temp = clone($scope.editContentPropsModel);
                    $scope.editClassifiedModel.contentProps.push(contentprop);
                }
            }
        };

        $scope.toggleSold = function () {
                if($scope.toBeEdited.content.properties['ifSold'].value == 'False') {
                    $scope.toBeEdited.content.properties['ifSold'].value = 'True';
                } else {
                    $scope.toBeEdited.content.properties['ifSold'].value = 'False';
                }
        };

        $scope.editClassified = function (form) {
            $scope.editClassifiedSubmit = true;
            if(form.$valid) {
                $rootScope.load();
                $scope.setEditClassifiedModel();
                classifiedService.authorize(authentication.authData.token).editClassified($scope.editClassifiedModel)
                .$promise.then(function (data) {
                    // var classifiedId = data.classifiedId;
                    if($scope.addedImages.length > 0) {
                        $scope.uploadImage($scope.toBeEdited.content.id, $scope.addedImages);
                    } else {
                        $rootScope.unload();
                        $scope.showConfirmationPopup(true);
                    }
                    // $rootScope.unload();
                }, function (error) {
                    $scope.classifiedSubmit = false;
                    $rootScope.unload();
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('failure') || 'Failure!',
                        $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                    );
                });
            }
        };

        $scope.addNewClassified = function (form) {
            $scope.classifiedSubmit = true;
            if(form.$valid) {
                $rootScope.load();
                classifiedService.authorize(authentication.authData.token).addNewClassified($scope.classifiedModel)
                .$promise.then(function (data) {
                    var classifiedId = data.classifiedId;
                    if($scope.classifiedModel.images.length > 0) {
                        $scope.uploadImage(classifiedId, $scope.classifiedModel.images);
                    } else {
                        $rootScope.unload();
                        $scope.showConfirmationPopup(true);
                    }
                }, function (error) {
                    $scope.classifiedSubmit = false;
                    $rootScope.unload();
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('failure') || 'Failure!',
                        $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                    );
                });
            }
        };

        $scope.doResize = function () {
            var maxrows = 5;
            var txt = $scope.classifiedModel.description;
            var cols = $scope.textArea.cols;

            var arraytxt = txt.split('\n');
            var rows = arraytxt.length;

            for (var i = 0; i < arraytxt.length; i++)
            rows += parseInt(arraytxt[i].length / cols);

            if (rows > maxrows)
            $scope.textArea.rows = maxrows;
            else
            $scope.textArea.rows = rows;
        };

        $scope.categoriesLoading = function () {
            classifiedService.authorize(authentication.authData.token).getAdvertsCategories(
                {
                    "lang": $rootScope.lang
                })
                .$promise.then(function (data) {
                    if (data) {
                        $scope.categories = data;
                        if($state.is('editClassifiedForm')) {
                            var index = 0;
                            for (var i = 0; i < $scope.categories.length; i++) {
                                if($scope.categories[i].id == $scope.toBeEdited.parent.id) {
                                    index= i;
                                }
                            }
                            $scope.selectedCat = $scope.categories[index];
                            $scope.selected_category = $scope.categories[index].id;
                        }
                    }
                });

            };

            $scope.uploadImage = function(id, imagesArray) {
                var url = window.backendUrl + "advertisement/saveimage";
                var successResponse = [];
                var failResponse = [];
                if(imagesArray.length != 0) {
                    for (var i = 0; i < imagesArray.length; i++) {
                        var img = imagesArray[i];
                        var targetPath = imagesArray[i];
                        // File name only
                        var filename = targetPath.substr(targetPath.lastIndexOf('/')+1).replace(/\s/g,'');
                        var optionsUpload = {
                            fileKey: "file",
                            fileName: filename,
                            chunkedMode: false,
                            mimeType: "multipart/form-data",
                            params : {
                                'fileName': filename,
                                'classifiedId': id,
                            },
                            headers: {
                            Connection: "close",
                             Authorization: 'Bearer ' + $rootScope.user.token
                            }
                        };
                        // uploadCounter = i;
                        $cordovaFileTransfer.upload(url, targetPath, optionsUpload).then(function(result) {
                            successResponse.push(result);
                            if(successResponse.length == (imagesArray.length)) {
                                $scope.showConfirmationPopup(true);
                            }
                        }, function (error) {
                            failResponse.push(error);
                            if(failResponse.length > 0) {
                                $scope.showConfirmationPopup(false);
                            }
                        });
                    }
                }

            };

            $scope.selectImages = function(imagesArray) {
                var optionsSelect = {
                    title: 'Please select images source',
                    buttonLabels: ['Load from gallery', 'Use camera'],
                    addCancelButtonWithLabel: 'Cancel',
                    androidEnableCancelButton : true,
                };
                if(imagesArray.length < 3) {
                    $cordovaActionSheet.show(optionsSelect).then(function(btnIndex) {
                        if (btnIndex === 1) {
                            var permissions = cordova.plugins.permissions;
                            permissions.checkPermission(permissions.READ_EXTERNAL_STORAGE, function( status ){
                                if ( status.hasPermission  ) {
                                    $scope.selectImagesFromGallery(imagesArray);
                                }
                                else {
                                    permissions.requestPermission(permissions.READ_EXTERNAL_STORAGE,
                                        function success( status ) {
                                        if( !status.hasPermission ) {

                                            error();
                                        } else {
                                            $scope.selectImagesFromGallery(imagesArray);
                                        }
                                    }, 
                                    function error() {
                                        console.warn('Camera permission is not turned on');
                                    });   
                                }
                            });
                        } else if (btnIndex === 2) {
                            $scope.selectImageByCamera(imagesArray);
                        }
                    });
                } else {
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('three_images_allowed') || 'Sorry! You can only upload three images per classified.'
                    );
                }
            };

            $scope.selectImagesFromGallery = function (imagesArray) {
                var optionsGallery = {
                    maximumImagesCount: 3 - imagesArray.length,
                    quality: 50
                };
                $cordovaImagePicker.getPictures(optionsGallery)
                .then(function (results) {
                    if(results.length != 0) {
                        for (var i = 0; i < results.length; i++) {

                            if($scope.toBeEdited != undefined && $scope.toBeEdited.content.type == 'classifieddetail') {
                                var img = {"src": results[i]};
                                imagesArray.push(img);
                                $scope.addedImages.push(results[i]);
                            } else {
                                imagesArray.push(results[i]);
                            }
                        }
                    }
                }, function(error) {

                });
            };

            $scope.selectImageByCamera = function (imagesArray) {
                var optionsCamera = {
                    quality: 50,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    saveToPhotoAlbum: false
                };

                $cordovaCamera.getPicture(optionsCamera).then(function(imagePath) {
                    var currentName = imagePath.replace(/^.*[\\\/]/, '');
                    var d = new Date(),
                    n = d.getTime(),
                    newFileName =  n + ".jpg";
                    var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);

                    $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){

                        if($scope.toBeEdited != undefined && $scope.toBeEdited.content.type == 'classifieddetail') {
                            var img = {"src": cordova.file.dataDirectory + newFileName};
                            imagesArray.push(img);
                            $scope.addedImages.push(cordova.file.dataDirectory + newFileName);
                        } else {
                            imagesArray.push(cordova.file.dataDirectory + newFileName);
                        }
                    }, function(error){
                        $scope.showAlert('Error', error.exception);
                    });

                },
                function(err){

                })
            };

            $scope.removeImage = function (index, imagesArray) {
                $scope.showRemovePopup(index, imagesArray);
            };

            $scope.showRemovePopup = function(index, imagesArray) {
                $ionicPopup.show({
                    template: '<span>Are you sure you want to delete this image?</span>',
                    title: 'Confirmation!',
                    scope: $scope,
                    buttons: [
                        { text: 'Cancel' },
                        {
                            text: '<b>Confirm</b>',
                            type: 'button-positive',
                            onTap: function(e) {
                                if(imagesArray === $scope.classifiedCurrentImages) {
                                    var imageProp = $scope.editContentPropsModel();
                                    imageProp.ContentPropertyId = $scope.classifiedCurrentImages[index].ContentPropertyId;
                                    imageProp.ContentId = $scope.toBeEdited.content.id;
                                    imageProp.PropertyId = $scope.classifiedCurrentImages[index].PropertyId;
                                    imageProp.EnglishValue = $scope.classifiedCurrentImages[index].src;
                                    imageProp.ArabicValue = $scope.classifiedCurrentImages[index].src;
                                    $scope.editClassifiedModel.deletedImages.push(imageProp);
                                }
                                imagesArray.splice(index, 1);
                            }
                        }
                    ]
                });
            };

            $scope.showConfirmationPopup = function(status) {
                $rootScope.unload();
                if (status === true) {
                    $ionicPopup.show({
                        template: 'Your Classified has been submitted successfully!',
                        title: 'Confirmation!',
                        scope: $scope,
                        buttons: [
                            {
                                text: '<b>Confirm</b>',
                                type: 'button-positive',
                                onTap: function(e) {
                                    $rootScope.deleteBackHistory();
                                    $state.go("userClassifiedList");
                                }
                            }
                        ]
                    });
                } else {
                    $ionicPopup.show({
                        template: 'Your Classified has been submitted, however not all images have been uploaded successfully. Please review the missing images and add them.',
                        title: 'Confirmation!',
                        scope: $scope,
                        buttons: [
                            {
                                text: '<b>Confirm</b>',
                                type: 'button-positive',
                                onTap: function(e) {
                                    $rootScope.deleteBackHistory();
                                    $state.go("userClassifiedList");
                                }
                            }
                        ]
                    });
                }
            };

            $scope.userAdvertisementsLoading = function () {
                classifiedService.authorize(authentication.authData.token).getUserAdverts(
                    {
                        "lang": $rootScope.lang
                    })
                    .$promise.then(function (data) {
                        if (data) {
                            $scope.data = data;
                        }
                    });
                };



            });
        })();
