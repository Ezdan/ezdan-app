'use strict';
(function() {
    angular
    .module('ezdanApp')
    .controller("cardTypesListController",function($rootScope, $stateParams, $ionicPopup, $ionicModal, $scope, $state, $timeout, cardService, authentication, DB) {
        $scope.cardTypes = [
            {
                text : $rootScope.langCounterpart('partners_card_offers') || 'Partner Offers',
                id : 1
            },
            {
                text : $rootScope.langCounterpart('limited_card_offers') || 'Limited Offers',
                id : 2
            },
            // {
            //     text : $rootScope.langCounterpart('partners_card_offers') || 'Partners Card Offers',
            //     id : 3
            // }
        ];
        $scope.enrollData = {
            contactNumber: $rootScope.user.mobile
        };
        $scope.formSubmitted = false;
        $scope.errors = {};
        $scope.cardTypeCategories = {
            allcategories: [],
            selectedCategory: null,
            categoryCards: []
        };
        // $scope.closeModal = function() {
        //     $scope.modal.hide();
        //     // $scope.modal.remove()
        // };
        $scope.enrollForm = {};
        $scope.$watch('enrollForm.input.$valid', function(newVal) {
            $scope.valid = newVal;
        });
        $scope.enrollAccount = function (form) {
            $scope.formSubmitted = true;
            if(form.$valid) {
                // return;
                $rootScope.load();
                cardService.enrollDiscountCard(authentication.authData.token,
                    {
                        'contactNumber': $scope.enrollData.contactNumber,
                        'category': $stateParams.category
                        ,"seo":'x18'
                    })
                    .then(function(result){
                    // $scope.closeModal();
                    $rootScope.unload();
                    $rootScope.popup(
                        'success',
                        $rootScope.langCounterpart('request_submitted') || 'Request Submitted',
                        $rootScope.langCounterpart('you_will_be_contacted_soon') || 'You will be contacted soon.'
                    );
                    $rootScope.deleteBackHistory();
                    $state.go("home");
                    // $state.go('cardTypes');
                },function(error){
                    // $scope.closeModal();
                    $rootScope.unload();
                    $rootScope.popup(
                        'error',
                        $rootScope.langCounterpart('failure') || 'Failure!',
                        $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                    );
                    // $state.go('cardTypes');
                });
            }
        }
        $scope.goEnroll = function () {
            if(!$rootScope.isOnline) return;
            // if($scope.cardTypeCategories.categoryCards.length === 0) {
            //     $rootScope.popup(
            //         'error',
            //         $rootScope.langCounterpart('card_category_missing') || 'Category Missing',
            //         $rootScope.langCounterpart('please_select_a_card_category') || 'Please select a category.'
            //     );
            //     return;
            // }
            $state.go('enrollCards', { category: $scope.cardTypeCategories.selectedCategory });
        };

        $scope.goOffers = function(cardsId, event) {
            $scope.getOffers(cardsId);
            $('.globalTab').removeClass('active');
            $(event.target.parentElement).addClass('active');
        }

        $scope.initDC = function(index) {
            $scope.getOffers(index);
        }

        $scope.initTab = function () {
            console.log($('.globalTab').length);
            if ($('.globalTab').length > 0) {
                ($('.globalTab')[0]).className+= " active";
            }
        };


        $scope.getEDCOffersPromise = function () {
            var DCKey = '1_' + $rootScope.lang;

            cardService.getEDCOffers().then(function(data){
                $scope.cardOffers = data;
                $scope.cardTypeCategories.allcategories = [];
                $scope.cardTypeCategories.categoryCards = [];
                $scope.cardTypeCategories.selectedCategory = null;
                for (var i = 0; i < $scope.cardOffers.length; i++) {
                    if ($rootScope.lang == 'English') {
                        if ($scope.cardTypeCategories.allcategories.indexOf($scope.cardOffers[i].Sector) < 0) {
                            $scope.cardTypeCategories.allcategories.push($scope.cardOffers[i].Sector);
                        }
                    } else {
                        if ($scope.cardTypeCategories.allcategories.indexOf($scope.cardOffers[i].SectorArabic) < 0) {
                            $scope.cardTypeCategories.allcategories.push($scope.cardOffers[i].SectorArabic);
                        }
                    }

                }

                DB.find('DC', 'key', DCKey).then(function(res){
                    if (res.length == 0) {
                        DB.insert('DC',[DCKey,JSON.stringify($scope.cardOffers),new Date()]);
                    } else {
                        DB.updateRow('DC','value',JSON.stringify($scope.cardOffers),'key',DCKey);
                        DB.updateRow('DC','last_modified',new Date(),'key',DCKey);
                    }
                });

                $rootScope.unload();
            },function(error){
                // $scope.closeModal();
                $rootScope.unload();
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
                // $state.go('cardTypes');
            });
        };

        $scope.getLimitedOffersPromise = function () {
            var DCKey = '2_' + $rootScope.lang;

            cardService.getLimitedOffers().then(function(data){
                $scope.cardOffers = data;
                $scope.cardTypeCategories.allcategories = [];
                $scope.cardTypeCategories.selectedCategory = null;
                $scope.cardTypeCategories.categoryCards = [];
                for (var i = 0; i < $scope.cardOffers.length; i++) {
                    if ($rootScope.lang == 'English') {
                        if ($scope.cardTypeCategories.allcategories.indexOf($scope.cardOffers[i].Sector) < 0) {
                            $scope.cardTypeCategories.allcategories.push($scope.cardOffers[i].Sector);
                        }
                    } else {
                        if ($scope.cardTypeCategories.allcategories.indexOf($scope.cardOffers[i].SectorArabic) < 0) {
                            $scope.cardTypeCategories.allcategories.push($scope.cardOffers[i].SectorArabic);
                        }
                    }
                }
                DB.find('DC', 'key', DCKey).then(function(res){
                    if (res.length == 0) {
                        DB.insert('DC',[DCKey,JSON.stringify($scope.cardOffers),new Date()]);
                    } else {
                        DB.updateRow('DC','value',JSON.stringify($scope.cardOffers),'key',DCKey);
                        DB.updateRow('DC','last_modified',new Date(),'key',DCKey);
                    }
                });

                $rootScope.unload();
            },function(error){
                // $scope.closeModal();
                $rootScope.unload();
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
                // $state.go('cardTypes');
            });
        };

        $scope.getPartnerOffersPromise = function () {
            var DCKey = '3_' + $rootScope.lang;
            cardService.getPartnerOffers().then(function(data){
                $scope.cardOffers = data;
                $scope.cardTypeCategories.allcategories = [];
                $scope.cardTypeCategories.selectedCategory = null;
                $scope.cardTypeCategories.categoryCards = [];
                for (var i = 0; i < $scope.cardOffers.length; i++) {
                    if ($rootScope.lang == 'English') {
                        if ($scope.cardTypeCategories.allcategories.indexOf($scope.cardOffers[i].Sector) < 0) {
                            $scope.cardTypeCategories.allcategories.push($scope.cardOffers[i].Sector);
                        }
                    } else {
                        if ($scope.cardTypeCategories.allcategories.indexOf($scope.cardOffers[i].SectorArabic) < 0) {
                            $scope.cardTypeCategories.allcategories.push($scope.cardOffers[i].SectorArabic);
                        }
                    }
                }
                // $scope.selectedCategory = $scope.cardTypeCategories.allcategories[0];
                DB.find('DC', 'key', DCKey).then(function(res){
                    if (res.length == 0) {
                        DB.insert('DC',[DCKey,JSON.stringify($scope.cardOffers),new Date()]);
                    } else {
                        DB.updateRow('DC','value',JSON.stringify($scope.cardOffers),'key',DCKey);
                        DB.updateRow('DC','last_modified',new Date(),'key',DCKey);
                    }
                });
                $rootScope.unload();
            },function(error){
                // $scope.closeModal();
                $rootScope.unload();
                $rootScope.popup(
                    'error',
                    $rootScope.langCounterpart('failure') || 'Failure!',
                    $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                );
                // $state.go('cardTypes');
            });
        };

        $scope.getOffers = function(cardtype){
            $rootScope.load();
            var DCKey = cardtype + '_' + $rootScope.lang;

            if (!$rootScope.isOnline) {
                DB.find('DC', 'key', DCKey).then(function(res){
                    if (res.length > 0) {
                        $scope.cardOffers = JSON.parse(res[0].value);
                        $scope.cardTypeCategories.allcategories = [];
                        $scope.cardTypeCategories.categoryCards = [];
                        $scope.cardTypeCategories.selectedCategory = null;
                        for (var i = 0; i < $scope.cardOffers.length; i++) {
                            if ($rootScope.lang == 'English') {
                                if ($scope.cardTypeCategories.allcategories.indexOf($scope.cardOffers[i].Sector) < 0) {
                                    $scope.cardTypeCategories.allcategories.push($scope.cardOffers[i].Sector);
                                }
                            } else {
                                if ($scope.cardTypeCategories.allcategories.indexOf($scope.cardOffers[i].SectorArabic) < 0) {
                                    $scope.cardTypeCategories.allcategories.push($scope.cardOffers[i].SectorArabic);
                                }
                            }

                        }

                        $rootScope.unload();

                    } else {
                        $rootScope.unload();
                        $rootScope.popup(
                            'error',
                            $rootScope.langCounterpart('failure') || 'Failure!',
                            $rootScope.langCounterpart('please_check_network') || 'Please check your network.'
                        );
                    }
                });
            } else {
                DB.find('DC', 'key', DCKey).then(function(res){
                    if (res.length > 0) {
                        var dateDifference = Math.floor((new Date() - new Date(res[0].last_modified)) / (1000*60));
                        if (dateDifference >= 30) {
                            if (cardtype == 1) {
                                $scope.getEDCOffersPromise();
                            }
                            else if(cardtype == 2){
                                $scope.getLimitedOffersPromise();
                            }
                            else if(cardtype == 3){
                                $scope.getPartnerOffersPromise();
                            }
                        } else {
                            $scope.cardOffers = JSON.parse(res[0].value);
                            $scope.cardTypeCategories.allcategories = [];
                            $scope.cardTypeCategories.categoryCards = [];
                            $scope.cardTypeCategories.selectedCategory = null;
                            for (var i = 0; i < $scope.cardOffers.length; i++) {
                                if ($rootScope.lang == 'English') {
                                    if ($scope.cardTypeCategories.allcategories.indexOf($scope.cardOffers[i].Sector) < 0) {
                                        $scope.cardTypeCategories.allcategories.push($scope.cardOffers[i].Sector);
                                    }
                                } else {
                                    if ($scope.cardTypeCategories.allcategories.indexOf($scope.cardOffers[i].SectorArabic) < 0) {
                                        $scope.cardTypeCategories.allcategories.push($scope.cardOffers[i].SectorArabic);
                                    }
                                }

                            }
                            $rootScope.unload();
                        }


                    } else {
                        if (cardtype == 1) {
                            $scope.getEDCOffersPromise();
                        }
                        else if(cardtype == 2){
                            $scope.getLimitedOffersPromise();
                        }
                        else if(cardtype == 3){
                            $scope.getPartnerOffersPromise();
                        }
                    }
                });
            }
        };

        $scope.categoryChange = function () {
            $rootScope.load();
            $scope.cardTypeCategories.categoryCards = [];
            for (var i = 0; i < $scope.cardOffers.length; i++) {
                if ($rootScope.lang == 'English') {
                    if ($scope.cardOffers[i].Sector === $scope.cardTypeCategories.selectedCategory) {
                        $scope.cardTypeCategories.categoryCards.push($scope.cardOffers[i]);
                    }
                } else {
                    if ($scope.cardOffers[i].SectorArabic === $scope.cardTypeCategories.selectedCategory) {
                        $scope.cardTypeCategories.categoryCards.push($scope.cardOffers[i]);
                    }
                }
            }
            $rootScope.unload();
        };

        // $scope.initDC($scope.cardTypes[0].id);
    });
})();
//
