'use strict';
(function() {
    angular
    .module('ezdanApp')
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
        .state('land', {
            url: '/land',
            templateUrl: 'components/land/land.html',
            controller: 'landController',
            cache: false
        })
        .state('setPassword', {
            url: '/setPassword',
            templateUrl: 'components/user/setPassword/setPassword.html',
            controller: 'setPasswordController',
            params: {
                email: null
            },
            cache: false
        })
        .state('register', {
            url: '/register',
            templateUrl: 'components/user/register/register.html',
            controller: 'registerController',
            cache: false
        })
        .state('home', {
            url: '/home',
            // abstract: true,
            templateUrl: 'components/home/home.html',
            controller: 'homeController',
            cache: false
        })
        .state('request', {
            url: '/request',
            controller: 'requestsController',
            templateUrl: 'components/request/request.html',
            cache: false
        })
        .state('surveys', {
            url: '/surveys',
            controller: 'surveysController',
            templateUrl: 'components/surveys/surveys.html'
        })
        .state('locationsurveies', {
            url: '/locationsurveies/:locationId/:location',
            controller: 'surveyPerLocationController',
            templateUrl: 'components/surveys/surveyPerLocation.html'
        })
        .state('survey', {
            url: '/survey/:surveyId/:locationid',
            controller: 'surveyQuestionsController',
            templateUrl: 'components/surveys/surveyQuestions.html',
            cache: false
        })
        .state('feedback', {
            url: '/feedback',
            controller: 'feedbackController',
            templateUrl: 'components/feedback/feedback.html',
            params: {
                id: null
            },
            cache: false
        })
        .state('properties', {
            url: '/properties/:resetFilter', ///:cityParam/:bedroomsParam/:propertyTypeParam/:isFurnished
            controller: 'propertiesController',
            templateUrl: 'components/property/properties/properties.html',
            cache: true
        })
        .state('propertyDetail', {
            url: '/property/:BuildingUnitId',
            controller: 'propertyDetailController',
            templateUrl: 'components/property/detail/detail.html',
            cache: false
        })
        .state('cardTypes', {
            url: '/cardTypes',
            controller: 'cardTypesListController',
            templateUrl: 'components/card/cardTypes/cardTypesList.html',
            cache: false
        })
        .state('enrollCards', {
            url: '/enrollcards',
            controller: 'cardTypesListController',
            templateUrl: 'components/card/cardTypes/cardEnroll.html',
            params: {
                category: null
            },
            cache: false
        })
        .state('newslist', {
            url: '/news',
            controller: 'newsListController',
            templateUrl: 'components/mediacenter/news/newsList/newsList.html'
        })
        .state('detailednews', {
            url: '/news/:newsId',
            controller: 'detailedNewsController',
            templateUrl: 'components/mediacenter/news/detailedNews/detailedNews.html',
            cache: false
        })
        .state('albumslist', {
            url: '/albums',
            controller: 'albumsListController',
            templateUrl: 'components/mediacenter/photogallery/albumsList/albumsList.html',
            cache: false
        })
        .state('plusRequest', {
            url: '/plusRequest',
            controller: 'plusRequestController',
            templateUrl: 'components/plus/plusRequest/plusRequest.html',
            cache: false,
            params: {
                serviceObj: null
            },
        })
        .state('userClassifiedList', {
            url: '/myclassifieds',
            controller: 'classifiedController',
            templateUrl: 'components/classified/userClassifiedList.html',
            cache: false
        })
        .state('createClassifiedForm', {
            url: '/myclassifieds/add',
            controller: 'classifiedController',
            templateUrl: 'components/classified/CreateClassifiedForm.html',
            cache: false
        })
        .state('editClassifiedForm', {
            url: '/myclassifieds/edit/:id',
            controller: 'classifiedController',
            templateUrl: 'components/classified/editClassifiedForm.html',
            cache: false
        })
        .state('content', {
            url: '/content/:id',
            controller: 'contentController',
            templateUrl: 'components/cms/content.html',
            cache: false
        })
        .state('appointments', {
            url: '/appointments',
            controller: 'appointmentController',
            templateUrl: 'components/appointments/appointment.html',
            cache: false
        })
        .state('chooseLanguage', {
            url: '/chooselanguage',
            controller: 'landController',
            templateUrl: 'components/land/chooseLanguage.html',
            cache: false
        })
        .state('login', {
            url: '/login',
            controller: 'loginController',
            templateUrl: 'components/user/login/login.html',
            cache: false
        })
        .state('userProfile', {
            url: '/userprofile',
            controller: 'userProfileController',
            templateUrl: 'components/user/userProfile/userProfile.html',
            cache: false
        })
        .state('accountSettings', {
            url: '/accountsettings',
            controller: 'accountSettingsController',
            templateUrl: 'components/user/accountSettings/accountSettings.html',
            cache: false
        })
        .state('verifySetPassword', {
            url: '/verifySetPassword',
            templateUrl: 'components/user/verifySetPassword/verifySetPassword.html',
            controller: 'verifySetPasswordController',
            params: {
                email: null
            },
            cache: false
        })
        .state('forgotPassword', {
            url: '/forgotPassword',
            templateUrl: 'components/user/forgotPassword/forgotPassword.html',
            controller: 'forgotPasswordController',
            cache: false
        })
        .state('map', {
            url: '/map',
            templateUrl: 'components/map/map.html',
            controller: 'mapController',
            params: {
                lat: null,
                long: null
            },
            cache: false
        })
        // New design views
        .state('ourServices', {
            url: '/ourServices',
            templateUrl: 'components/new/ourServices/ourServices.html',
            controller: 'ourServicesController',
            cache: false
        })
        .state('sakinService', {
            url: '/sakinService',
            templateUrl: 'components/new/sakinService/sakinService.html',
            controller: 'sakinServiceController',
            cache: false
        })
        .state('customerCare', {
            url: '/customerCare',
            templateUrl: 'components/new/customerCare/customerCare.html',
            controller: 'customerCareController',
            cache: false
        })
        .state('ezdanPlus', {
            url: '/ezdanPlus',
            templateUrl: 'components/new/ezdanPlus/ezdanPlus.html',
            controller: 'ezdanPlusController',
            cache: false
        })
        .state('contactUs', {
            url: '/contactUs',
            templateUrl: 'components/new/contactUs/contactUs.html',
            controller: 'contactUsController',
            cache: false
        })
        .state('announcements', {
            url: '/announcements',
            templateUrl: 'components/announcements/announcements.html',
            controller: 'announcementsController',
            cache: false
        });

        // $urlRouterProvider.otherwise('/chooselanguage');
    });
})();
