## v1.2.0
### Features:
- Added **cacheClearSize** option to clear cache after some limit.
- Add **add**, **get**, **remove**, **getCacheSize** methods.

### Improvements:
- Add jsdoc to service methods.
- Minor code improvements.
