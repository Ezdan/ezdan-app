'use strict';
var dependencies = [
    'ui.router',
    'ionic',
    'ngResource',
    'ngCookies',
    'LocalStorageModule',
    'ionicImgCache',
    'ngAnimate',
    'ngCordova',
    'LocalStorageModule',
    'ion-datetime-picker',
    'ionic-ratings',
    'intlpnIonic',
    'ui.calendar',
    'jett.ionic.filter.bar'
];
(function() {
    angular.module('ezdanApp', dependencies)
    .constant('DEBUG_INFO_ENABLED', false)
    // .constant('BACKEND_URL', "http://35.163.239.173:8085/api/")
    .constant('BACKEND_URL', "http://mobile.ezdanrealestate.qa/api/")
    .constant('DB_CONFIG',{
        name: 'DB.db',
        tables: [
            {
                name: 'home',
                columns: [
                    {name:'bannerImg',type:'text'},
                    {name:'last_modified',type:'DATETIME'}
                ]
            },
            {
                name: 'property',
                columns: [
                    {name:'key',type:'text'},
                    {name:'value',type:'text'},
                    {name:'last_modified',type:'DATETIME'}
                ]
            },
            {
                name: 'services',
                columns: [
                    {name: 'parentRecordId', type: 'integer'},
                    {name: 'categoryType', type: 'text'},
                    {name: 'recordId', type: 'text'},
                    {name: 'caseCategory', type: 'text'},
                    {name: 'description', type: 'text'}
                ]
            },
            {
                name:'dictionary',
                columns:[
                    {name:'key',type :'text'},
                    {name:'english',type :'text'},
                    {name:'arabic',type :'text'},
                ]
            },
            {
                name: 'tempUser',
                columns: [
                    {name:'id',type:'text'},
                    {name:'title',type:'text'},
                    {name:'email',type:'text'},
                    {name:'name',type:'text'},
                    {name:'phone',type:'text'},
                    {name:'password',type:'text'},
                    {name:'passwordConf',type:'text'},
                    {name:'contractNum',type:'text'},
                    {name:'AXAccountId',type:'text'},
                    {name:'AXPartyRecId',type:'text'},
                    {name:'AXLocation',type:'text'},
                    {name:'AXUnitId',type:'text'},
                    {name:'lang',type:'text'}
                ]
            },
            {
                name:'user',
                columns:[
                    {name:'email',type:'text'},
                    {name:'language',type:'text'},
                    {name:'notification',type:'text'},
                    {name:'imageURL',type:'text'},
                    {name:'mobile',type:'text'},
                    {name:'fullName',type:'text'},
                    {name:'title',type:'text'},
                    {name:'unit',type:'text'},
                    {name:'location',type:'text'},
                    {name:'access_token',type:'text'},
                    {name:'expires_in',type:'text'}
                ]
            },
            {
                name:'content',
                columns:[
                    {name:'key',type:'text'},
                    {name:'value',type:'text'},
                    {name:'last_modified',type:'DATETIME'}
                ]
            },
            {
                name: 'propertiesList',
                columns: [
                    {name:'key',type:'text'},
                    {name:'value',type:'text'},
                    {name:'last_modified',type:'DATETIME'}
                ]
            },
            {
                name: 'propertiesFilters',
                columns: [
                    {name:'id',type:'text'},
                    {name:'display',type:'text'},
                    {name:'value',type:'text'},
                    {name:'type',type:'text'}
                ]
            },
            {
                name: 'newsList',
                columns: [
                    {name:'key',type:'text'},
                    {name:'value',type:'text'},
                    {name:'last_modified',type:'DATETIME'}
                ]
            },
            {
                name: 'singleNews',
                columns: [
                    {name:'key',type:'text'},
                    {name:'value',type:'text'},
                    {name:'last_modified',type:'DATETIME'}
                ]
            },
            {
                name: 'albumsList',
                columns: [
                    {name:'key',type:'text'},
                    {name:'value',type:'text'},
                    {name:'last_modified',type:'DATETIME'}
                ]
            },
            {
                name: 'album',
                columns: [
                    {name:'key',type:'text'},
                    {name:'value',type:'text'},
                    {name:'last_modified',type:'DATETIME'}
                ]
            },
            {
                name: 'DC',
                columns: [
                    {name:'key',type:'text'},
                    {name:'value',type:'text'},
                    {name:'last_modified',type:'DATETIME'}
                ]
            },
            // {
            //     name: 'FilterObj',
            //     columns: [
            //         {name:'city',type:'text'},
            //         {name:'bedrooms',type:'text'},
            //         {name:'propertyType',type:'text'},
            //         {name:'isFurnished',type:'text'},
            //     ]
            // }
        ]
    })
    .config(['$httpProvider', '$animateProvider', '$ionicConfigProvider', '$sceDelegateProvider', '$ionicFilterBarConfigProvider', 'BACKEND_URL',
        function ($httpProvider, $animateProvider, $ionicConfigProvider, $sceDelegateProvider, $ionicFilterBarConfigProvider, BACKEND_URL) {
            window.backendUrl = BACKEND_URL;
            $ionicConfigProvider.backButton.previousTitleText(false);
            $ionicConfigProvider.backButton.text("");
            $ionicConfigProvider.views.transition('platform');
            $ionicConfigProvider.views.maxCache(0);
            $animateProvider.classNameFilter(/^((?!(no-animate)).)*$/);
            $sceDelegateProvider.resourceUrlWhitelist([
                // Allow same origin resource loads.
                'self',
                // Allow loading from our assets domain. **.
                'http://ezdanrealestate.qa/**'
            ]);

            $ionicFilterBarConfigProvider.transition('horizontal');
            $ionicFilterBarConfigProvider.placeholder('Search Here');
            // $ionicFilterBarConfigProvider.backdrop(false);
        }]);
})();
//
